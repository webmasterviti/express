-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 30, 2018 at 03:44 PM
-- Server version: 5.7.20
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aramex`
--

-- --------------------------------------------------------

--
-- Table structure for table `box`
--

CREATE TABLE `box` (
  `id` int(10) UNSIGNED NOT NULL,
  `goods_value` varchar(64) DEFAULT NULL,
  `pickup_by` varchar(64) DEFAULT NULL,
  `shield_value` varchar(64) DEFAULT NULL,
  `weight` float UNSIGNED DEFAULT NULL,
  `charg_wt` varchar(64) DEFAULT NULL,
  `pcs` int(10) UNSIGNED DEFAULT NULL,
  `cube` varchar(64) DEFAULT NULL,
  `factor` varchar(32) DEFAULT NULL,
  `product_1` varchar(64) DEFAULT NULL,
  `due_dt` varchar(100) DEFAULT NULL,
  `pickup_dt` varchar(64) DEFAULT NULL,
  `loc` varchar(100) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `goods_org` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `product_2` varchar(64) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `box`
--

INSERT INTO `box` (`id`, `goods_value`, `pickup_by`, `shield_value`, `weight`, `charg_wt`, `pcs`, `cube`, `factor`, `product_1`, `due_dt`, `pickup_dt`, `loc`, `remarks`, `goods_org`, `description`, `product_2`, `length`, `width`) VALUES
(15, NULL, NULL, NULL, 10, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-25 22:04:00', NULL, NULL, NULL, 'document ', NULL, 10, 4),
(27, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(28, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(29, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(30, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(31, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(32, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(33, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(34, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(35, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(36, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(37, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(38, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(39, '69', NULL, NULL, 0.4, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:55:00', NULL, NULL, NULL, 'TAILWALK GUNZ 160S 85G TB1', NULL, NULL, NULL),
(40, '0', NULL, NULL, 0.15, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-27 22:04:00', NULL, NULL, NULL, 'docs', NULL, NULL, NULL),
(41, '0', NULL, NULL, 0.1, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-27 16:00:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL),
(42, '0', NULL, NULL, 0.03, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 05:44:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL),
(43, '0', NULL, NULL, 0.1, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 20:42:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL),
(44, '0', NULL, NULL, 0.1, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-30 19:45:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL),
(45, '9.72', NULL, NULL, 20, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-21 20:34:00', NULL, NULL, NULL, 'PRINTED MATTER', NULL, NULL, NULL),
(46, '0', NULL, NULL, 0.07, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-27 11:52:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL),
(47, '0', NULL, NULL, 0.11, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-27 22:31:00', NULL, NULL, NULL, 'DOCUMENT', NULL, NULL, NULL),
(48, '0', NULL, NULL, 0.07, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-27 21:03:00', NULL, NULL, NULL, 'DOC', NULL, NULL, NULL),
(49, '0', NULL, NULL, 0.2, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-28 16:47:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL),
(50, '0', NULL, NULL, 0.5, NULL, 1, NULL, NULL, NULL, NULL, '2017-11-29 13:32:00', NULL, NULL, NULL, 'Documents', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `consignee`
--

CREATE TABLE `consignee` (
  `id` int(10) UNSIGNED NOT NULL,
  `account` varchar(64) DEFAULT NULL,
  `contry` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `name_c` varchar(100) DEFAULT NULL,
  `attn` varchar(100) DEFAULT NULL,
  `ref` varchar(64) DEFAULT NULL,
  `ref2` varchar(64) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `zip` varchar(64) DEFAULT NULL,
  `ukr_address_id` varchar(32) DEFAULT NULL,
  `ukr_client_id` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `consignee`
--

INSERT INTO `consignee` (`id`, `account`, `contry`, `phone`, `phone2`, `name_c`, `attn`, `ref`, `ref2`, `address`, `city`, `state`, `zip`, `ukr_address_id`, `ukr_client_id`) VALUES
(15, NULL, 'UA', '380577663786', NULL, 'Олександр Храбан', 'SG PRODUCTION LIMITED ', NULL, NULL, '', 'Львов', NULL, '79007', NULL, NULL),
(27, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(28, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(29, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(30, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(31, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(32, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(33, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(34, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(35, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(36, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(37, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(38, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(39, NULL, 'UA', '380964857206', NULL, 'Private person', 'Dima Zaika', '861', NULL, 'Svyatonikolaevska ste. 12/42', 'Chernihiv', NULL, NULL, NULL, NULL),
(40, NULL, 'UA', '380675493572', NULL, 'State Company UKROBORONSERVICE', 'SHAROPOV', NULL, NULL, 'p.b49, 3a Rososhanska str Kyiv', 'Kiev', NULL, NULL, NULL, NULL),
(41, NULL, 'UA', '380676708706', NULL, '\"SMC\" Ecopharm\" LTD', 'Nadezhda Ginguliak ', 'ginguliak@ecopharm.ua', NULL, '136B, Naberezhno-Korchavatskaya st.', 'Kiev', NULL, NULL, NULL, NULL),
(42, NULL, 'UA', '380932549999', NULL, 'ISMAIL IBRAHIM ELSIED AHMED', 'ISMAIL IBRAHIM ELSIED AHMED', NULL, NULL, 'P.O BOX 31(044-5216894)', 'Kiev', NULL, NULL, NULL, NULL),
(43, NULL, 'UA', '0038503106332', NULL, 'ARENSOL LLC', 'MISS OLGA RICHA', 'NIL', NULL, 'KYIV CITY', 'Kiev', NULL, NULL, NULL, NULL),
(44, NULL, 'UA', '380954684000', NULL, 'Vitalii Solntsev', 'Vitalii Solntsev', NULL, NULL, 'Academic Edoremova St 7, Flat 106 ZIP CODE 03179', 'Kiev', NULL, NULL, NULL, NULL),
(45, NULL, 'UA', '380444906777', NULL, 'Yuliya Poliakova', 'Yuliya Poliakova', '1028953690000795', NULL, '75 Zhylyanska Street (10th floor)', 'Kyivets', NULL, NULL, NULL, NULL),
(46, NULL, 'UA', '380931009190', NULL, 'Mohamad Mohamad Ahmad', 'Mohamad Mohamad Ahmad', NULL, NULL, 'Kosmonavtov  Street  62/2 , Flat Number 41', 'Odesa', NULL, NULL, NULL, NULL),
(47, NULL, 'UA', 'NONE', NULL, 'V.SHIPS UKRAINE', 'INNA', NULL, NULL, '37/2 MARSHALA ZHUKOV STR.', 'Odesa', NULL, NULL, NULL, NULL),
(48, NULL, 'UA', '380677401733', NULL, 'Sergiy Podlubny', 'Sergiy Podlubny', NULL, NULL, '2 - A Chernomorskaya ST,App. 50 Odessa,ukraine', 'Odesa', NULL, NULL, NULL, NULL),
(49, NULL, 'UA', '380952087725', NULL, 'ELENA', 'ELENA', NULL, NULL, 'SHEVCHENKO AVE 27, FLAT 74', 'Odesa', NULL, NULL, NULL, NULL),
(50, NULL, 'UA', '380933555222', NULL, 'ALI HASAN JABER ', 'ALI HASAN JABER ', NULL, NULL, 'SOMY ', 'Somyn', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `destination_entity`
--

CREATE TABLE `destination_entity` (
  `id` int(10) UNSIGNED NOT NULL,
  `destination_entity` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `express_mawb`
--

CREATE TABLE `express_mawb` (
  `id` int(10) UNSIGNED NOT NULL,
  `al_code` varchar(32) DEFAULT NULL,
  `flt_no` varchar(32) DEFAULT NULL,
  `etd` varchar(64) DEFAULT NULL,
  `etd_time` varchar(32) DEFAULT NULL,
  `eta` varchar(64) DEFAULT NULL,
  `eta_time` varchar(32) DEFAULT NULL,
  `via` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exspress_hawb`
--

CREATE TABLE `exspress_hawb` (
  `id` int(10) UNSIGNED NOT NULL,
  `al_code` varchar(32) DEFAULT NULL,
  `flt_no` varchar(32) DEFAULT NULL,
  `etd` varchar(32) DEFAULT NULL,
  `etd_time` varchar(32) DEFAULT NULL,
  `eta` varchar(32) DEFAULT NULL,
  `eta_time` varchar(32) DEFAULT NULL,
  `via` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exspress_hawb`
--

INSERT INTO `exspress_hawb` (`id`, `al_code`, `flt_no`, `etd`, `etd_time`, `eta`, `eta_time`, `via`) VALUES
(1, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(2, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(3, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(4, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(5, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(6, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(7, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(8, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(9, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(10, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(11, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(12, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(13, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(14, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(15, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(16, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(17, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(18, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(19, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(20, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(21, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(22, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(23, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(24, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(25, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(26, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(27, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(28, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(29, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(30, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(31, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(32, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(33, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(34, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(35, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(36, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(37, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(38, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(39, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(40, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(41, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(42, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(43, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(44, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(45, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(46, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(47, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(48, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(49, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL),
(50, NULL, NULL, NULL, NULL, '2017-12-02 11:20:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hawb`
--

CREATE TABLE `hawb` (
  `id` bigint(20) NOT NULL,
  `origin` varchar(32) DEFAULT NULL,
  `destination` varchar(32) DEFAULT NULL,
  `manifest` varchar(32) DEFAULT NULL,
  `snipper_id` int(10) UNSIGNED DEFAULT NULL,
  `consignee_id` int(10) UNSIGNED DEFAULT NULL,
  `box_id` int(10) UNSIGNED DEFAULT NULL,
  `money_id` int(10) UNSIGNED DEFAULT NULL,
  `mawb_id` varchar(32) DEFAULT NULL,
  `ukr_shipment_id` varchar(32) DEFAULT NULL,
  `express_id` int(10) UNSIGNED DEFAULT NULL,
  `excel` tinyint(1) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hawb`
--

INSERT INTO `hawb` (`id`, `origin`, `destination`, `manifest`, `snipper_id`, `consignee_id`, `box_id`, `money_id`, `mawb_id`, `ukr_shipment_id`, `express_id`, `excel`, `barcode`, `user_id`) VALUES
(3455257271, 'CCU', NULL, NULL, 15, 15, 15, 15, NULL, NULL, 15, 1, '0500100286516', 1),
(3468344891, 'BGW', NULL, NULL, 50, 50, 50, 50, NULL, NULL, 50, 1, NULL, 1),
(3476001701, 'EVN', NULL, NULL, 41, 41, 41, 41, NULL, NULL, 41, 1, NULL, 1),
(3484627904, 'DXB', NULL, NULL, 43, 43, 43, 43, NULL, NULL, 43, 1, NULL, 1),
(3570184321, 'KRT', NULL, NULL, 42, 42, 42, 42, NULL, NULL, 42, 1, NULL, 1),
(3583627434, 'DHA', NULL, NULL, 48, 48, 48, 48, NULL, NULL, 48, 1, NULL, 1),
(30263288311, 'JFK', NULL, NULL, 45, 45, 45, 45, NULL, NULL, 45, 1, NULL, 1),
(30339665974, 'BAH', NULL, NULL, 40, 40, 40, 40, NULL, NULL, 40, 1, NULL, 1),
(30937150042, 'BNK', NULL, NULL, 47, 47, 47, 47, NULL, NULL, 47, 1, NULL, 1),
(31087789386, 'DAM', NULL, NULL, 46, 46, 46, 46, NULL, NULL, 46, 1, NULL, 1),
(31097870370, 'KZT', NULL, NULL, 49, 49, 49, 49, NULL, NULL, 49, 1, NULL, 1),
(31125614170, 'TBS', NULL, NULL, 44, 44, 44, 44, NULL, NULL, 44, 1, NULL, 1),
(31439736661, 'DXB', NULL, NULL, 39, 39, 39, 39, NULL, NULL, 39, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mawb`
--

CREATE TABLE `mawb` (
  `id` bigint(20) NOT NULL,
  `org_entity` varchar(64) DEFAULT NULL,
  `org_port` varchar(32) DEFAULT NULL,
  `carrier` varchar(64) DEFAULT NULL,
  `mawn_no` varchar(32) DEFAULT NULL,
  `origin_branch` varchar(255) DEFAULT NULL,
  `destination_port` varchar(255) DEFAULT NULL,
  `destination_entity` varchar(64) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `mode_of_transport` varchar(255) DEFAULT NULL,
  `no_of_baby_bags` varchar(64) DEFAULT NULL,
  `no_of_master_bags` varchar(64) DEFAULT NULL,
  `ttc_bags_weight` varchar(64) DEFAULT NULL,
  `actual_weight` varchar(64) DEFAULT NULL,
  `hawbs_weight` varchar(64) DEFAULT NULL,
  `hawbs_cube` varchar(64) DEFAULT NULL,
  `remarks` varchar(64) DEFAULT NULL,
  `linehaul_supplier` varchar(255) DEFAULT NULL,
  `srr` varchar(255) DEFAULT NULL,
  `custom_reference` varchar(255) DEFAULT NULL,
  `exspress_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `sub` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `money`
--

CREATE TABLE `money` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment` varchar(128) DEFAULT NULL,
  `srn_no` varchar(128) DEFAULT NULL,
  `collection_ref` varchar(64) DEFAULT NULL,
  `collect_amt` varchar(64) DEFAULT NULL,
  `cod_amt` varchar(64) DEFAULT NULL,
  `cash_amt` varchar(64) DEFAULT NULL,
  `awb_reference` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money`
--

INSERT INTO `money` (`id`, `payment`, `srn_no`, `collection_ref`, `collect_amt`, `cod_amt`, `cash_amt`, `awb_reference`) VALUES
(15, NULL, NULL, NULL, '127', NULL, NULL, NULL),
(27, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(28, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(29, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(30, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(31, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(32, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(33, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(34, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(35, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(36, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(37, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(38, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(39, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(40, 'CASH', NULL, NULL, '0', NULL, NULL, NULL),
(41, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(42, 'CASH', NULL, NULL, '0', NULL, NULL, NULL),
(43, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(44, 'ACCT', NULL, NULL, '0', NULL, NULL, NULL),
(45, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(46, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(47, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(48, 'CASH', NULL, NULL, '0', NULL, NULL, NULL),
(49, NULL, NULL, NULL, '0', NULL, NULL, NULL),
(50, NULL, NULL, NULL, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `snipper`
--

CREATE TABLE `snipper` (
  `id` int(10) UNSIGNED NOT NULL,
  `account` varchar(32) DEFAULT NULL,
  `contry` varchar(16) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `name_s` varchar(100) DEFAULT NULL,
  `sent_by` varchar(100) DEFAULT NULL,
  `ref` varchar(64) DEFAULT NULL,
  `ref2` varchar(64) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `zip` int(10) UNSIGNED DEFAULT NULL,
  `ukr_address_id` varchar(32) DEFAULT NULL,
  `ukr_client_id` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `snipper`
--

INSERT INTO `snipper` (`id`, `account`, `contry`, `phone`, `phone2`, `name_s`, `sent_by`, `ref`, `ref2`, `address`, `city`, `state`, `zip`, `ukr_address_id`, `ukr_client_id`) VALUES
(15, NULL, 'IN', '380931489522', NULL, 'Artemiy Karkusha', NULL, NULL, NULL, NULL, 'Бровари', NULL, 7401, NULL, NULL),
(27, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, NULL, 'AE', NULL, NULL, 'Express Union General Trading LLC', NULL, '861', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, NULL, 'BH', NULL, NULL, 'Irina Duisimbekova', NULL, '97339566254', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, NULL, 'AM', NULL, NULL, 'Meliora Farm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, NULL, 'SD', NULL, NULL, 'HAMDI MOHAMED HAMED ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, NULL, 'AE', NULL, NULL, 'WAY IN INTL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, NULL, 'GE', NULL, NULL, 'TBC BANK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, NULL, 'US', NULL, NULL, 'ITS', NULL, '1028953690000795', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, NULL, 'SY', NULL, NULL, 'Mahmoud  Mohamad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, NULL, 'TH', NULL, NULL, 'GDA CO.,LTD.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, NULL, 'SA', NULL, NULL, 'Ramil Cruz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, NULL, 'KZ', NULL, NULL, 'Bekk Courier Kazakhstan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, NULL, 'IQ', NULL, NULL, 'HASAN SABER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `law` varchar(32) DEFAULT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `firstname`, `lastname`, `law`, `token`) VALUES
(1, 'maestro', 'aae070341d', 'Artemiy', 'Karkusha', 'level_3', 'd8a5459-ds3ca-9cbt3ecb9-fa3ad26970'),
(2, 'besame', 'besame', 'oleksandr', 'Khraban', 'level_3', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_box_id` (`id`);

--
-- Indexes for table `consignee`
--
ALTER TABLE `consignee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_consignee_id` (`id`);

--
-- Indexes for table `destination_entity`
--
ALTER TABLE `destination_entity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `express_mawb`
--
ALTER TABLE `express_mawb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exspress_hawb`
--
ALTER TABLE `exspress_hawb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hawb`
--
ALTER TABLE `hawb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_hawb_snipper_id` (`snipper_id`),
  ADD KEY `idx_hawb_consignee_id` (`consignee_id`),
  ADD KEY `idx_hawb_box_id` (`box_id`),
  ADD KEY `idx_hawb_money_id` (`money_id`),
  ADD KEY `idx_hawb_mawb_id` (`mawb_id`),
  ADD KEY `idx_hawb_express_id` (`express_id`);

--
-- Indexes for table `mawb`
--
ALTER TABLE `mawb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_mawb_exspress_id` (`exspress_id`),
  ADD KEY `idx_mawb_user_id` (`user_id`);

--
-- Indexes for table `money`
--
ALTER TABLE `money`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_money_id` (`id`),
  ADD KEY `idx_money_id_0` (`id`);

--
-- Indexes for table `snipper`
--
ALTER TABLE `snipper`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_snipper_id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `box`
--
ALTER TABLE `box`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `consignee`
--
ALTER TABLE `consignee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `destination_entity`
--
ALTER TABLE `destination_entity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `express_mawb`
--
ALTER TABLE `express_mawb`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exspress_hawb`
--
ALTER TABLE `exspress_hawb`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `hawb`
--
ALTER TABLE `hawb`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;

--
-- AUTO_INCREMENT for table `mawb`
--
ALTER TABLE `mawb`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `money`
--
ALTER TABLE `money`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `snipper`
--
ALTER TABLE `snipper`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hawb`
--
ALTER TABLE `hawb`
  ADD CONSTRAINT `fk_hawb_box` FOREIGN KEY (`box_id`) REFERENCES `box` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hawb_consignee` FOREIGN KEY (`consignee_id`) REFERENCES `consignee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hawb_exspress_hawb` FOREIGN KEY (`express_id`) REFERENCES `exspress_hawb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hawb_money` FOREIGN KEY (`money_id`) REFERENCES `money` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hawb_snipper` FOREIGN KEY (`snipper_id`) REFERENCES `snipper` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
