<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 6/4/2018
 * Time: 8:07 PM
 */
class Controller_manifest extends Controller
{

    function __construct()
    {
        $this->model = new model_manifest();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        $data[] = $this->model->getTable($this->model->getAllDbMawb());
        $this->view->generate('admin/manifest_view.php', 'admin/template_view.php', $data);
    }
}