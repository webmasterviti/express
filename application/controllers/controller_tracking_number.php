<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/1/2018
 * Time: 12:11 PM
 */

class controller_tracking_number extends Controller{

    function __construct()
    {
        $this->model = new model_tracking_number();
        $this->view = new View();
    }


    function action_index()
    {
        if ($_POST){
            $data = $this->model->getData($_POST['number_delivery']);
            if($data === false){
                $this->view->generate('tracking_number_input_view.php', 'tracking_number_input_template.php',$data);
            }else{
                $this->view->generate('tracking_number_view.php', 'tracking_number_template.php',$data);
            }
        }else{
            $this->view->generate('tracking_number_input_view.php', 'tracking_number_input_template.php');
        }
    }

}