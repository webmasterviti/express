<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 11.05.2018
 * Time: 17:24
 */

class Controller_Login extends Controller
{

    function __construct()
    {
        $this->view = new View();
    }


    function action_index()
    {
        $this->view->generate('login_view.php', 'template_view.php');
    }
}