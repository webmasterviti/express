<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/23/2018
 * Time: 6:47 PM
 */
class controller_user_edit extends Controller
{

    function __construct()
    {
        $this->model = new Model_user_edit();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST){
            if(isset($_POST['update_user'])) {
                $data['update_user'] = $this->model->updateUser($_POST);
                unset($_POST['update_user']);
            }
            if(isset($_POST['delete_user'])) {
                $data['delete_user'] = $this->model->deleteUser($_POST);
                unset($_POST['delete_user']);
            }
        }
        //create div card about all user
        $data ['allCard'] = $this->model->generateCard();
        $this->view->generate('admin/user_edit_view.php', 'admin/template_view.php',$data);;
    }
}