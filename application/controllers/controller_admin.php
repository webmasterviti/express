<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 01.05.2018
 * Time: 13:33
 */

class controller_admin extends Controller
{

    function __construct()
    {
        $this->model = new model_admin();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if(isset($_POST['download_excel'])){
            $this->model->downloadExcel();
            unset($_POST);
        }
        $this->view->generate('admin/main_view.php', 'admin/template_view.php',$data);
    }
}