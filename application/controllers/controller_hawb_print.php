<?php

/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/23/2018
 * Time: 6:43 PM
 */
class controller_hawb_print extends Controller
{

    function __construct()
    {
        $this->model = new model_hawb_print();
        $this->view = new View();
    }

    function action_index()
    {

        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST) {
            if ($_POST['search']) {
                $data['search'] = $this->model->seaview($_POST['search']);
            }
            if ($_POST['select']) {
                $data['search'] = $this->model->getAllHawb($_POST['select']);
            }
            if (!$data['search'] == 0) {
                if ($data['search']) {
                    $this->view->generate('admin/invoice_view.php', 'admin/invoiceTemplate_view.php', $data);
                } else {
                    $this->view->generate('admin/hawb_print_view.php', 'admin/template_view.php', $data);
                }
            } else {
                $data['select'] = $this->model->getSelect();
                $data['search'] = "Number or branch available";
                $this->view->generate('admin/hawb_print_view.php', 'admin/template_view.php', $data);
            }
        } else {
            $data['select'] = $this->model->getSelect();
            $this->view->generate('admin/hawb_print_view.php', 'admin/template_view.php', $data);
        }
    }
}