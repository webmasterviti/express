<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 17.05.2018
 * Time: 21:12
 */

class controller_ukrposhta extends Controller
{

    function __construct()
    {
        $this->model = new model_ukrposhta();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if($_POST) {
            $shipment = $this->model->addForwarding($this->model->getData($_POST['search']));
            $this->model->addrLabel($shipment['uuid']);
            exit;
        }
        //$this->model->addrLabel($shipment['uuid']);

        //print_r($shipment);
        //$this->model->pdf();
        /*if ($_POST){
            if(isset($_POST['add_label'])){
                $this->model->addrLabel($shipment['uuid']);
            }
            if(isset($_POST['postal_note'])){
                $this->model->postalNote($shipment['uuid']);
            }
        }*/
        $this->view->generate('admin/ukrposhta_view.php', 'admin/template_view.php',$data);
    }
}