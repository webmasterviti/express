<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 17.05.2018
 * Time: 22:44
 */
class controller_mawb_add extends Controller
{

    function __construct()
    {
        $this->model = new model_mawb_add();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        array_push($data,$this->model->partInfoHawb());
        if($_POST) {
            $ttc_bags_weight = $_POST['ttc_bags_weight'] . ' ' . $_POST['ttc_bags_weight_2'];
            $hawbs_cube = $_POST['hawbs_cube'] . ' ' . $_POST['hawbs_cube_2'];
            $id_mawb = $this->model->addInTable($_POST['al_code'],$_POST['flt_no'],$_POST['etd_date'],$_POST['etd_time'],$_POST['eta_date'],$_POST['eta_time'],$_POST['org_entity'],$_POST['org_port'],$_POST['carrier'],$_POST['mawb_no'],$_POST['origin_branch'],$_POST['destination_port'],$_POST['destination_entity'],$_POST['branch'],$_POST['mode_of_transport'],$_POST['no_of_baby_bags'],$_POST['no_of_master_bags'],$ttc_bags_weight,$_POST['actual_weight'],$_POST['hawbs_wight'],$hawbs_cube,$_POST['remarks'],$_POST['linehaur_supplier'],$_POST['srr'],$_POST['custom_reference'],$_POST['sub']);
            $data[] = ['resultInsert' => $this->model->addManifest($id_mawb,$_POST)];
            $this->model->createMawbpdf($id_mawb);
            $data['resultMail'] = $this->model->sendMail(EMAIL);
            $this->view->generate('admin/mawb_add_view.php', 'admin/template_view.php', $data);
        } else {
            $this->view->generate('admin/mawb_add_view.php', 'admin/template_view.php', $data);
        }
    }

    function action_down(){
        $this->auth();
        $data = $this->model->allInfoUser();
        $this->view->generate('admin/mawb_add_view.php', 'admin/template_view.php',$data);
    }
}