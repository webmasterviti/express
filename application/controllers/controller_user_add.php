<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/23/2018
 * Time: 6:46 PM
 */
class controller_user_add extends Controller
{

    function __construct()
    {
        $this->model = new model_user_add();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST){
            $data['add_user'] = $this->model->userAdd($_POST);
        }
        $this->view->generate('admin/user_add_view.php', 'admin/template_view.php',$data);
        unset($_POST);
    }
}