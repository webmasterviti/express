<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 7/26/2018
 * Time: 1:19 PM
 */

class controller_api extends Controller
{


    function __construct()
    {
        $this->model = new model_api();
        $this->view = new View();
    }

    function action_index()
    {
        //$this->view->generate('main_view.php', 'template_index_view.php');
    }

    function action_statuses(){
        $data = $this->model->getStatuses($_GET['token'],$_GET['barcode']);
        echo json_encode($data);
    }

    function action_statuses_last(){
        $data = $this->model->getStatuses($_GET['token'],$_GET['barcode'],'last');
        echo json_encode($data);
    }
}