<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 04.06.2018
 * Time: 16:21
 */

class controller_mawb_search extends Controller
{


    function __construct()
    {
        $this->model = new model_mawb_search();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if($_POST){
            $data['search'] = $this->model->seaview($_POST["search"]);
            $this->view->generate('admin/mawb_search_view.php', 'admin/template_view.php', $data);
        } else{
            $this->view->generate('admin/mawb_search_view.php', 'admin/template_view.php',$data);
        }

    }

}