<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/1/2018
 * Time: 3:08 PM
 */

class controller_hawb_status extends Controller
{

    function __construct()
    {
        $this->model = new model_hawb_status();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST) {
            if (!empty($_POST['search'])) {
                if (isset($_POST['search'])) {
                    $data['test'] = $this->model->statusTest($_POST['search']);
                    if ($data['test'] === true) {
                        $data['status'] = $this->model->getData($_POST['search']);
                    }
                }
                if (isset($_POST['add_status'])) {
                    $data['add_status'] = $this->model->setData($_POST);
                    $data['status'] = $this->model->getData($_POST['hawb_id']);
                }
                if (isset($_POST['delete_status'])) {
                    $data['delete_status'] = $this->model->statusDelete($_POST['hawb_id']);
                    $data['status'] = $this->model->getData($_POST['hawb_number']);
                }
            } elseif (isset($_POST['add_status_batch'])){
                $data['add_status_batch'] = $this->model->setStatusForBatch($_POST);
                $data['countUpdStatParcel'] = $this->model->countUpdateParcelForBatch;
                $data['batch'] = $_POST['select'];
            }
        }
        $data['select'] = $this->model->getSelect();
        $this->view->generate('admin/hawb_status_view.php', 'admin/template_view.php', $data);
    }
}