<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 30.05.2018
 * Time: 21:47
 */

class controller_mawb_edit extends Controller
{


    function __construct()
    {
        $this->model = new model_mawb_edit();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if ($_POST) {
            if (isset($_POST['search'])) {
                session_start();
                $_SESSION['search']= $_POST["search"];
                $data['search'] = $this->model->seaview($_POST["search"]);
                $this->view->generate('admin/mawb_edit_view.php', 'admin/template_view.php', $data);
            }
            if (isset($_POST['org_entity'])) {
                $ttc_bags_weight = $_POST['ttc_bags_weight'] . ' ' . $_POST['ttc_bags_weight_2'];
                $hawbs_cube = $_POST['hawbs_cube'] . ' ' . $_POST['hawbs_cube_2'];
                $search = $_SESSION['search'];
                $this->model->update($search,$_POST['al_code'],$_POST['flt_no'],$_POST['etd_date'],$_POST['etd_time'],$_POST['eta_date'],$_POST['eta_time'],$_POST['org_entity'],$_POST['org_port'],$_POST['carrier'],$_POST['mawb_no'],$_POST['origin_branch'],$_POST['destination_port'],$_POST['destination_entity'],$_POST['branch'],$_POST['mode_of_transport'],$_POST['no_of_baby_bags'],$_POST['no_of_master_bags'],$ttc_bags_weight,$_POST['actual_weight'],$_POST['hawbs_wight'],$hawbs_cube,$_POST['remarks'],$_POST['linehaur_supplier'],$_POST['srr'],$_POST['custom_reference'],$_POST['sub']);
                $this->view->generate('admin/mawb_edit_view.php', 'admin/template_view.php', $data);
            }
        }

        else {
            $this->view->generate('admin/mawb_edit_view.php', 'admin/template_view.php', $data);
        }

    }


}