<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/23/2018
 * Time: 6:42 PM
 */
class controller_hawb_excel extends Controller
{
    function __construct()
    {
        $this->model = new model_hawb_excel();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if (isset($_POST['upload_new'])){
            $data['addFromExcel'] = $this->model->addFromNewExcel();
        }
        if (isset($_POST['upload_old'])){
            $data['addFromOldExcel'] = $this->model->addFromOldExcel($data['id'],$_POST['batch_name']);
            $data['countDelivery'] = $this->model->countAddDelivery;
        }
        $this->view->generate('admin/hawb_excel_view.php', 'admin/template_view.php',$data);
        unset($_POST['upload_submit']);
    }
}