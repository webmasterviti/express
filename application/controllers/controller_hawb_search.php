<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 04.06.2018
 * Time: 16:21
 */

class controller_hawb_search extends Controller
{

    function __construct()
    {
        $this->model = new model_hawb_search();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if($_POST){
            if(isset($_POST['search_but'])) {
                $data['search'] = $this->model->seaview($_POST["search"]);
            }
            if(isset($_POST['search_excel'])) {
                $data['search_excel'] = $this->model->withdraw();
            }
            if(isset($_POST['invoice_id'])){
                $this->model->printInvoice($_POST['invoice_id']);
                exit;
            }
            $this->view->generate('admin/hawb_search_view.php', 'admin/template_view.php', $data);
        } else{
            $this->view->generate('admin/hawb_search_view.php', 'admin/template_view.php',$data);
        }

    }
}