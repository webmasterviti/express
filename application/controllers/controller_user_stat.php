<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/23/2018
 * Time: 6:45 PM
 */
class controller_user_stat extends Controller
{
    function __construct()
    {
        $this->model = new model_user_stat();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        $this->view->generate('admin/user_stat_view.php', 'admin/template_view.php',$data);
    }
}