<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 17.05.2018
 * Time: 22:51
 */
class controller_admin_api extends Controller
{
    function action_index()
    {
        $this->auth();
        $this->view->generate('admin/api_view.php', 'admin/template_view.php');
    }
}