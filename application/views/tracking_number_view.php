

<div class="container-fluid text-center mb-5">
    <div class="row content">
        <div class="col-sm-2 sidenav">
        </div>
        <div class="col-sm-8 text-left">
            <h1><a href="/tracking_number">Назад</a></h1>
            <div class="card" style="margin-top: 20px;">
                <h2 class="card-header">Shipment Summary</h2>
                <div class="card-body">
                    <h5 class="card-title">Tracking Number:<?=$data['id']?></h5>
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <th scope="col">From</th>
                            <th scope="col">To</th>
                            <th scope="col">Current Activity</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?=$data['snipper_id']['contry'];?> <br>
                                <?=$data['snipper_id']['city'];?>
                            </td>
                            <td><?=$data['consignee_id']['contry'];?> <br>
                                <?=$data['consignee_id']['city'];?>
                            <td>
                                <?=$data['status'];?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="margin-top: 20px;">
                <h2 class="card-header">History</h2>
                <div class="card-body">
                    <h1 class="card-title">Where Your Shipment Has Been:</h1>
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Location</th>
                            <th scope="col">Activity</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?=$data['express_id']?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card" style="margin-top: 20px;">
                <h2 class="card-header">Details</h2>
                <div class="card-body">
                    <h1 class="card-title">More About Your Shipment:</h1>
                    <div class="container">
                        <div class="row card-details mt-5">
                            <div class="col-3">
                                Weight:<br>
                                <?=$data['box_id']['weight'];?> KG
                            </div>
                            <div class="col-3">
                                Service<br>
                                N/A
                            </div>
                            <div class="col-6">
                                No. Of Items <br>
                                <?=$data['box_id']['pcs'];?>
                            </div>
                        </div>
                        <div class="row mt-5 card-details">
                            <div class="col-6">
                                Origin <br>
                                <?=$data['consignee_id']['contry'];?> <br>
                                <?=$data['consignee_id']['city'];?>
                            </div>
                            <div class="col-6">
                                Origin <br>
                                <?=$data['consignee_id']['contry'];?> <br>
                                <?=$data['consignee_id']['city'];?>
                            </div>
                        </div>
                        <div class="row mt-5 card-details">
                            <div class="col-12">
                                Description:
                                <?=$data['box_id']['description'];?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-2 sidenav">
        </div>
    </div>
</div>