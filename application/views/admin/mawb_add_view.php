
<body class="sidebar-mini fixed">
<div class="wrapper">
    <!-- Navbar-->
    <header class="main-header hidden-print"><a class="logo" href="/admin">Inter Express</a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!--Notification Menu-->
                    <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o fa-lg"></i></a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have 4 new notifications.</li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block">2min ago</span></div></a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block">2min ago</span></div></a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block">2min ago</span></div></a></li>
                            <li class="not-footer"><a href="#">See all notifications.</a></li>
                        </ul>
                    </li>
                    <!-- User Menu-->
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                            <li><a href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                            <li><a href="/ logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image"><img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image"></div>
                <div class="pull-left info">
                    <p><?=$data['firstname'] .' '. $data['lastname']?></p>
                    <p class="designation">
                        Position</p>
                </div>
            </div>
            <!-- Sidebar Menu-->
            <ul class="sidebar-menu">
                <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>HAWB</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="/hawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                        <li><a href="/hawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                        <li><a href="/hawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        <li><a href="/hawb_status"><i class="fa fa-circle-o"></i>Status</a></li>
                        <li><a href="/hawb_excel"><i class="fa fa-circle-o"></i>Excel add</a></li>
                        <li><a href="/hawb_print"><i class="fa fa-circle-o"></i>Print cod</a></li>
                    </ul>
                </li>
                <?if($data['userLaw'] >= 2) :?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>MAWB</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/mawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                            <li><a href="/mawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                            <li><a href="/mawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        </ul>
                    </li>
                    <li><a href="/manifest"><i class="fa fa-edit"></i><span>Manifest</span></a></li>
                    <li><a href="/ukrposhta"><i class="fa fa-edit"></i><span>Express Ukrposhta</span></a></li>
                <?endif;?>
                <?if($data['userLaw'] == 3) :?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>Admin</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/user_stat"><i class="fa fa-circle-o"></i>Statictic</a></li>
                            <li><a href="/user_add"><i class="fa fa-circle-o"></i>Add user</a></li>
                            <li><a href="/user_edit"><i class="fa fa-circle-o"></i>Edit user</a></li>
                        </ul>
                    </li>
                <?endif;?>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <?if($data['userLaw']>=2){?>
        <div class="page-title">
            <div>
                <h1><i class="fa fa-dashboard"></i>MAWB add</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="/admin">Main</a></li>
                </ul>
            </div>
        </div>
            <form action="mawb_add" method="post">
        <div class="row">
            <?if($data[1]['resultInsert'] === TRUE):?>
            <div class="alert alert-success" role="alert">
                Data loaded successfully
            </div>
            <?endif;?>
            <?if($data[1]['resultInsert'] === FALSE):?>
            <div class="alert alert-danger" role="alert">
                Data not loaded
            </div>
            <?endif;?>
            <?if($data['resultMail'] === TRUE):?>
                <div class="alert alert-success" role="alert">
                    E-mail was sent
                </div>
            <?endif;?>
            <?if($data['resultMail'] === FALSE):?>
                <div class="alert alert-success" role="alert">
                    E-mail had benn sent back
                </div>
            <?endif;?>
            <div class="col-md-2 mt-10">
                <br>
                <b>Master Airwaybill</b>
            </div>
            <div class="col-md-2">
                <label for="org_entity">Org.Entity</label>
                <input class="form-control" id="org_entity" name="org_entity" placeholder="" value="" required=""
                       type="text">
            </div>
            <div class="col-md-1">
                <label for="carrier">Carrier</label>
                <input class="form-control" id="carrier" name="carrier" placeholder="" value="" required=""
                       type="text">
            </div>
            <div class="col-md-1">
                <br>
                <button type="button" class="btn btn-info">...</button>
            </div>
            <div class="col-md-2">
                <label for="org_port">Org.Port</label><br>
                <select class="form-control" id="org_port" name="org_port" required="">
                    <option value="KBR" selected>KBR</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="mawb_no">MAWB No.</label>
                <input class="form-control" id="mawb_no" name="mawb_no" placeholder="" value="" required=""
                       type="text">
            </div>
            <div class="col-md-2">
                <label for="sub">Sub</label>
                <input class="form-control" id="sub" name="sub" placeholder="" value="" required=""
                       type="text">
            </div>
        </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="origin_branch">Origin Branch</label>
                </div>
                <div class="col-md-2 pt-10">
                    <select class="form-control" id="origin_branch" name="origin_branch" required="">
                        <option value="26368_Kiev" selected>26368-Kiev</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="destination_port">Destination Port</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="destination_port" name="destination_port" placeholder="" value="" required=""
                           type="text">
                </div>
                <div class="col-md-2 pt-10">
                    <button type="button" class="btn btn-info">...</button>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="destination_entity">Destination Entity</label>
                </div>
                <div class="col-md-3 pt-10">
                    <select class="form-control" id="destination_entity" name="destination_entity" required="">
                        <option value="Aramex/JFK" selected>Aramex/JFK</option>
                    </select>
                </div>
                <div class="col-md-1 pt-10">
                    <label for="branch">Branch</label>
                </div>
                <div class="col-md-3 pt-10">
                    <select class="form-control" id="branch" name="branch" required="">
                        <option value="14420_new_york_office" selected>14420 - New York Office</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="mode_of_transport">Mode of Transport</label>
                </div>
                <div class="col-md-3 pt-10">
                    <select class="form-control" id="mode_of_transport" name="mode_of_transport" required="">
                        <option value="frt" selected>FRT</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="no_of_baby_bags">No. of Baby Bags</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="no_of_baby_bags" name="no_of_baby_bags" placeholder="" value="" required=""
                           type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="no_of_master_bags">No. of Master Bags(TTC)</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="no_of_master_bags" name="no_of_master_bags" placeholder="" value="" required=""
                           type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="ttc_bags_weight">TTC Bags Weight</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="ttc_bags_weight" name="ttc_bags_weight" placeholder="" value="" required=""
                           type="text">
                </div>
                <div class="col-md-2 pt-10">
                    <select class="form-control" id="ttc_bags_weight_iz" name="ttc_bags_weight_2" required="">
                        <option value="kg" selected>Kg</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="actual_weight">Actual Weight</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="actual_weight" name="actual_weight" placeholder="" value="" required=""
                    type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="hawbs_wight">HAWBs Weight</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="hawbs_wight" name="hawbs_wight" placeholder="" value="" required=""
                           type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="hawbs_cube">HAWBs Cube</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="hawbs_cube" name="hawbs_cube" placeholder="" value="" required=""
                           type="text">
                </div>
                <div class="col-md-2 pt-10">
                    <select class="form-control" id="hawbs_cube_iz" name="hawbs_cube_2" required="">
                        <option value="m3" selected>m3</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="remarks">Remarks</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="remarks" name="remarks" placeholder="" value="" required=""
                           type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="linehaur_supplier">Linehaur supplier</label>
                </div>
                <div class="col-md-2 pt-10">
                    <select class="form-control" id="linehaur_supplier" name="linehaur_supplier" required="">
                        <option value="selected" selected>-Selected-</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="srr">SRR</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="srr" name="srr" placeholder="" value="" required=""
                           type="text">
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="custom_reference">Custom Reference</label>
                </div>
                <div class="col-md-2 pt-10">
                    <input class="form-control" id="custom_reference" name="custom_reference" placeholder="" value="" required=""
                           type="text">
                </div>
            </div>

            <hr style="border:1px solid darkgrey">

            <div class="row">
                <div class="col-md-1 mt-10">
                    <br>
                    <b>Flight</b>
                </div>
                <div class="col-md-2">
                    <label for="al_code">A/L Code</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="al_code" name="al_code" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-info">...</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <label for="flt_no">Flt No</label>
                    <input class="form-control" id="flt_no" name="flt_no" placeholder="" value="" required=""
                           type="text">
                </div>
                <div class="col-md-3">
                    <label for="etd_date">ETD(m/d/y h:mm)</label><br>
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="etd_date" name="etd_date" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info">...</button>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="etd_time" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="eta_date">ETA(m/d/y h:mm)</label><br>
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" id="eta_date" name="eta_date" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info">...</button>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="eta_time" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                </div>
            </div>
            <hr style="border:1px solid darkgrey">
                <div class="row">
                    <table class="table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Add</th>
                            <th scope="col">HAWB</th>
                            <th scope="col">Origin</th>
                            <th scope="col">Snipper Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Consignee Name</th>
                            <th scope="col">Phone</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?
                            $infoHawb = $data[0];
                            for($i = 0; $i < count($infoHawb); $i++)
                            {
                                ?>
                                <tr>
                                <th scope="row">
                                    <input id='<?=$i+1;?>' name="<?=$infoHawb[$i]['id']?>" type='checkbox'>
                                    <label for='<?=$i+1;?>'>
                                        <?=$i+1;?>
                                    </label>
                                </th>
                                <td><?=$infoHawb[$i]['id']?></td>
                                <td><?=$infoHawb[$i]['origin']?></td>
                                <td><?=$infoHawb[$i]['snipper_id']['name_s']?></td>
                                <td><?=$infoHawb[$i]['snipper_id']['phone']?></td>
                                <td><?=$infoHawb[$i]['consignee_id']['name_c']?></td>
                                <td><?=$infoHawb[$i]['consignee_id']['phone']?></td>
                                </tr>
                                <?
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            <hr style="border:1px solid darkgrey">
            <div class="row">
                <div class="col-md-2 pt-10">
                    <label for="created_dt">Creared</label>
                </div>
                <div class="col-md-3 pt-10">
                    <input class="form-control" id="created_dt" name="created_dt" placeholder="" value="<?=date("Y-m-d H:i:s");?>" required=""
                           readonly  type="text" >
                </div>
                <div class="col-md-2 pt-10">
                    <label for="created_by">By</label>
                </div>
                <div class="col-md-3 pt-10">
                    <input class="form-control" id="created_by" name="created_by" placeholder="" value="<?=$data['firstname'] . ' ' . $data['lastname'];?>" required=""
                           readonly type="text" >
                </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block mt-10" type="submit">Continue to checkout</button>
            </form>
        <?}else{?>
            <div class="page-title">
                <div>
                    <h1 style="color:red">You don't have accesss this page</h1>
                </div>
                <div>
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home fa-lg"></i></li>
                        <li><a href="/admin">Main</a></li>
                    </ul>
                </div>
            </div>
        <?}?>
    </div>
</div>