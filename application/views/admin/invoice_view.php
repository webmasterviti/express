<?php header('Content-Type: text/html; charset=utf-8'); ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Накладная</title>
    </head>
    <link  rel="stylesheet" type="text/css" href="css/invoice.css">
    <body>

    <?php
    $item = $data['search'];
    ?>

    <?php
    for($i = 0; $i< count($item) ; $i++):?>
        <div class="b-sticker">
            <table>
                <tr>
                    <td width="20%" class="customer-info">
                        <div> Origin:</div>
                        <div class="seller"><h2><?php echo $item[$i]['origin'];?></h2></div>
                    </td>
                    <td width="70%">
                        <div class="barcode"><?php echo barcode::code39($item[$i]['id']); ?></div>
                    </td>
                </tr>
            </table>
            <table>
                <tr class="line2">
                    <td width="50%">
                        <div><h1>Destination: <?php echo $item[$i]['destination']; ?></h1></div>
                    </td>
                    <td width="40%">
                        <div><p>Pickup Date: <?php echo $item[$i]['box']['pickup_dt']; ?></p></div>
                    </td>
                </tr>
            </table>
            <table>
                <tr class="line2">
                    <td class="right" width="33%">
                        <div><h1> <?php echo $item[$i]['box']['product_1']; ?></h1></div>
                    </td>
                    <td class="right" width="33%">
                        <div><h1> <?php echo $item[$i]['box']['product_2']; ?></h1></div>
                    </td>
                    <td width="33%">
                        <div><p> Payment: <span class="span"> <?php echo $item[$i]['money']['payment']; ?></span></p></div>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="right" width="80%">
                        <div class="line3">
                            <div class="display">
                                <div>Weight: <?php echo $item[$i]['box']['weight']; ?> </div>
                                <div> Pieces: <?php echo $item[$i]['box']['pcs']; ?> </div>
                                <div>Value: <?php echo $item[$i]['box']['goods_value']; ?></div>
                            </div>
                            <br>
                            <div class="display2">
                                <div>Description: <?php echo $item[$i]['box']['description']; ?></div>
                                <!--<div>Chargeable: <?php /*echo $item[$i]['chargeable']; */?> </div>-->
                            </div>
                        </div>
                        <div class="line3 ot">
                            <div><h4>From</h4></div>
                            <div>Country: <?=$item[$i]['snipper']['contry'];?><br></div>
                            <div>Name: <?=$item[$i]['snipper']['name_s'];?></div>
                            <div>City: <?=$item[$i]['snipper']['city'];?><br></div>
                            <div>Address: <?=$item[$i]['snipper']['address'];?><br></div>
                            <!--<div><br></div>
                            <div><br></div>
                            <div><br></div>-->
                            <div>Phone: <?=$item[$i]['snipper']['phone'];?></div>
                        </div>
                        <br>
                        <div class="line3 ot">
                            <div><h4>To</h4></div>
                            <div>Country: <?=$item[$i]['consignee']['contry'];?><br></div>
                            <div>Name: <?=$item[$i]['consignee']['name_c'];?></div>
                            <div>City: <?=$item[$i]['consignee']['city'];?><br></div>
                            <div>Address: <?=$item[$i]['consignee']['address'];?><br></div>
                            <!--<div><br></div>
                            <div><br></div>
                            <div><br></div>-->
                            <div>Phone: <?=$item[$i]['consignee']['phone'];?></div>
                        </div>
                    </td>
                    <td width="20%">
                        <div class="display3">
                            <div class="barcode1"><?php echo barcode::code39($item[$i]['id']) ?>
                            </div>
                        </div>
                    </td>

                </tr>
            </table>
            <!--<table>
                <div class="display2">
                    <div>Shpr Ref:</div>
                    <div>Cons Ref:</div>
                </div>
            </table>-->
        </div>
    <?php endfor;?>

    </body>
    </html>

<?php
class barcode {

    protected static $code39 = array(
        '0' => 'bwbwwwbbbwbbbwbw', '1' => 'bbbwbwwwbwbwbbbw',
        '2' => 'bwbbbwwwbwbwbbbw', '3' => 'bbbwbbbwwwbwbwbw',
        '4' => 'bwbwwwbbbwbwbbbw', '5' => 'bbbwbwwwbbbwbwbw',
        '6' => 'bwbbbwwwbbbwbwbw', '7' => 'bwbwwwbwbbbwbbbw',
        '8' => 'bbbwbwwwbwbbbwbw', '9' => 'bwbbbwwwbwbbbwbw',
        'A' => 'bbbwbwbwwwbwbbbw', 'B' => 'bwbbbwbwwwbwbbbw',
        'C' => 'bbbwbbbwbwwwbwbw', 'D' => 'bwbwbbbwwwbwbbbw',
        'E' => 'bbbwbwbbbwwwbwbw', 'F' => 'bwbbbwbbbwwwbwbw',
        'G' => 'bwbwbwwwbbbwbbbw', 'H' => 'bbbwbwbwwwbbbwbw',
        'I' => 'bwbbbwbwwwbbbwbw', 'J' => 'bwbwbbbwwwbbbwbw',
        'K' => 'bbbwbwbwbwwwbbbw', 'L' => 'bwbbbwbwbwwwbbbw',
        'M' => 'bbbwbbbwbwbwwwbw', 'N' => 'bwbwbbbwbwwwbbbw',
        'O' => 'bbbwbwbbbwbwwwbw', 'P' => 'bwbbbwbbbwbwwwbw',
        'Q' => 'bwbwbwbbbwwwbbbw', 'R' => 'bbbwbwbwbbbwwwbw',
        'S' => 'bwbbbwbwbbbwwwbw', 'T' => 'bwbwbbbwbbbwwwbw',
        'U' => 'bbbwwwbwbwbwbbbw', 'V' => 'bwwwbbbwbwbwbbbw',
        'W' => 'bbbwwwbbbwbwbwbw', 'X' => 'bwwwbwbbbwbwbbbw',
        'Y' => 'bbbwwwbwbbbwbwbw', 'Z' => 'bwwwbbbwbbbwbwbw',
        '-' => 'bwwwbwbwbbbwbbbw', '.' => 'bbbwwwbwbwbbbwbw',
        ' ' => 'bwwwbbbwbwbbbwbw', '*' => 'bwwwbwbbbwbbbwbw',
        '$' => 'bwwwbwwwbwwwbwbw', '/' => 'bwwwbwwwbwbwwwbw',
        '+' => 'bwwwbwbwwwbwwwbw', '%' => 'bwbwwwbwwwbwwwbw'
    );

    public static function code39($text) {
        if (!preg_match('/^[A-Z0-9-. $+\/%]+$/i', $text)) {
            throw new Exception('Ошибка ввода');
        }
        $number = $text;
        $text ="*" .strtoupper($text)."*";
        $length = strlen($text);
        $chars = str_split($text);
        $colors = '';

        foreach ($chars as $char) {
            $colors .= self::$code39[$char];
        }

        $html = '
            <div style=" float:left;">
            <div>';

        foreach (str_split($colors) as $i => $color) {
            if ($color=='b') {
                $html.='<SPAN style="BORDER-LEFT: 0.02in solid; DISPLAY: inline-block; HEIGHT: 1in;"></SPAN>';
            } else {
                $html.='<SPAN style="BORDER-LEFT: white 0.02in solid; DISPLAY: inline-block; HEIGHT: 1in;"></SPAN>';
            }
        }

        $html.='</div>
            <div style="float:left; width:100%;" align=center >'.$number.'</div></div>';
        //echo htmlspecialchars($html);
        echo $html;
    }

}