<?php
$col = 50;
$file = file("stat.log"); ?>

    <html>
    <head>
        <style type='text/css'>
            table {
                border-spacing: 0 10px;
                font-family: 'Open Sans', sans-serif;
                font-weight: bold;
            }

            th {
                padding: 10px 20px;
                background: #56433D;
                color: #F9C941;
                border-right: 2px solid;
                font-size: 0.9em;
            }

            th:first-child {

            }

            th:last-child {
                border-right: none;
            }

            td {
                vertical-align: middle;
                padding: 10px;
                font-size: 14px;
                text-align: center;
                border-top: 2px solid #56433D;
                border-bottom: 2px solid #56433D;
                border-right: 2px solid #56433D;
            }

            td:first-child {
                border-left: 2px solid #56433D;
                border-right: none;
            }

            td:nth-child(2) {
                text-align: left;
            }
        </style>
    </head>

<body>
<center>
<?php
if ($col > sizeof($file)) {
    $col = sizeof($file);
}
echo "Останні <b>" . $col . "</b> відвідування сайту:"; ?>

    <table class="table table-striped" width="680" cellspacing="1" cellpadding="1" border="0"
           STYLE="table-layout:fixed">
        <tr bgcolor="#eeeeee">
            <th class="zz" width="100"><b>Час та дата</b></th>
            <th class="zz" width="200"><b>Данні про відвідувачів</b></th>
            <th class="zz" width="100"><b>IP/проксі</b></th>
            <th class="zz" width="280"><b>Відвіданний URL</b></th>
            <th class="zz" width="100"><b>Доменне ім'я</b></th>
        </tr>

<?php
for ($si = sizeof($file) - 1; $si + 1 > sizeof($file) - $col; $si--) {
    $string = explode("|", $file[$si]);
    $q1[$si] = $string[0]; // дата и время
    $q2[$si] = $string[1]; // имя бота
    $q3[$si] = $string[2]; // ip бота
    $q4[$si] = $string[3]; // адрес посещения
    $q5[$si] = $string[4];
    echo '<tr bgcolor="#eeeeee">';
    echo '<td class="zz">' . $q1[$si] . '</td>';
    echo '<td class="zz">' . $q2[$si] . '</td>';
    echo '<td class="zz">' . $q3[$si] . '</td>';
    echo '<td class="zz">' . $q4[$si] . '</td>';
    echo '<td class="zz">' . $q5[$si] . '</td></tr>';
}
echo '</table>';
echo '<br>Переглянуті останні <a href=?col=100>100</a> <a href=?col=500>500</a>';
echo '<br><a href=?col=1000>1000</a> відвідувань.';
echo '<br>Переглянути <a href=?col=' . sizeof($file) . '>всі відвідування</a>.</center>';
echo '</body></html>';
?>