<script type="text/javascript">
    function setFocus() {
        document.getElementsByTagName("input")[0].focus();
    }
    document.addEventListener("DOMContentLoaded", setFocus);
</script>
<body class="sidebar-mini fixed">
    <div class="wrapper">
      <!-- Navbar-->
      <header class="main-header hidden-print"><a class="logo" href="/admin">Inter Express</a>
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
          <!-- Navbar Right Menu-->
          <div class="navbar-custom-menu">
            <ul class="top-nav">
              <!--Notification Menu-->
              <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o fa-lg"></i></a>
                <ul class="dropdown-menu">
                  <li class="not-head">You have 4 new notifications.</li>
                  <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                      <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block">2min ago</span></div></a></li>
                  <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                      <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block">2min ago</span></div></a></li>
                  <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                      <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block">2min ago</span></div></a></li>
                  <li class="not-footer"><a href="#">See all notifications.</a></li>
                </ul>
              </li>
              <!-- User Menu-->
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu">
                  <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                  <li><a href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                  <li><a href="/logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Side-Nav-->
      <aside class="main-sidebar hidden-print">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image"><img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image"></div>
              <div class="pull-left info">
                  <p><?=$data['firstname'] .' '. $data['lastname']?></p>
                  <p class="designation">
                      Position</p>
              </div>
          </div>
          <!-- Sidebar Menu-->
            <ul class="sidebar-menu">
                <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>HAWB</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="/hawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                        <li><a href="/hawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                        <li><a href="/hawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        <li><a href="/hawb_status"><i class="fa fa-circle-o"></i>Status</a></li>
                        <li><a href="/hawb_excel"><i class="fa fa-circle-o"></i>Excel add</a></li>
                        <li><a href="/hawb_print"><i class="fa fa-circle-o"></i>Print cod</a></li>
                    </ul>
                </li>
                <?if($data['userLaw'] >= 2) :?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>MAWB</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/mawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                            <li><a href="/mawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                            <li><a href="/mawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        </ul>
                    </li>
                    <li><a href="/manifest"><i class="fa fa-edit"></i><span>Manifest</span></a></li>
                    <li><a href="/ukrposhta"><i class="fa fa-edit"></i><span>Express Ukrposhta</span></a></li>
                <?endif;?>
                <?if($data['userLaw'] == 3) :?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>Admin</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/user_stat"><i class="fa fa-circle-o"></i>Statictic</a></li>
                            <li><a href="/user_add"><i class="fa fa-circle-o"></i>Add user</a></li>
                            <li><a href="/user_edit"><i class="fa fa-circle-o"></i>Edit user</a></li>
                        </ul>
                    </li>
                <?endif;?>
            </ul>
        </section>
      </aside>
      <div class="content-wrapper">
          <?if($data['userLaw'] >= 2){?>
        <div class="page-title">
          <div>
            <h1><i class="fa fa-dashboard"></i> Ukrposhta</h1>
          </div>
          <div>
            <ul class="breadcrumb">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li><a href="/admin">Main</a></li>
            </ul>
          </div>
        </div>
          <div class="row">
              <div class="col-md-12">
                  <h3>Введите номер накладной:</h3>
                  <form method="post" target="_blank" action="/ukrposhta" class="form-search form-inline" onsubmit="deleteValue()">
                      <input type="text" name="search" class="search-query" placeholder="# invoice" /> <button class="btn btn-success" type="submit">Create invoice</button> <hr>
                  </form>
              </div>
          </div>
      </div>
        <?}else{?>
            <div class="page-title">
                <div>
                    <h1 style="color:red">You don't have accesss this page</h1>
                </div>
                <div>
                    <ul class="breadcrumb">
                        <li><i class="fa fa-home fa-lg"></i></li>
                        <li><a href="/admin">Main</a></li>
                    </ul>
                </div>
            </div>
        <?}?>
    </div>
    <script>

        function deleteValue() {
            setTimeout(function(){
                var text = document.getElementsByTagName("input")[0];
                text.value = '';
            },1000);
        }
    </script>