<body class="sidebar-mini fixed">
<div class="wrapper">
    <!-- Navbar-->
    <header class="main-header hidden-print"><a class="logo" href="/admin">Inter Express</a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!--Notification Menu-->
                    <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown"
                                                              aria-expanded="false"><i
                                    class="fa fa-bell-o fa-lg"></i></a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have 4 new notifications.</li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-primary"></i><i
                                                    class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-danger"></i><i
                                                    class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-success"></i><i
                                                    class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li class="not-footer"><a href="#">See all notifications.</a></li>
                        </ul>
                    </li>
                    <!-- User Menu-->
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                            <li><a href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                            <li><a href="/logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image"><img class="img-circle"
                                                  src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"
                                                  alt="User Image"></div>
                <div class="pull-left info">
                    <p><?=$data['firstname'] .' '. $data['lastname']?></p>
                    <p class="designation">
                        Position</p>
                </div>
            </div>
            <!-- Sidebar Menu-->
            <ul class="sidebar-menu">
                <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>HAWB</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="/hawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                        <li><a href="/hawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                        <li><a href="/hawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        <li><a href="/hawb_status"><i class="fa fa-circle-o"></i>Status</a></li>
                        <li><a href="/hawb_excel"><i class="fa fa-circle-o"></i>Excel add</a></li>
                        <li><a href="/hawb_print"><i class="fa fa-circle-o"></i>Print cod</a></li>
                    </ul>
                </li>
                <?if($data['userLaw'] >= 2) :?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>MAWB</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/mawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                            <li><a href="/mawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                            <li><a href="/mawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        </ul>
                    </li>
                    <li><a href="/manifest"><i class="fa fa-edit"></i><span>Manifest</span></a></li>
                    <li><a href="/ukrposhta"><i class="fa fa-edit"></i><span>Express Ukrposhta</span></a></li>
                <?endif;?>
                <?if($data['userLaw'] == 3) :?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>Admin</span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/user_stat"><i class="fa fa-circle-o"></i>Statictic</a></li>
                            <li><a href="/user_add"><i class="fa fa-circle-o"></i>Add user</a></li>
                            <li><a href="/user_edit"><i class="fa fa-circle-o"></i>Edit user</a></li>
                        </ul>
                    </li>
                <?endif;?>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <div class="page-title">
            <div>
                <h1><i class="fa fa-dashboard"></i> HAWB</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="/admin">Main</a></li>
                </ul>
            </div>
        </div>
        <form action="/hawb_add" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="origin">Origin</label>
                            <input class="form-control" id="origin" name="origin" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="id">HAWB</label>
                            <input class="form-control" id="id" name="hawb" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="id">Destination</label>
                            <input class="form-control" id="id" name="destination" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="id">Manifest</label>
                            <input class="form-control" id="id" name="manifest" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 order-md-1">
                    <h3 class="mb-3">Shipper</h3>
                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="account">Account</label>
                            <input class="form-control" id="account" name="account" placeholder="" value=""
                                   required="" type="text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="country">Country</label>
                            <input class="form-control" id="country" name="country" placeholder="" value=""
                                   required="" type="text">
                        </div>
                        <!--<div class="invalid-feedback">
                            Please select a valid country.
                        </div>-->
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="phone">Phone</label>
                            <input class="form-control" id="phone" name="phone" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="mobile">Mobile</label>
                            <input class="form-control" id="mobile" name="phone2" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="name">Name</label>
                            <input class="form-control" id="name" name="name" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="sent_by">Sent By</label>
                            <input class="form-control" id="sent_by" name="sent_by" placeholder="" value=""
                                   required="" type="text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="ref">Ref</label>
                            <input class="form-control" id="ref" name="ref" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="address">Ref2</label>
                            <input class="form-control" id="address" name="ref2" placeholder="1234 Main St" required=""
                                   type="text">
                            <!--<div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="address">Address</label>
                            <input class="form-control" id="address" name="address" placeholder="1234 Main St"
                                   required="" type="text">
                            <!--<div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="city">City/Town</label>
                            <input class="form-control" id="city" name="city" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="state">State</label>
                            <input class="form-control" id="state" name="state" placeholder="" value="" required=""
                                   type="text">
                            <!--<div class="invalid-feedback">
                                Please provide a valid state.
                            </div>-->
                        </div>
                        <div class="col-md-5 mb-3">
                            <label for="zip">Zip</label>
                            <input class="form-control" id="zip" name="zip" placeholder="" value="" required=""
                                   type="text">
                            <!--<div class="invalid-feedback">
                                Zip code required.
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 class="mb-3">Consignee</h3>
                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="account">Account</label>
                            <input class="form-control" id="account" name="accountc" placeholder="" value=""
                                   required="" type="text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="country">Country</label>
                            <input class="form-control" id="country" name="countryc" placeholder="" value=""
                                   required="" type="text">
                        </div>
                        <!--<div class="invalid-feedback">
                            Please select a valid country.
                        </div>-->
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="phone">Phone</label>
                            <input class="form-control" id="phone" name="phonec" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 mb-3">
                            <label for="mobile">Mobile</label>
                            <input class="form-control" id="mobile" name="phone2c" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="name">Name</label>
                            <input class="form-control" id="name" name="name_c" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="attn">Attn.</label>
                            <input class="form-control" id="attn" name="attn" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="ref">Ref</label>
                            <input class="form-control" id="refc" name="refc" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="ref">Ref</label>
                            <input class="form-control" id="ref2c" name="ref2c" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="address">Address</label>
                            <input class="form-control" id="address" name="addressc" placeholder="1234 Main St"
                                   required=""
                                   type="text">
                            <!--<div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 mb-3">
                            <label for="city">City/Town</label>
                            <input class="form-control" id="city" name="cityc" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="state">State</label>
                            <input class="form-control" id="state" name="statec" placeholder="" value="" required=""
                                   type="text">
                            <!--<div class="invalid-feedback">
                                Please provide a valid state.
                            </div>-->
                        </div>
                        <div class="col-md-5 mb-3">
                            <label for="zip">Zip</label>
                            <input class="form-control" id="zip" name="zipc" placeholder="" value="" required=""
                                   type="text">
                            <!--<div class="invalid-feedback">
                                Zip code required.
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <hr style="border: 1px solid grey">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3 pb-10">
                            <label for="country">Payment</label>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" id="www" name="payment_id" required="">
                                <option value="">Choose...</option>
                                <option>P</option>
                            </select>
                            <!--<div class="invalid-feedback">
                                Please select a valid country.
                            </div>-->

                        </div>
                        <div class="col-md-3">
                            <select class="form-control" id="www" name="payment_id_2" required="">
                                <option value="">N/A</option>
                                <option>NA</option>
                            </select>
                            <!--<div class="invalid-feedback">
                                Please select a valid country.
                            </div>-->

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="srn">SRN No.</label>
                        </div>
                        <div class="col-md-6 pb-10">
                            <input class="form-control" id="srn" name="srn_no" placeholder="" required="" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="collection_ref">Collection Ref.</label>
                        </div>
                        <div class="col-md-6 pb-10">
                            <input class="form-control" id="collection_ref" name="collection_ref" placeholder=""
                                   required="" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="collect_amt">Collect Amt.</label>
                        </div>
                        <div class="col-md-4 pb-10">
                            <input class="form-control" id="collect_amt" name="collect_amt" placeholder="" required=""
                                   type="text" value="0">
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="collect_amt_2" placeholder="" required="" type="text"
                                   value="USD">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="cod_amt">COD Amt.</label>
                        </div>
                        <div class="col-md-4 pb-10">
                            <input class="form-control" id="cod_amt" name="cod_amt" placeholder="" required=""
                                   type="text" value="0">
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="cod_amt_2" placeholder="" required="" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="cash_amt">Cash/Add Amt.</label>
                        </div>
                        <div class="col-md-4 pb-10">
                            <input class="form-control" id="cash_amt" name="cash_amt" placeholder="" required=""
                                   type="text" value="0">
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" name="cash_amt_2" placeholder="" required="" type="text"
                                   value="UAH">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 mb-2">
                            <label for="awb_reference">AWB Reference</label>
                        </div>
                        <div class="col-md-6 mb-6">
                            <input class="form-control" id="awb_reference" name="awb_reference" placeholder=""
                                   required="" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3 pb-10">
                            <label for="goods_value">Goods Value</label>
                        </div>
                        <div class="col-md-3 pb-10">
                            <input class="form-control" id="goods_value" name="goods_value" placeholder="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" name="goods_value_2" placeholder="" required="" type="text"
                                   value="USD">
                        </div>
                        <div class="col-md-2">
                            <label for="pickup_by">Pickup By</label>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" id="pickup_by" name="pickup_by" placeholder="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row pb-10">
                        <div class="col-md-12">
                            <input type="checkbox" class="check-input" id="exampleCheck1">
                            <label class="check-label" for="exampleCheck1">"SHIELD": Extended Liability</label>
                        </div>
                    </div>
                    <div class="row pb-10">
                        <div class="col-md-3">
                            <label for="shield_value">SHIELD value</label>
                        </div>

                        <div class="col-md-3">
                            <input class="form-control" id="shield_value" name="shield_value" placeholder="" value="0"
                                   required="" type="text">
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" name="shield_value_2" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            <label for="weight">Weight</label>
                        </div>
                        <div class="col-md-2 pb-10">
                            <input class="form-control" id="weight" name="weight" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3">
                            <label for="charg_wt">Charg. Wt.</label>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" id="charg_wt" name="charg_wt" placeholder="" value=""
                                   required="" type="text">
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" id="charg_wt_iden" name="charg_wt_iden"
                                    required="">
                                <option value="kg">Kg</option>
                                <option>Funt</option>
                            </select>
                            <!--<div class="invalid-feedback">
                                Please select a valid country.
                            </div>-->

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 mb-3">
                            <label for="pcs">Pcs</label>
                        </div>
                        <div class="col-md-2 mb-2">
                            <input class="form-control" id="pcs" name="pcs" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cube">Cube</label>
                        </div>
                        <div class="col-md-2 mb-2">
                            <input class="form-control" id="cube" name="cube" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3 mb-2">
                            <select class="form-control" id="cube_iden" name="cube_iden"
                                    required="">
                                <option value="m3">m3</option>
                                <option>Funt</option>
                            </select>
                            <!--<div class="invalid-feedback">
                                Please select a valid country.
                            </div>-->
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="factor">Factor</label>
                        </div>
                        <div class="col-md-3 mb-10">
                            <select class="form-control" name="factor">
                                <option value="5000" selected>5000</option>
                                <option value="6000">6000</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-info">Calc</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="due_dt">Due Dt</label>
                        </div>
                        <div class="col-md-3">
                            <input class="form-control" id="due_dt" name="due_dt" placeholder="" value="" required=""
                                   type="text">
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info">...</button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="product">Product</label>
                        </div>
                            <div class="col-md-3 mb-10">
                                <select class="form-control" name="product_exp">
                                    <option value="exp" selected>EXP</option>
                                    <option value="exp_1">exp-1</option>
                                </select>
                            </div>
                            <div class="col-md-3 mb-10">
                                <select class="form-control" name="product_ppx">
                                    <option value="ppx" selected>PPX</option>
                                    <option value="ppx_1">ppx-1</option>
                                </select>
                            </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="pickup_dt">Pickup Dt</label>
                        </div>
                        <div class="col-md-3 mb-10">
                            <input class="form-control" id="pickup_dt" name="pickup_dt" placeholder="" value="5/25/2018"
                                   required=""
                                   type="text">
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info" style="margin:0;">...</button>
                        </div>
                        <div class="col-md-3">
                            <input class="form-control" id="pickup_dt_tume" name="pickup_dt_time" placeholder="" value="12:00"
                                   required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="Loc">Loc</label>
                        </div>
                        <div class="col-md-3 mb-10">
                            <input class="form-control" id="Loc" name="loc" placeholder="" value="" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <select class="form-control" id="remarks" name="remarks" required="">
                                <option value="remarks">Remarks</option>
                                <option value="remarks">remarks-1</option>
                                <option>remarks-2</option>
                            </select>
                            <!--<div class="invalid-feedback">
                               Please select a valid country.
                           </div>-->
                        </div>
                        <div class="col-md-6 mb-10">
                            <input class="form-control" id="remarks_val" name="remarks_val" placeholder="" value=""
                                   required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="goods_org">Goods Org</label>
                        </div>
                        <div class="col-md-3 mb-10">
                            <input class="form-control" id="goods_org" name="goods_org" placeholder="" value="UA" required=""
                                   type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="goods_org">Desc.</label>
                        </div>
                        <div class="col-md-4 mb-10">
                            <input class="form-control" id="goods_org" name="description" placeholder="" value="UA" required=""
                                   type="text">
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-info">Items</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mb-3">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
        </form>
    </div>
</div>
