<body class="sidebar-mini fixed">
<div class="wrapper">
    <!-- Navbar-->
    <header class="main-header hidden-print"><a class="logo" href="/admin">Inter Express</a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!--Notification Menu-->
                    Сreated by <a href="https://www.linkedin.com/in/artemiy-karkusha-00020b161/" target="_blank">Artemiy
                        Karkusha</a> and <a href="https://www.linkedin.com/in/oleksandr-khraban-34b355161/"
                                            target="_blank">Khraban Oleksandr</a>
                    <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown"
                                                              aria-expanded="false"><i
                                    class="fa fa-bell-o fa-lg"></i></a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have 4 new notifications.</li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-primary"></i><i
                                                    class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-danger"></i><i
                                                    class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-success"></i><i
                                                    class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li class="not-footer"><a href="#">See all notifications.</a></li>
                        </ul>
                    </li>
                    <!-- User Menu-->
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                            <li><a href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                            <li><a href="/logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image"><img class="img-circle"
                                                  src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"
                                                  alt="User Image"></div>
                <div class="pull-left info">
                    <p><?= $data['firstname'] . ' ' . $data['lastname'] ?></p>
                    <p class="designation">
                        Position</p>
                </div>
            </div>
            <!-- Sidebar Menu-->
            <ul class="sidebar-menu">
                <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>HAWB</span><i
                                class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="/hawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                        <li><a href="/hawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                        <li><a href="/hawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        <li><a href="/hawb_status"><i class="fa fa-circle-o"></i>Status</a></li>
                        <li><a href="/hawb_excel"><i class="fa fa-circle-o"></i>Excel add</a></li>
                        <li><a href="/hawb_print"><i class="fa fa-circle-o"></i>Print cod</a></li>
                    </ul>
                </li>
                <? if ($data['userLaw'] >= 2) : ?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>MAWB</span><i
                                    class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/mawb_add"><i class="fa fa-circle-o"></i>Add new</a></li>
                            <li><a href="/mawb_search"><i class="fa fa-circle-o"></i>Search</a></li>
                            <li><a href="/mawb_edit"><i class="fa fa-circle-o"></i>Edit</a></li>
                        </ul>
                    </li>
                    <li><a href="/manifest"><i class="fa fa-edit"></i><span>Manifest</span></a></li>
                    <li><a href="/ukrposhta"><i class="fa fa-edit"></i><span>Express Ukrposhta</span></a></li>
                <? endif; ?>
                <? if ($data['userLaw'] == 3) : ?>
                    <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>Admin</span><i
                                    class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/user_stat"><i class="fa fa-circle-o"></i>Statictic</a></li>
                            <li><a href="/user_add"><i class="fa fa-circle-o"></i>Add user</a></li>
                            <li><a href="/user_edit"><i class="fa fa-circle-o"></i>Edit user</a></li>
                        </ul>
                    </li>
                <? endif; ?>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <div class="page-title">
            <div>
                <h1><i class="fa fa-dashboard"></i>Manual</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="/admin">Main</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                        <h3>Введите номер накладной:</h3>
                        <form method="post" action="/hawb_status" class="form-search form-inline">
                            <input type="text" name="search" class="search-query" placeholder="# invoice" value="<?=$data['status']['id_hawb'];?>">
                            <button class="btn btn-success" type="submit">Status</button>
                            <hr>
                        </form>
                    <?php if ($data['test']=== false):?>
                        <div class="alert alert-danger mt-10" role="alert">
                            Накладная не найдена
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <?php if (empty($data['status'])):?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="post" action="/hawb_status">
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="date_time">Date</label>
                                <input type="text" class="form-control" id="date_time" name="date_time" required>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="location">Location</label>
                                <input type="text" class="form-control" id="location" name="location" required>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="activity">Activity</label>
                                <input type="text" class="form-control" id="activity" name="activity" required>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>Выбирете партию:</label>
                                <?=$data['select'];?>
                            </div>
                        </div>
                        <button class="btn btn-primary mt-10" type="submit" name="add_status_batch">Add</button>
                        <?if($data['add_status_batch'] === true){?>
                            <div class="alert alert-success mt-10" role="alert">
                                New status successfully added for <?=$data['countUpdStatParcel'];?> parcel <?=$data['batch'];?> batch
                            </div>
                        <?}?>
                    </form>
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php if (!empty($data['status'])):?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Location</th>
                            <th scope="col">Activity</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?=$data['status']['express_id'];?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form method="post" action="/hawb_status">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="date_time">Date</label>
                                <input type="text" class="form-control" id="date_time" name="date_time" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="location">Location</label>
                                <input type="text" class="form-control" id="location" name="location" required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="activity">Activity</label>
                                <input type="text" class="form-control" id="activity" name="activity" required>
                            </div>
                            <input style="display: none;" type="text" hidden="hidden" class="form-control" id="hawb_id" name="hawb_id" value="<?=$data['status']['id_hawb'];?>">
                        </div>
                        <button class="btn btn-primary mt-10" type="submit" name="add_status">Add</button>
                        <?if($data['add_status'] === true){?>
                            <div class="alert alert-success mt-10" role="alert">
                                New status successfully added
                            </div>
                        <?}?>
                    </form>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
