<!DOCTYPE html>
<html>
    <head>
	    <meta charset="utf-8">
        <title>Express</title>
        <link href="css/style.css" rel="stylesheet">
	  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	  	rel='stylesheet' type='text/css'>
        <script
                src="https://code.jquery.com/jquery-3.2.1.js"
                integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
                crossorigin="anonymous">
        </script>
    </head>
	<body>

<?php include 'application/views/'.$content_view; ?>

    </body>
</html>
