<!--
Created by PhpStorm.
 User: Artemiy Karkusha
 Date: 11.05.2018
 Time: 17:14
 -->

<header id="home">
<section class="hero" id="hero">
    <div id="bg">
        <img src="/img/main.png" alt="">
    </div>
    <div class="main_header">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
           <h1>Inter Express</h1>
        </div>
        <div class="col-md-5">

        </div>
        <div  class="col-md-3 margin">
            <p href="">tel. (380)93-716-36-87</p>
        </div>
        <div class="col-md-1">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h1 class="text-slogan"> <span>CLIENTS</span> IN YOUR BUSINESS </h1><br>
                <p>Your clients the internet. Learn how to receive them.</p><br>
            <p class="links"><a href="/login">Join</a></p>
    </div>
    </div>
</section>
</header>