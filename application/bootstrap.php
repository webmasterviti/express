<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 03.01.2018
 * Time: 23:25
 */

require_once './config.php';

require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
require_once 'core/database.php';

Route::start(); // запускаем маршрутизатор

?>