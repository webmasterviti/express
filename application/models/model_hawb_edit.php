<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 24.05.2018
 * Time: 20:29
 */

class Model_hawb_edit extends Model
{
    private $connect;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
    }

    function seaview($search)
    {
        $sql = "SELECT * FROM hawb WHERE id = $search";
        try {
            $result = $this->connect->db->query($sql);
            $row = $result->fetch();

            $snipper = $row['snipper_id'];
            $consignee = $row['consignee_id'];
            $money = $row['money_id'];
            $box = $row['box_id'];


            $sql1 = "SELECT * FROM snipper WHERE id = $snipper";
            $result = $this->connect->db->query($sql1);
            $row1 = $result->fetch();


            $sql2 = "SELECT * FROM consignee WHERE id = $consignee";
            $result = $this->connect->db->query($sql2);
            $row2 = $result->fetch();


            $sql3 = "SELECT * FROM money WHERE id = $money";
            $result = $this->connect->db->query($sql3);
            $row3 = $result->fetch();
            $payments = explode(" ", $row3['payment']);
            $collect_amt = explode(" ", $row3['collect_amt']);
            $cod_amt = explode(" ", $row3['cod_amt']);
            $cash_amt = explode(" ", $row3['cash_amt']);


            $sql4 = "SELECT * FROM box WHERE id = $box";
            $result = $this->connect->db->query($sql4);
            $row4 = $result->fetch();
            $goods_value = explode(" ", $row4['goods_value']);
            $shield_value = explode(" ", $row4['shield_value']);
            $charg_wt = explode(" ", $row4['charg_wt']);
            $remarks = explode(" ", $row4['remarks']);
            $cube = explode(" ", $row4['cube']);


            $text = " <form action=\"/hawb_edit\" method=\"post\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"row\">
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"origin\">Origin</label>
                            <input class=\"form-control\" id=\"origin\" name=\"origin\" placeholder=\"\" value=\"" . $row['origin'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"id\">HAWB</label>
                            <input class=\"form-control\" id=\"id\" name=\"hawb\" placeholder=\"\" value=\"" . $row['id'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"id\">Destination</label>
                            <input class=\"form-control\" id=\"id\" name=\"destination\" placeholder=\"\" value=\"" . $row['destination'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"id\">Manifest</label>
                            <input class=\"form-control\" id=\"id\" name=\"manifest\" placeholder=\"\" value=\"" . $row['manifest'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 order-md-1\">
                    <h3 class=\"mb-3\">Shipper</h3>
                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"account\">Account</label>
                            <input class=\"form-control\" id=\"account\" name=\"account\" placeholder=\"\" value=\"" . $row1['account'] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"country\">Country</label>
                            <input class=\"form-control\" id=\"country\" name=\"country\" placeholder=\"\" value=\"" . $row1['contry'] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                        <!--<div class=\"invalid-feedback\">
                            Please select a valid country.
                        </div>-->
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"phone\">Phone</label>
                            <input class=\"form-control\" id=\"phone\" name=\"phone\" placeholder=\"\" value=\"" . $row1['phone'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"mobile\">Mobile</label>
                            <input class=\"form-control\" id=\"mobile\" name=\"phone2\" placeholder=\"\" value=\"" . $row1['phone2'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"name\">Name</label>
                            <input class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"\" value=\"" . $row1['name_s'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"sent_by\">Sent By</label>
                            <input class=\"form-control\" id=\"sent_by\" name=\"sent_by\" placeholder=\"\" value=\"" . $row1['sent_by'] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"ref\">Ref</label>
                            <input class=\"form-control\" id=\"ref\" name=\"ref\" placeholder=\"\" value=\"" . $row1['ref'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"address\">Ref2</label>
                            <input class=\"form-control\" id=\"address\" name=\"ref2\" placeholder=\"1234 Main St\" value=\"" . $row1['ref2'] . "\"
                                   type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"address\">Address</label>
                            <input class=\"form-control\" id=\"address\" name=\"address\" placeholder=\"1234 Main St\"
                                   value=\"" . $row1['address'] . "\" type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"city\">City/Town</label>
                            <input class=\"form-control\" id=\"city\" name=\"city\" placeholder=\"\" value=\"" . $row1['city'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6 mb-3\">
                            <label for=\"state\">State</label>
                            <input class=\"form-control\" id=\"state\" name=\"state\" placeholder=\"\" value=\"" . $row1['state'] . "\" required=\"\"
                                   type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Please provide a valid state.
                            </div>-->
                        </div>
                        <div class=\"col-md-5 mb-3\">
                            <label for=\"zip\">Zip</label>
                            <input class=\"form-control\" id=\"zip\" name=\"zip\" placeholder=\"\" value=\"" . $row1['zip'] . "\" required=\"\"
                                   type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Zip code required.
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <h3 class=\"mb-3\">Consignee</h3>
                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"account\">Account</label>
                            <input class=\"form-control\" id=\"account\" name=\"accountc\" placeholder=\"\" value=\"" . $row2['account'] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"country\">Country</label>
                            <input class=\"form-control\" id=\"country\" name=\"countryc\" placeholder=\"\" value=\"" . $row2['contry'] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                        <!--<div class=\"invalid-feedback\">
                            Please select a valid country.
                        </div>-->
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"phone\">Phone</label>
                            <input class=\"form-control\" id=\"phone\" name=\"phonec\" placeholder=\"\" value=\"" . $row2['phone'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"mobile\">Mobile</label>
                            <input class=\"form-control\" id=\"mobile\" name=\"phone2c\" placeholder=\"\" value=\"" . $row2['phone2'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"name\">Name</label>
                            <input class=\"form-control\" id=\"name\" name=\"name_c\" placeholder=\"\" value=\"" . $row2['name_c'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"attn\">Attn.</label>
                            <input class=\"form-control\" id=\"attn\" name=\"attn\" placeholder=\"\" value=\"" . $row2['attn'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"ref\">Ref</label>
                            <input class=\"form-control\" id=\"refc\" name=\"refc\" placeholder=\"\" value=\"" . $row2['ref'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"ref\">Ref</label>
                            <input class=\"form-control\" id=\"ref2c\" name=\"ref2c\" placeholder=\"\" value=\"" . $row2['ref2'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"address\">Address</label>
                            <input class=\"form-control\" id=\"address\" name=\"addressc\" placeholder=\"1234 Main St\"
                                   value=\"" . $row2['address'] . "\"
                                   type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"city\">City/Town</label>
                            <input class=\"form-control\" id=\"city\" name=\"cityc\" placeholder=\"\" value=\"" . $row2['city'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6 mb-3\">
                            <label for=\"state\">State</label>
                            <input class=\"form-control\" id=\"state\" name=\"statec\" placeholder=\"\" value=\"" . $row2['state'] . "\" required=\"\"
                                   type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Please provide a valid state.
                            </div>-->
                        </div>
                        <div class=\"col-md-5 mb-3\">
                            <label for=\"zip\">Zip</label>
                            <input class=\"form-control\" id=\"zip\" name=\"zipc\" placeholder=\"\" value=\"" . $row2['zip'] . "\" required=\"\"
                                   type=\"text\">
                            <!--<div class=\"invalid-feedback\">
                                Zip code required.
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <hr style=\"border: 1px solid grey\">
            <div class=\"row\">
                <div class=\"col-md-6\">
                    <div class=\"row\">
                        <div class=\"col-md-3 pb-10\">
                            <label for=\"country\">Payment</label>
                        </div>
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"www\" name=\"payment_id\" required=\"\">
                            <option> " . $payments[0] . "</option>
                                <option value=\"\">Choose...</option>
                                <option>P</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->

                        </div>
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"www\" name=\"payment_id_2\" required=\"\">
                            <option> " . $payments[1] . "</option>
                                <option value=\"\">N/A</option>
                                <option>NA</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->

                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"srn\">SRN No.</label>
                        </div>
                        <div class=\"col-md-6 pb-10\">
                            <input class=\"form-control\" id=\"srn\" name=\"srn_no\" placeholder=\"\" value=\"" . $row3['srn_no'] . "\" type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"collection_ref\">Collection Ref.</label>
                        </div>
                        <div class=\"col-md-6 pb-10\">
                            <input class=\"form-control\" id=\"collection_ref\" name=\"collection_ref\" placeholder=\"\"
                                   value=\"" . $row3['collection_ref'] . "\" type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"collect_amt\">Collect Amt.</label>
                        </div>
                        <div class=\"col-md-4 pb-10\">
                            <input class=\"form-control\" id=\"collect_amt\" name=\"collect_amt\" placeholder=\"\" required=\"\"
                                   type=\"text\" value=\"" . $collect_amt[0] . "\">
                        </div>
                        <div class=\"col-md-4\">
                            <input class=\"form-control\" name=\"collect_amt_2\" placeholder=\"\" required=\"\" type=\"text\"
                                   value=\"" . $collect_amt[1] . "\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"cod_amt\">COD Amt.</label>
                        </div>
                        <div class=\"col-md-4 pb-10\">
                            <input class=\"form-control\" id=\"cod_amt\" name=\"cod_amt\" placeholder=\"\" required=\"\"
                                   type=\"text\" value=\"" . $cod_amt[0] . "\">
                        </div>
                        <div class=\"col-md-4\">
                            <input class=\"form-control\" name=\"cod_amt_2\" value=\"" . $cod_amt[1] . "\" placeholder=\"\" required=\"\" type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"cash_amt\">Cash/Add Amt.</label>
                        </div>
                        <div class=\"col-md-4 pb-10\">
                            <input class=\"form-control\" id=\"cash_amt\" name=\"cash_amt\" placeholder=\"\" required=\"\"
                                   type=\"text\" value=\"" . $cash_amt[0] . "\">
                        </div>
                        <div class=\"col-md-4\">
                            <input class=\"form-control\" name=\"cash_amt_2\" placeholder=\"\" required=\"\" type=\"text\"
                                   value=\"" . $cash_amt[1] . "\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3 mb-2\">
                            <label for=\"awb_reference\">AWB Reference</label>
                        </div>
                        <div class=\"col-md-6 mb-6\">
                            <input class=\"form-control\" id=\"awb_reference\" name=\"awb_reference\" placeholder=\"\"
                                   value=\"" . $row3["awb_reference"] . "\" type=\"text\">
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <div class=\"row\">
                        <div class=\"col-md-3 pb-10\">
                            <label for=\"goods_value\">Goods Value</label>
                        </div>
                        <div class=\"col-md-3 pb-10\">
                            <input class=\"form-control\" id=\"goods_value\" name=\"goods_value\" placeholder=\"\" value=\"" . $goods_value[0] . "\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" name=\"goods_value_2\" placeholder=\"\" value=\"" . $goods_value[1] . "\" type=\"text\"
                                   value=\"USD\">
                        </div>
                        <div class=\"col-md-2\">
                            <label for=\"pickup_by\">Pickup By</label>
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" id=\"pickup_by\" name=\"pickup_by\" placeholder=\"\" value=\"" . $row4['pickup_by'] . "\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row pb-10\">
                        <div class=\"col-md-12\">
                            <input type=\"checkbox\" class=\"check-input\" id=\"exampleCheck1\">
                            <label class=\"check-label\" for=\"exampleCheck1\">\"SHIELD\": Extended Liability</label>
                        </div>
                    </div>
                    <div class=\"row pb-10\">
                        <div class=\"col-md-3\">
                            <label for=\"shield_value\">SHIELD value</label>
                        </div>

                        <div class=\"col-md-3\">
                            <input class=\"form-control\" id=\"shield_value\" name=\"shield_value\" placeholder=\"\" value=\"" . $shield_value[0] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" name=\"shield_value_2\" placeholder=\"\" value=\"" . $row4['pickup_by'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-2\">
                            <label for=\"weight\">Weight</label>
                        </div>
                        <div class=\"col-md-2 pb-10\">
                            <input class=\"form-control\" id=\"weight\" name=\"weight\" placeholder=\"\" value=\"" . $row4['weight'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3\">
                            <label for=\"charg_wt\">Charg. Wt.</label>
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" id=\"charg_wt\" name=\"charg_wt\" placeholder=\"\" value=\"" . $charg_wt[0] . "\"
                                   required=\"\" type=\"text\">
                        </div>
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"charg_wt_iden\" name=\"charg_wt_iden\"
                                    required=\"\" >
                                    <option value=\"kg\">" . $charg_wt[1] . "</option>
                                <option value=\"kg\">Kg</option>
                                <option>Funt</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->

                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-2 mb-3\">
                            <label for=\"pcs\">Pcs</label>
                        </div>
                        <div class=\"col-md-2 mb-2\">
                            <input class=\"form-control\" id=\"pcs\" name=\"pcs\" placeholder=\"\" value=\"" . $row4['pcs'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"cube\">Cube</label>
                        </div>
                        <div class=\"col-md-2 mb-2\">
                            <input class=\"form-control\" id=\"cube\" name=\"cube\" placeholder=\"\" value=\"" . $cube[0] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3 mb-2\">
                            <select class=\"form-control\" id=\"cube_iden\" name=\"cube_iden\"
                                    required=\"\">
                                    <option value=\"m3\">" . $cube[0] . "</option>
                                <option value=\"m3\">m3</option>
                                <option>Funt</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->
                        </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"factor\">Factor</label>
                        </div>
                        <div class=\"col-md-2 mb-10\">
                            <select class=\"form-control\" name=\"factor\" style=\"height: 33px; width:80px;\">
                                <option value=\"5000\" selected>5000</option>
                                <option value=\"6000\">6000</option>
                            </select>
                        </div>
                        <div class=\"col-md-3\">
                            <button type=\"button\" class=\"btn btn-info\">Calc</button>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"due_dt\">Due Dt</label>
                        </div>
                        <div class=\"col-md-3\">
                            <input class=\"form-control\" id=\"due_dt\" name=\"due_dt\" placeholder=\"\" value=\"" . $row4['due_dt'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-2\">
                            <button type=\"button\" class=\"btn btn-info\">...</button>
                        </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"product\">Product</label>
                        </div>
                            <div class=\"col-md-3 mb-10\">
                                <select class=\"form-control\" name=\"product_exp\">
                                <option value=\"exp\" selected>" . $row4['product_1'] . "</option>
                                    <option value=\"exp\" selected>EXP</option>
                                    <option value=\"exp_1\">exp-1</option>
                                </select>
                            </div>
                            <div class=\"col-md-3 mb-10\">
                                <select class=\"form-control\" name=\"product_ppx\">
                                <option value=\"exp\" selected>" . $row4['product_2'] . "</option>
                                    <option value=\"ppx\" selected>PPX</option>
                                    <option value=\"ppx_1\">ppx-1</option>
                                </select>
                            </div>  
                                           
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"pickup_dt\">Pickup Dt</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <input class=\"form-control\" id=\"pickup_dt\" name=\"pickup_dt\" placeholder=\"\" value=\"" . $row4['pickup_dt'] . "\"
                                   required=\"\"
                                   type=\"text\">
                        </div>
                        <div class='col-md-2'>
                        <button type=\"button\" class=\"btn btn-info\">...</button>
                            </div>
                            <div class=\"col-md-3\">
                            <input class=\"form-control\" id=\"pickup_dt_tume\" name=\"pickup_dt_time\" placeholder=\"\"
                                   required=\"\"
                                   type=\"text\" value=\"Then\">
                        </div>   
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"Loc\">Loc</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <input class=\"form-control\" id=\"Loc\" name=\"loc\" placeholder=\"\" value=\"" . $row4['loc'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"remarks\" name=\"remarks\" required=\"\">
                                <option>" . $remarks[0] . "</option>
                                <option>remarks-1</option>
                                <option>remarks-2</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                               Please select a valid country.
                           </div>-->
                        </div>
                        <div class=\"col-md-6 mb-10\">
                            <input class=\"form-control\" id=\"remarks_val\" name=\"remarks_val\" placeholder=\"\" value=\"" . $remarks[1] . "\"
                                   required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"goods_org\">Goods Org</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <input class=\"form-control\" id=\"goods_org\" name=\"goods_org\" placeholder=\"\" value=\"" . $row4['goods_org'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"goods_org\">Desc.</label>
                        </div>
                        <div class=\"col-md-4 mb-10\">
                            <input class=\"form-control\" id=\"goods_org\" name=\"description\" placeholder=\"\" value=\"" . $row4['description'] . "\" required=\"\"
                                   type=\"text\">
                        </div>
                        <div class=\"col-md-3\">
                            <button type=\"button\" class=\"btn btn-info\">Items</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr class=\"mb-3\">
            <button class=\"btn btn-primary btn-lg btn-block\" type=\"submit\">Continue to checkout</button>
        </form>";

            return $text;
        } catch (PDOException $e) {
            return "попробуйте другой номер накладной";
        }
    }


    function update($ses, $m1, $m2, $m3, $b1, $b2, $b3, $b4, $b5, $b6, $b7, $b8, $b9, $b9_2, $b10, $b11, $b12, $b13, $b14, $b15, $s1, $s2, $s3, $s4, $s5, $s6, $s7, $s8, $s9, $s10, $s11, $s12, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $p1, $p2, $p3, $p4, $p5, $p6, $p7)
    {
        $sql = "SELECT * FROM hawb WHERE id = $ses";
        $result = $this->connect->db->query($sql);
        $row = $result->fetch();
        $box = $row['box_id'];
        $money = $row['money_id'];
        $snipper = $row['snipper_id'];
        $consignee = $row['consignee_id'];

        /*snipper*/
        $statement = $this->connect->db->prepare("UPDATE snipper SET account=:account,contry=:contry, phone=:phone, phone2=:phone2, name_s=:name_s, sent_by=:sent_by, ref=:ref,ref2=:ref2,address=:address,city=:city, state=:state,zip=:zip WHERE id= $snipper");
        $statement->bindParam(':account', $s1, PDO::PARAM_STR);
        $statement->bindParam(':contry', $s2, PDO::PARAM_STR);
        $statement->bindParam(':phone', $s3, PDO::PARAM_STR);
        $statement->bindParam(':phone2', $s4, PDO::PARAM_STR);
        $statement->bindParam(':name_s', $s5, PDO::PARAM_STR);
        $statement->bindParam(':sent_by', $s6, PDO::PARAM_STR);
        $statement->bindParam(':ref', $s7, PDO::PARAM_STR);
        $statement->bindParam(':ref2', $s8, PDO::PARAM_STR);
        $statement->bindParam(':address', $s9, PDO::PARAM_STR);
        $statement->bindParam(':city', $s10, PDO::PARAM_STR);
        $statement->bindParam(':state', $s11, PDO::PARAM_STR);
        $statement->bindParam(':zip', $s12, PDO::PARAM_STR);
        $statement->execute();

        /*end snipper*/


        /*start consignee */


        $statement = $this->connect->db->prepare("UPDATE consignee SET account=:account,contry=:contry, phone=:phone, phone2=:phone2, name_c=:name_c, attn=:attn, ref=:ref,ref2=:ref2,address=:address,city=:city, state=:state,zip=:zip WHERE id= $consignee");
        $statement->bindParam(':account', $c1, PDO::PARAM_STR);
        $statement->bindParam(':contry', $c2, PDO::PARAM_STR);
        $statement->bindParam(':phone', $c3, PDO::PARAM_STR);
        $statement->bindParam(':phone2', $c4, PDO::PARAM_STR);
        $statement->bindParam(':name_c', $c5, PDO::PARAM_STR);
        $statement->bindParam(':attn', $c6, PDO::PARAM_STR);
        $statement->bindParam(':ref', $c7, PDO::PARAM_STR);
        $statement->bindParam(':ref2', $c8, PDO::PARAM_STR);
        $statement->bindParam(':address', $c9, PDO::PARAM_STR);
        $statement->bindParam(':city', $c10, PDO::PARAM_STR);
        $statement->bindParam(':state', $c11, PDO::PARAM_STR);
        $statement->bindParam(':zip', $c12, PDO::PARAM_STR);
        $statement->execute();


        /*end consignee */

        /*start payment */

        $statement = $this->connect->db->prepare("UPDATE money SET payment=:payments,srn_no=:srn_no,collection_ref=:collection_ref,cod_amt=:cod_amt_id,collect_amt=:collect_amt_id,cash_amt=:cash_amt_id,awb_reference=:awb_reference  WHERE id=$money");
        $statement->bindParam(':payments', $p1, PDO::PARAM_STR);
        $statement->bindParam(':srn_no', $p2, PDO::PARAM_STR);
        $statement->bindParam(':collection_ref', $p3, PDO::PARAM_STR);
        $statement->bindParam(':collect_amt_id', $p4, PDO::PARAM_STR);
        $statement->bindParam(':cod_amt_id', $p5, PDO::PARAM_STR);
        $statement->bindParam(':cash_amt_id', $p6, PDO::PARAM_STR);
        $statement->bindParam(':awb_reference', $p7, PDO::PARAM_STR);
        $statement->execute();

        /*end payment */


        /*start box */

        $statement = $this->connect->db->prepare("UPDATE box SET goods_value=:goods_value, pickup_by = :pickup_by, shield_value = :shield_value,  weight = :weight, charg_wt = :charg_wt, pcs = :pcs, cube = :cube, factor = :factor, product_1 = :product_1, product_2 = :product_2, due_dt = :due_dt, pickup_dt = :pickup_dt, loc = :loc, remarks = :remarks, goods_org = :goods_org, description = :description WHERE id= $box");
        $statement->bindParam(':goods_value', $b1, PDO::PARAM_STR);
        $statement->bindParam(':pickup_by', $b2, PDO::PARAM_STR);
        $statement->bindParam(':shield_value', $b3, PDO::PARAM_STR);
        $statement->bindParam(':weight', $b4, PDO::PARAM_STR);
        $statement->bindParam(':charg_wt', $b5, PDO::PARAM_STR);
        $statement->bindParam(':pcs', $b6, PDO::PARAM_STR);
        $statement->bindParam(':cube', $b7, PDO::PARAM_STR);
        $statement->bindParam(':factor', $b8, PDO::PARAM_STR);
        $statement->bindParam(':product_1', $b9, PDO::PARAM_STR);
        $statement->bindParam(':product_2', $b9_2, PDO::PARAM_STR);
        $statement->bindParam(':due_dt', $b10, PDO::PARAM_STR);
        $statement->bindParam(':loc', $b11, PDO::PARAM_STR);
        $statement->bindParam(':pickup_dt', $b12, PDO::PARAM_STR);
        $statement->bindParam(':remarks', $b13, PDO::PARAM_STR);
        $statement->bindParam(':goods_org', $b14, PDO::PARAM_STR);
        $statement->bindParam(':description', $b15, PDO::PARAM_STR);
        $statement->execute();
        /*end box */

        /*start hawp */
        $statement = $this->connect->db->prepare("UPDATE hawb SET origin=:origin, destination = :destination, manifest = :manifest WHERE id=$ses");
        $statement->bindParam(':origin', $m1, PDO::PARAM_STR);
        $statement->bindParam(':destination', $m2, PDO::PARAM_STR);
        $statement->bindParam(':manifest', $m3, PDO::PARAM_STR);
        $statement->execute();
        /*end hawp */

    }

    public function getAllHawb()
    {
        //Create connect database
        $connect = new Database(HOST, DB, USER, PASS);
        ///Делаем запрос для вывода всей информации с таблицы hawb
        $query = $connect->db->prepare("SELECT * FROM hawb");
        $query->execute();
        /////Превращаем полученые данные в асоциативный массив
        $resultHawb = $query->fetchAll();
        ////Проходимся по каждому масиву $resultHawb
        /// Информация по hawb
        for ($i = 0; $i < count($resultHawb); $i++) {
            //////
            /// =====================Snipper
            ///////
            ////Делаем запрос к базе используя значение в ключе snipper_id масива $resultHawb
            $query = $connect->db->prepare("SELECT * FROM snipper WHERE id = :id");
            $query->bindParam(':id', $resultHawb[$i]['snipper_id']);
            try {
                $query->execute();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                return FALSE;
            }
            /////Превращаем полученые данные в асоциативный массив
            $result_snipper = $query->fetchAll();
            //Вставляем полученые данные с массива $result_snipper в главный массив $resultHawb в ключ snipper_id
            $resultHawb[$i]['snipper_id'] = $result_snipper[0];
            ////////
            ///=====================Consignee
            ////////
            ////Делаем запрос к базе используя значение в ключе consignee_id масива $resultHawb
            $query = $connect->db->prepare("SELECT * FROM consignee WHERE id = :id");
            $query->bindParam(':id', $resultHawb[$i]['consignee_id']);
            try {
                $query->execute();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                return FALSE;
            }
            /////Превращаем полученые данные в асоциативный массив
            $result_consignee = $query->fetchAll();
            //Вставляем полученые данные с массива $result_consignee в главный массив $resultHawb в ключ consignee_id
            $resultHawb[$i]['consignee_id'] = $result_consignee[0];
            ////////
            ///=====================Box
            ////////
            ////Делаем запрос к базе используя значение в ключе box_id масива $resultHawb
            $query = $connect->db->prepare("SELECT * FROM box WHERE id = :id");
            $query->bindParam(':id', $resultHawb[$i]['box_id']);
            try {
                $query->execute();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                return FALSE;
            }
            /////Превращаем полученые данные в асоциативный массив
            $result_box = $query->fetchAll();
            //Вставляем полученые данные с массива $result_box в главный массив $resultHawb в ключ box_id
            $resultHawb[$i]['box_id'] = $result_box[0];
            ////////
            ///=====================Box
            ////////
            ////Делаем запрос к базе используя значение в ключе money_id масива $resultHawb
            $query = $connect->db->prepare("SELECT * FROM money WHERE id = :id");
            $query->bindParam(':id', $resultHawb[$i]['money_id']);
            try {
                $query->execute();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                return FALSE;
            }
            /////Превращаем полученые данные в асоциативный массив
            $result_money = $query->fetchAll();
            //Вставляем полученые данные с массива $result_money в главный массив $resultHawb в ключ money_id
            $resultHawb[$i]['money_id'] = $result_money[0];
        }
        return $resultHawb;
    }

    public function subExcel()
    {
        $bool = TRUE;
        $str = '';
        $connect = new Database(HOST, DB, USER, PASS);
        $sql = 'SELECT * FROM hawb WHERE excel=' . $bool;
        $query = $connect->db->prepare($sql);
        $query->execute();
        $resultQuery = $query->fetchAll();
        foreach ($resultQuery as $key => $value) {
            $str .= '<h4>' . $value['id'] . '</h4><br>';
        }
        if ($str == '') {
            return FALSE;
        } else {
            return $str;
        }
    }

    public function genTable($array)
    {
        $body = '<div class="row">
                <div class="collapse" id="collapseExample\'.$i.\'">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col" class="manifest-table-title-blue">Serial</th>
                            <th scope="col" class="manifest-table-title-blue">HAWB</th>
                            <th scope="col" class="manifest-table-title-blue">Org</th>
                            <th scope="col" class="manifest-table-title-blue">Sender</th>
                            <th scope="col" class="manifest-table-title-blue">Receiver</th>
                            <th scope="col" class="manifest-table-title-blue">Shpt Info</th>
                            <th scope="col" class="manifest-table-title-blue">Service Info</th>
                        </tr>
                        </thead>
                        <tbody>';
        for ($j = 0; $j < count($array); $j++) {
            $array_hawb_j = $array[$j];
            $body .= '
                        <tr>
                            <th scope="row" class="manifest-table-blue">' . ($j + 1) . '</th>
                            <td class="manifest-table-blue">' . $array_hawb_j['id'] . '</td>
                            <td class="manifest-table-blue">' . $array_hawb_j['origin'] . '</td>
                            <td class="manifest-table-blue">' . $array_hawb_j['snipper_id']['name_s'] . '<br>'
                . $array_hawb_j['snipper_id']['address'] . '<br>'
                . $array_hawb_j['snipper_id']['contry'] . '<br>'
                . $array_hawb_j['snipper_id']['city'] . '<br> Tel:'
                . $array_hawb_j['snipper_id']['phone']
                . '</td>
                            <td class="manifest-table-blue">' . $array_hawb_j['consignee_id']['name_c'] . '<br>'
                . $array_hawb_j['consignee_id']['address'] . '<br>'
                . $array_hawb_j['consignee_id']['contry'] . '<br>'
                . $array_hawb_j['consignee_id']['city'] . '<br> Tel:'
                . $array_hawb_j['consignee_id']['phone']
                . '</td>
                            <td class="manifest-table-blue">' . 'Wgt: ' . $array_hawb_j['box_id']['charg_wt'] . '<br>'
                . 'Pcs: ' . $array_hawb_j['box_id']['pcs'] . '<br>'
                . 'Ask Goods: <br>'
                . 'Customs Value: ' . $array_hawb_j['box_id']['goods_value'] . '<br>'
                . 'Destination: ' . $array_hawb_j['consignee_id']['contry'] . '<br>'
                . '
                            </td>
                            <td class="manifest-table-blue">
                                ' . 'Type: ' . $array_hawb_j['box_id']['product_2'] . '<br>'
                . 'CC Amt Ask: ' . $array_hawb_j['box_id']['pcs'] . '<br>'
                . '</td>
                        </tr>
                        
                        ';
            unset($array_hawb_j);
        }
        $body .= '</tbody>
                    </table>

                </div>
            </div>
         </div>
         ';
        return $body;
    }


}

