<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 24.05.2018
 * Time: 8:48
 */

class Model_hawb_add extends Model
{
    private $connect;

    public function __construct(){
        $this->connect =  new Database(HOST,DB,USER,PASS);
    }

    function addInTable ($m1,$m2,$m3,$b1,$b2,$b3,$b4,$b5,$b6,$b7,$b8,$b9,$b9_2,$b10,$b11,$b12,$b13,$b14,$b15,$s1, $s2, $s3, $s4, $s5, $s6, $s7, $s8, $s9,$s10,$s11,$s12,$c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9,$c10,$c11,$c12,$p1,$p2,$p3,$p4,$p5,$p6,$p7){



       /*snipper*/
        $sql = 'INSERT INTO snipper(id, account, contry, phone, phone2, name_s, sent_by, ref,ref2,address,city, state,zip) VALUES (:id, :account, :contry, :phone, :phone2, :name_s, :sent_by, :ref,:ref2,:address,:city, :state,:zip)';
        $statement = $this->connect->db->prepare($sql);
        $id=0;
        $statement ->bindParam(':id', $id);
        $statement ->bindParam(':account', $s1);
        $statement ->bindParam(':contry', $s2);
        $statement ->bindParam(':phone', $s3);
        $statement ->bindParam(':phone2', $s4);
        $statement ->bindParam(':name_s', $s5);
        $statement ->bindParam(':sent_by', $s6);
        $statement ->bindParam(':ref', $s7);
        $statement ->bindParam(':ref2', $s8);
        $statement ->bindParam(':address', $s9);
        $statement ->bindParam(':city', $s10);
        $statement ->bindParam(':state', $s11);
        $statement ->bindParam(':zip', $s12);

        $statement->execute();
        $idsnipper = $this->connect->db->lastInsertId();

        /*end snipper*/


        /*start consignee */
        $sql = 'INSERT INTO consignee(id, account, contry, phone, phone2, name_c, attn, ref,ref2,address,city, state,zip) VALUES (:id, :account, :contry, :phone, :phone2, :name_c, :sent_by, :ref,:ref2,:address,:city, :state,:zip)';
        $statement = $this->connect->db->prepare($sql);
        $id_c=0;
        $statement ->bindParam(':id', $id_c);
        $statement ->bindParam(':account', $c1);
        $statement ->bindParam(':contry', $c2);
        $statement ->bindParam(':phone', $c3);
        $statement ->bindParam(':phone2', $c4);
        $statement ->bindParam(':name_c', $c5);
        $statement ->bindParam(':sent_by', $c6);
        $statement ->bindParam(':ref', $c7);
        $statement ->bindParam(':ref2', $c8);
        $statement ->bindParam(':address', $c9);
        $statement ->bindParam(':city', $c10);
        $statement ->bindParam(':state', $c11);
        $statement ->bindParam(':zip', $c12);

        $statement->execute();
        $idconsignee = $this->connect->db->lastInsertId();

        /*end consignee */

        /*start payment */
        $sql = 'INSERT INTO money(id, payment, srn_no, collection_ref, collect_amt, cod_amt , cash_amt, awb_reference) VALUES (:id, :payment_id, :srn_no, :collection_ref, :collect_amt_id, :cod_amt_id , :cash_amt_id, :awb_reference)';
        $statement = $this->connect->db->prepare($sql);
        $id_p=0;
        $statement ->bindParam(':id', $id_p);
        $statement ->bindParam(':payment_id', $p1);
        $statement ->bindParam(':srn_no', $p2);
        $statement ->bindParam(':collection_ref', $p3);
        $statement ->bindParam(':collect_amt_id', $p4);
        $statement ->bindParam(':cod_amt_id', $p5);
        $statement ->bindParam(':cash_amt_id', $p6);
        $statement ->bindParam(':awb_reference', $p7);

        $statement->execute();
        $idpayment = $this->connect->db->lastInsertId();
        /*end payment */


        /*start box */
        $sql = 'INSERT INTO box(id, goods_value, pickup_by, shield_value, weight, charg_wt , pcs, cube,factor,product_1,due_dt,pickup_dt,loc,remarks,goods_org,description,product_2) VALUES (:id, :goods_value, :pickup_by, :shield_value, :weight, :charg_wt , :pcs, :cube,:factor,:product_1,:due_dt,:pickup_dt,:loc,:remarks,:goods_org,:description,:product_2)';
        $statement = $this->connect->db->prepare($sql);
        $id_b = 0;
        $statement ->bindParam(':id', $id_b);
        $statement ->bindParam(':goods_value', $b1);
        $statement ->bindParam(':pickup_by', $b2);
        $statement ->bindParam(':shield_value', $b3);
        $statement ->bindParam(':weight', $b4);
        $statement ->bindParam(':charg_wt', $b5);
        $statement ->bindParam(':pcs', $b6);
        $statement ->bindParam(':cube', $b7);
        $statement ->bindParam(':factor', $b8);
        $statement ->bindParam(':product_1', $b9);
        $statement ->bindParam(':due_dt',$b10);
        $statement ->bindParam(':pickup_dt', $b11);
        $statement ->bindParam(':loc', $b12);
        $statement ->bindParam(':remarks', $b13);
        $statement ->bindParam(':goods_org', $b14);
        $statement ->bindParam(':description', $b15);
        $statement ->bindParam(':product_2', $b9_2);

        $statement->execute();
        $idbox = $this->connect->db->lastInsertId();
        /*end box */

        /*start hawp */
        $sql = 'INSERT INTO hawb(id,origin,destination,manifest,snipper_id,consignee_id,box_id,money_id) VALUES (:id,:origin,:destination,:manifest,:snipper_id,:consignee_id,:box_id,:money_id)';
        $statement = $this->connect->db->prepare($sql);
        $id_m = 0;
        $statement ->bindParam(':id', $id_m);
        $statement ->bindParam(':origin', $m1);
        $statement ->bindParam(':destination', $m2);
        $statement ->bindParam(':manifest', $m3);
        $statement ->bindParam(':snipper_id', $idsnipper);
        $statement ->bindParam(':consignee_id', $idconsignee);
        $statement ->bindParam(':box_id', $idbox);
        $statement ->bindParam(':money_id', $idpayment);

          $statement->execute();
        /*end hawp */
    }
}

