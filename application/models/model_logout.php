<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:19 AM
 */
class model_logout extends Model{
    public function logout(){
        session_start();
        unset($_SESSION);
        session_destroy();
        return TRUE;
    }
}