<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/1/2018
 * Time: 12:12 PM
 */

class model_tracking_number extends Model{



    private function addIndex($index){
        if(strlen($index) <= 4){
            return '0' . $index;
        }else{
            return $index;
        }
    }

    protected function statusInfo($arr){
        $table = '';
        for($i = count($arr)-1; $i > -1 ; $i--){
            $table .= "<tr>
                            <td>{$arr[$i]['date_time']}
                            </td>
                            <td>{$arr[$i]['location']}</td>
                            <td><p>{$arr[$i]['activity']}</p></td>
                        </tr>";
        }
        return $table;
    }

    public function getData($number = null)
    {
        if (!empty($number)) {
            //Создаем екземпляр класса Database
            $connect = new Database(HOST, DB, USER, PASS);
            //Екранируем запрос
            $query = $connect->db->prepare('SELECT * FROM hawb WHERE id=' . $number);
            $query->execute();
            //Получаем все данные с таблицы HAWB
            $resultQuery = $query->fetchAll();
            if(empty($resultQuery)){
                return false;
            }
            $resultQuery = $resultQuery[0];
            //Пробигаемся по всем внутренним массивам переменной $resultQuery( по каждому HAWB)
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ snipper заполняем информацией с таблицы snipper
            $query = $connect->db->prepare('SELECT name_s,phone,address,city,zip,contry FROM snipper WHERE id =' . $resultQuery['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            $resultQuery['snipper_id'] = $result_snipper[0];
            $resultQuery['snipper_id']['zip'] = $this->addIndex($resultQuery['snipper_id']['zip']);
            $arr = explode(' ',$resultQuery['snipper_id']['name_s']);
            $resultQuery['snipper_id']['firstName'] = $arr[0];
            $resultQuery['snipper_id']['lastName'] = $arr[1];
            ///////////
            ///
            ///
            ///
            ///
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ consignee заполняем информацией с таблицы consignee
            $query = $connect->db->prepare('SELECT name_c,phone,address,city,zip,contry FROM consignee WHERE id =' . $resultQuery['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery['consignee_id'] = $result_consignee[0];
            $resultQuery['consignee_id']['zip'] = $this->addIndex($resultQuery['consignee_id']['zip']);
            $arr = explode(' ',$resultQuery['consignee_id']['name_c']);
            $resultQuery['consignee_id']['firstName'] = $arr[0];
            $resultQuery['consignee_id']['lastName'] = $arr[1];
            /////
            ///
            ///
            ///
            $query = $connect->db->prepare('SELECT collect_amt FROM money WHERE id =' . $resultQuery['money_id']);
            $query->execute();
            $result_money = $query->fetchAll();
            $resultQuery['money_id'] = $result_money[0];
            ////////
            /////
            ///
            ///
            ///
            $query = $connect->db->prepare('SELECT weight,width,`length`,pcs,description FROM box WHERE id =' . $resultQuery['box_id']);
            $query->execute();
            $result_box = $query->fetchAll();
            $resultQuery['box_id'] = $result_box[0];
            ////////
            ///
            $query = $connect->db->prepare('SELECT * FROM exspress_hawb WHERE hawb_id =' . $resultQuery['id']);
            $query->execute();
            $result_status = $query->fetchAll();
            $resultQuery['status'] = $result_status[count($result_status)-1]['activity'];
            $result_status = $this->statusInfo($result_status);
            $resultQuery['express_id'] = $result_status;
            return $resultQuery;
        }
    }



}