<?php

/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 9:51 AM
 */
class model_hawb_print extends Model
{
    private $connect;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
    }

    public function getSelect()
    {
        try {
            $sql = "SELECT batch_name FROM hawb WHERE batch_name IS NOT NULL";
            $result = $this->connect->db->prepare($sql);
            $result->execute();
            $query = $result->fetchAll();
        } catch (Exception $e) {
             return false;
        }
        $count = count($query);
        for ($i = 0; $i < $count; $i++) {
            if (isset($query[$i])) {
                for ($j = $i + 1; $j < $count; $j++) {
                    if ($query[$j] == $query[$i]) {
                        unset($query[$j]);
                    }
                }
            }
        }
        $text = "<select class=\"form-control\" style=\"width: 40%;margin-top: 0px;\" tabindex=\"-1\" aria-hidden=\"true\" name=\"select\">";
        $text .= "<option selected=\"selected\">Выберете отчёт</option>";
        foreach ($query as $key => $value){
            $text .= "<option value=\"{$value['batch_name']}\">{$value['batch_name']}</option>";
        }
        $text .= "</select>";
        return $text;
    }

    public function getAllHawb($batch){
        $result_arr = [];
        $sql = "SELECT * FROM hawb WHERE batch_name = \"$batch\"";
        $result = $this->connect->db->prepare($sql);
        $result->execute();
        $data = $result->fetchAll();
        foreach ($data as $key => $row){
            try {
                $sql = "SELECT * FROM box WHERE id = {$row['box_id']}";
                unset($row['box_id']);
                $result = $this->connect->db->query($sql);
                $arr = $result->fetch();
                $row['box'] = $arr;
                $charg_wt = explode(" ", $arr['charg_wt']);

                $sql = "SELECT * FROM money WHERE id = {$row['money_id']}";
                unset($row['money_id']);
                $result = $this->connect->db->query($sql);
                $arr = $result->fetch();
                $row['money'] = $arr;
                $sql = "SELECT * FROM consignee WHERE id = {$row['consignee_id']}";
                $result1 = $this->connect->db->query($sql);
                $text = "";
                $arr = $result1->fetch();
                $row['consignee'] = $arr;

                if ($arr) {
                    $text .= '<h3> Страна назначения:</h3>' . $row['contry'] . '<br><h3> Имя получятеля:</h3>' . $row['name_c'] . '<br><h3> телефон получателя:</h3>' . $row['phone'];

                }
                $sql = "SELECT * FROM snipper WHERE id = {$row['snipper_id']}";
                unset($row['snipper_id']);
                $result1 = $this->connect->db->query($sql);
                $text = "";
                $arr = $result1->fetch();
                $row['snipper'] = $arr;
                $result_arr[] = $row;
            } catch (PDOException $e) {

            }
        }
        return $result_arr;
    }

    function seaview($search)
    {
        $sql = "SELECT * FROM hawb WHERE id = $search";
        try {
            $result = $this->connect->db->query($sql);
            $row = $result->fetch();
            //$snipper = $row['snipper_id'];
            $sql = "SELECT * FROM box WHERE id = {$row['box_id']}";
            unset($row['box_id']);
            $result = $this->connect->db->query($sql);
            $arr = $result->fetch();
            $row['box'] = $arr;
            $charg_wt = explode(" ", $arr['charg_wt']);

            $sql = "SELECT * FROM money WHERE id = {$row['money_id']}";
            unset($row['money_id']);
            $result = $this->connect->db->query($sql);
            $arr = $result->fetch();
            $row['money'] = $arr;
            $sql = "SELECT * FROM consignee WHERE id = {$row['consignee_id']}";
            $result1 = $this->connect->db->query($sql);
            $text = "";
            $arr = $result1->fetch();
            $row['consignee'] = $arr;

            if ($arr) {
                $text .= '<h3> Страна назначения:</h3>' . $row['contry'] . '<br><h3> Имя получятеля:</h3>' . $row['name_c'] . '<br><h3> телефон получателя:</h3>' . $row['phone'];

            }
            $sql = "SELECT * FROM snipper WHERE id = {$row['snipper_id']}";
            unset($row['snipper_id']);
            $result1 = $this->connect->db->query($sql);
            $text = "";
            $arr = $result1->fetch();
            $row['snipper'] = $arr;
            $result_arr[] = $row;
            return $result_arr;
        } catch (PDOException $e) {
            return 0;
        }
    }
}