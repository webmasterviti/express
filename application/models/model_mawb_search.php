<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 04.06.2018
 * Time: 16:23
 */

class model_mawb_search extends Model
{
    private $connect;

    public function __construct(){
        $this->connect =  new Database(HOST,DB,USER,PASS);
    }
    function seaview ($search)
    {
        $sql = "SELECT * FROM mawb WHERE id = $search";
        try {
            $result = $this->connect->db->query($sql);
            $row = $result->fetch();
            $ttc_bags_weight = explode(" ", $row['ttc_bags_weight']);
            $hawbs_cube = explode(" ", $row['hawbs_cube']);

            $exspress_id = $row['exspress_id'];
            $sql1 = "SELECT * FROM express_mawb WHERE id = $exspress_id";
            $result = $this->connect->db->query($sql1);
            $row1 = $result->fetch();

            $text .= "
            <div class=\"row\">
<div class=\"col-md-2 mt-10\">
    <br>
    <b>Master Airwaybill</b>
</div>
<div class=\"col-md-2\">
    <label for=\"org_entity\">Org.Entity</label>
    <input class=\"form-control\" id=\"org_entity\" name=\"org_entity\" placeholder=\"\" value=\"". $row['org_entity']."\" required=\"\"
           type=\"text\" readonly>
</div>
<div class=\"col-md-1\">
    <label for=\"carrier\">Carrier</label>
    <input class=\"form-control\" id=\"carrier\" name=\"carrier\" placeholder=\"\" value=\"". $row['carrier']."\" required=\"\"
           type=\"text\" readonly>
</div>
<div class=\"col-md-1\">
    <br>
    <button type=\"button\" class=\"btn btn-info\">...</button>
</div>
<div class=\"col-md-2\">
    <label for=\"org_port\">Org.Port</label><br>
    <select class=\"form-control\" id=\"org_port\" name=\"org_port\" required=\"\" disabled>
        <option value=\"". $row['org_port']."\" selected>". $row['org_port']."</option>
    </select>
</div>
<div class=\"col-md-2\">
    <label for=\"mawb_no\">MAWB No.</label>
    <input class=\"form-control\" id=\"mawb_no\" name=\"mawb_no\" placeholder=\"\" value=\"". $row['mawn_no']."\" required=\"\"
           type=\"text\" readonly>
</div>
<div class=\"col-md-2\">
    <label for=\"sub\">Sub</label>
    <input class=\"form-control\" id=\"sub\" name=\"sub\" placeholder=\"\" value=\"". $row['sub']."\" required=\"\"
           type=\"text\" readonly>
</div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"origin_branch\">Origin Branch</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"origin_branch\" name=\"origin_branch\" required=\"\" disabled>
            <option value=\"". $row['origin_branch']."\" selected>". $row['origin_branch']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"destination_port\">Destination Port</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"destination_port\" name=\"destination_port\" placeholder=\"\" value=\"". $row['destination_port']."\" required=\"\"
               type=\"text\" readonly>
    </div>
    <div class=\"col-md-2 pt-10\">
        <button type=\"button\" class=\"btn btn-info\">...</button>
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"destination_entity\">Destination Entity</label>
    </div>
    <div class=\"col-md-3 pt-10\">
        <select class=\"form-control\" id=\"destination_entity\" name=\"destination_entity\" required=\"\" disabled>
            <option value=\"". $row['destination_entity']."\" selected>Aramex/JFK</option>
        </select>
    </div>
    <div class=\"col-md-1 pt-10\">
        <label for=\"branch\">Branch</label>
    </div>
    <div class=\"col-md-3 pt-10\">
        <select class=\"form-control\" id=\"branch\" name=\"branch\" required=\"\" disabled>
            <option value=\"". $row['branch']."\" selected>". $row['branch']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"mode_of_transport\">Mode of Transport</label>
    </div>
    <div class=\"col-md-3 pt-10\">
        <select class=\"form-control\" id=\"mode_of_transport\" name=\"mode_of_transport\" required=\"\" disabled>
            <option value=\"". $row['mode_of_transport']."\" selected>". $row['mode_of_transport']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"no_of_baby_bags\">No. of Baby Bags</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"no_of_baby_bags\" name=\"no_of_baby_bags\" placeholder=\"\" value=\"". $row['no_of_baby_bags']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"no_of_master_bags\">No. of Master Bags(TTC)</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"no_of_master_bags\" name=\"no_of_master_bags\" placeholder=\"\" value=\"". $row['no_of_master_bags']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"ttc_bags_weight\">TTC Bags Weight</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"ttc_bags_weight\" name=\"ttc_bags_weight\" placeholder=\"\" value=\"". $ttc_bags_weight[0]."\" required=\"\"
               type=\"text\" readonly>
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"ttc_bags_weight_iz\" name=\"ttc_bags_weight_2\" required=\"\" disabled>
            <option value=\"". $ttc_bags_weight[1]."\" selected>". $ttc_bags_weight[1]."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"actual_weight\">Actual Weight</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"actual_weight\" name=\"actual_weight\" placeholder=\"\" value=\"". $row['actual_weight']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"hawbs_wight\">HAWBs Weight</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"hawbs_wight\" name=\"hawbs_wight\" placeholder=\"\" value=\"". $row['hawbs_weight']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"hawbs_cube\">HAWBs Cube</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"hawbs_cube\" name=\"hawbs_cube\" placeholder=\"\" value=\"". $hawbs_cube[0]."\" required=\"\"
               type=\"text\" readonly>
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"hawbs_cube_iz\" name=\"hawbs_cube_2\" required=\"\" disabled>
            <option value=\"". $hawbs_cube[1]."\" selected>". $hawbs_cube[1]."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"remarks\">Remarks</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"remarks\" name=\"remarks\" placeholder=\"\" value=\"". $row['remarks']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"linehaur_supplier\">Linehaur supplier</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"linehaur_supplier\" name=\"linehaur_supplier\" required=\"\" disabled>
            <option value=\"". $row['linehaul_supplier']."\" selected>". $row['linehaul_supplier']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"srr\">SRR</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"srr\" name=\"srr\" placeholder=\"\" value=\"". $row['srr']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"custom_reference\">Custom Reference</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"custom_reference\" name=\"custom_reference\" placeholder=\"\" value=\"". $row['custom_reference']."\" required=\"\"
               type=\"text\" readonly>
    </div>
</div>

<hr style=\"border:1px solid darkgrey\">

<div class=\"row\">
    <div class=\"col-md-1 mt-10\">
        <br>
        <b>Flight</b>
    </div>
    <div class=\"col-md-2\">
        <label for=\"al_code\">A/L Code</label>
        <div class=\"row\">
            <div class=\"col-md-6\">
                <input class=\"form-control\" id=\"al_code\" name=\"al_code\" placeholder=\"\" value=\"". $row1['al_code']."\" required=\"\"
                       type=\"text\" readonly>
            </div>
            <div class=\"col-md-6\">
                <button type=\"button\" class=\"btn btn-info\">...</button>
            </div>
        </div>
    </div>
    <div class=\"col-md-1\">
        <label for=\"flt_no\">Flt No</label>
        <input class=\"form-control\" id=\"flt_no\" name=\"flt_no\" placeholder=\"\" value=\"". $row1['flt_no']."\" required=\"\"
               type=\"text\" readonly>
    </div>
    <div class=\"col-md-3\">
        <label for=\"etd_date\">ETD(m/d/y h:mm)</label><br>
        <div class=\"row\">
            <div class=\"col-md-6\">
                <input class=\"form-control\" id=\"etd_date\" name=\"etd_date\" placeholder=\"\" value=\"". $row1['etd']."\" required=\"\"
                       type=\"text\" readonly>
            </div>
            <div class=\"col-md-2\">
                <button type=\"button\" class=\"btn btn-info\">...</button>
            </div>
            <div class=\"col-md-4\">
                <input class=\"form-control\" name=\"etd_time\" placeholder=\"\" value=\"". $row1['etd_time']."\" required=\"\"
                       type=\"text\" readonly>
            </div>
        </div>
    </div>
    <div class=\"col-md-3\">
        <label for=\"eta_date\">ETA(m/d/y h:mm)</label><br>
        <div class=\"row\">
            <div class=\"col-md-6\">
                <input class=\"form-control\" id=\"eta_date\" name=\"eta_date\" placeholder=\"\" value=\"". $row1['eta']."\" required=\"\"
                       type=\"text\" readonly>
            </div>
            <div class=\"col-md-2\">
                <button type=\"button\" class=\"btn btn-info\">...</button>
            </div>
            <div class=\"col-md-4\">
                <input class=\"form-control\" name=\"eta_time\" placeholder=\"\" value=\"". $row1['eta_time']."\" required=\"\"
                       type=\"text\" readonly>
            </div>
        </div>
    </div>
</div>
<hr style=\"border:1px solid darkgrey\">
<div class=\"row\">

        ";

            return  $text;
        } catch (PDOException $e) {
            return "попробуйте другой номер накладной";
        }
    }

}





