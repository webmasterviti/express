<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 04.06.2018
 * Time: 16:22
 */

class model_hawb_search extends Model
{
    private $connect;

    public function __construct(){
        $this->connect =  new Database(HOST,DB,USER,PASS);
    }

    function seaview ($search)
    {
        $sql = "SELECT * FROM hawb WHERE id = $search";
        try {
            $result = $this->connect->db->query($sql);
            $row = $result->fetch();

            $snipper = $row['snipper_id'];
            $consignee = $row['consignee_id'];
            $money = $row['money_id'];
            $box = $row['box_id'];

            $sql1 = "SELECT * FROM snipper WHERE id = $snipper";
            $result = $this->connect->db->query($sql1);
            $row1 = $result->fetch();


            $sql2 = "SELECT * FROM consignee WHERE id = $consignee";
            $result = $this->connect->db->query($sql2);
            $row2 = $result->fetch();


            $sql3 = "SELECT * FROM money WHERE id = $money";
            $result = $this->connect->db->query($sql3);
            $row3 = $result->fetch();
            $payments = explode(" ", $row3['payment']);
            $collect_amt = explode(" ", $row3['collect_amt']);
            $cod_amt = explode(" ", $row3['cod_amt']);
            $cash_amt = explode(" ", $row3['cash_amt']);


            $sql4 = "SELECT * FROM box WHERE id = $box";
            $result = $this->connect->db->query($sql4);
            $row4 = $result->fetch();
            $goods_value = explode(" ", $row4['goods_value']);
            $shield_value = explode(" ", $row4['shield_value']);
            $charg_wt = explode(" ", $row4['charg_wt']);
            $remarks = explode(" ", $row4['remarks']);
            $cube = explode(" ", $row4['cube' ]);



            $text .= "
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"row\">
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"origin\">Origin</label>
                            <input class=\"form-control\" id=\"origin\" name=\"origin\" placeholder=\"\" value=\"". $row['origin'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"id\">HAWB</label>
                            <input class=\"form-control\" id=\"id\" name=\"hawb\" placeholder=\"\" value=\"". $row['id'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"id\">Destination</label>
                            <input class=\"form-control\" id=\"id\" name=\"destination\" placeholder=\"\" value=\"". $row['destination'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"id\">Manifest</label>
                            <input class=\"form-control\" id=\"id\" name=\"manifest\" placeholder=\"\" value=\"". $row['manifest'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 order-md-1\">
                    <h3 class=\"mb-3\">Shipper</h3>
                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"account\">Account</label>
                            <input class=\"form-control\" id=\"account\" name=\"account\" placeholder=\"\" value=\"". $row1['account'] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"country\">Country</label>
                            <input class=\"form-control\" id=\"country\" name=\"country\" placeholder=\"\" value=\"". $row1['contry'] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                        <!--<div class=\"invalid-feedback\">
                            Please select a valid country.
                        </div>-->
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"phone\">Phone</label>
                            <input class=\"form-control\" id=\"phone\" name=\"phone\" placeholder=\"\" value=\"". $row1['phone'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"mobile\">Mobile</label>
                            <input class=\"form-control\" id=\"mobile\" name=\"phone2\" placeholder=\"\" value=\"". $row1['phone2'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"name\">Name</label>
                            <input class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"\" value=\"". $row1['name_s'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"sent_by\">Sent By</label>
                            <input class=\"form-control\" id=\"sent_by\" name=\"sent_by\" placeholder=\"\" value=\"". $row1['sent_by'] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"ref\">Ref</label>
                            <input class=\"form-control\" id=\"ref\" name=\"ref\" placeholder=\"\" value=\"". $row1['ref'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"address\">Ref2</label>
                            <input class=\"form-control\" id=\"address\" name=\"ref2\" placeholder=\"1234 Main St\" value=\"". $row1['ref2'] ."\"
                                   type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"address\">Address</label>
                            <input class=\"form-control\" id=\"address\" name=\"address\" placeholder=\"1234 Main St\"
                                   value=\"". $row1['address'] ."\" type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"city\">City/Town</label>
                            <input class=\"form-control\" id=\"city\" name=\"city\" placeholder=\"\" value=\"". $row1['city'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6 mb-3\">
                            <label for=\"state\">State</label>
                            <input class=\"form-control\" id=\"state\" name=\"state\" placeholder=\"\" value=\"". $row1['state'] ."\" required=\"\"
                                   type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Please provide a valid state.
                            </div>-->
                        </div>
                        <div class=\"col-md-5 mb-3\">
                            <label for=\"zip\">Zip</label>
                            <input class=\"form-control\" id=\"zip\" name=\"zip\" placeholder=\"\" value=\"". $row1['zip'] ."\" required=\"\"
                                   type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Zip code required.
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <h3 class=\"mb-3\">Consignee</h3>
                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"account\">Account</label>
                            <input class=\"form-control\" id=\"account\" name=\"accountc\" placeholder=\"\" value=\"". $row2['account'] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"country\">Country</label>
                            <input class=\"form-control\" id=\"country\" name=\"countryc\" placeholder=\"\" value=\"". $row2['contry'] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                        <!--<div class=\"invalid-feedback\">
                            Please select a valid country.
                        </div>-->
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"phone\">Phone</label>
                            <input class=\"form-control\" id=\"phone\" name=\"phonec\" placeholder=\"\" value=\"". $row2['phone'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-8 mb-3\">
                            <label for=\"mobile\">Mobile</label>
                            <input class=\"form-control\" id=\"mobile\" name=\"phone2c\" placeholder=\"\" value=\"". $row2['phone2'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"name\">Name</label>
                            <input class=\"form-control\" id=\"name\" name=\"name_c\" placeholder=\"\" value=\"". $row2['name_c'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"attn\">Attn.</label>
                            <input class=\"form-control\" id=\"attn\" name=\"attn\" placeholder=\"\" value=\"". $row2['attn'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"ref\">Ref</label>
                            <input class=\"form-control\" id=\"refc\" name=\"refc\" placeholder=\"\" value=\"". $row2['ref'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"ref\">Ref</label>
                            <input class=\"form-control\" id=\"ref2c\" name=\"ref2c\" placeholder=\"\" value=\"". $row2['ref2'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"address\">Address</label>
                            <input class=\"form-control\" id=\"address\" name=\"addressc\" placeholder=\"1234 Main St\"
                                   value=\"". $row2['address'] ."\"
                                   type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Please enter your shipping address.
                            </div>-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-11 mb-3\">
                            <label for=\"city\">City/Town</label>
                            <input class=\"form-control\" id=\"city\" name=\"cityc\" placeholder=\"\" value=\"". $row2['city'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-6 mb-3\">
                            <label for=\"state\">State</label>
                            <input class=\"form-control\" id=\"state\" name=\"statec\" placeholder=\"\" value=\"". $row2['state'] ."\" required=\"\"
                                   type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Please provide a valid state.
                            </div>-->
                        </div>
                        <div class=\"col-md-5 mb-3\">
                            <label for=\"zip\">Zip</label>
                            <input class=\"form-control\" id=\"zip\" name=\"zipc\" placeholder=\"\" value=\"". $row2['zip'] ."\" required=\"\"
                                   type=\"text\" readonly>
                            <!--<div class=\"invalid-feedback\">
                                Zip code required.
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <hr style=\"border: 1px solid grey\">
            <div class=\"row\">
                <div class=\"col-md-6\">
                    <div class=\"row\">
                        <div class=\"col-md-3 pb-10\">
                            <label for=\"country\">Payment</label>
                        </div>
                        <div class=\"col-md-3\">
                            <select class=\"form-control mb-10\" id=\"www\" name=\"payment_id\" required=\"\" disabled>
                            <option > " . $payments[0] . "</option>
                                <option value=\"\">Choose...</option>
                                <option>P</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->

                        </div>
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"www\" name=\"payment_id_2\" required=\"\" disabled>
                            <option> " . $payments[1] . "</option>
                                <option value=\"\">N/A</option>
                                <option>NA</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->

                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"srn\">SRN No.</label>
                        </div>
                        <div class=\"col-md-6 pb-10\">
                            <input class=\"form-control\" id=\"srn\" name=\"srn_no\" placeholder=\"\" value=\"". $row3['srn_no'] ."\" type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"collection_ref\">Collection Ref.</label>
                        </div>
                        <div class=\"col-md-6 pb-10\">
                            <input class=\"form-control\" id=\"collection_ref\" name=\"collection_ref\" placeholder=\"\"
                                   value=\"". $row3['collection_ref'] ."\" type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"collect_amt\">Collect Amt.</label>
                        </div>
                        <div class=\"col-md-4 pb-10\">
                            <input class=\"form-control\" id=\"collect_amt\" name=\"collect_amt\" placeholder=\"\" required=\"\"
                                   type=\"text\" value=\"". $collect_amt[0] ."\" readonly>
                        </div>
                        <div class=\"col-md-4\">
                            <input class=\"form-control\" name=\"collect_amt_2\" placeholder=\"\" required=\"\" type=\"text\"
                                   value=\"". $collect_amt[1] ."\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"cod_amt\">COD Amt.</label>
                        </div>
                        <div class=\"col-md-4 pb-10\">
                            <input class=\"form-control\" id=\"cod_amt\" name=\"cod_amt\" placeholder=\"\" required=\"\"
                                   type=\"text\" value=\"". $cod_amt[0] ."\" readonly>
                        </div>
                        <div class=\"col-md-4\">
                            <input class=\"form-control\" name=\"cod_amt_2\" value=\"". $cod_amt[1] ."\" placeholder=\"\" required=\"\" type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"cash_amt\">Cash/Add Amt.</label>
                        </div>
                        <div class=\"col-md-4 pb-10\">
                            <input class=\"form-control\" id=\"cash_amt\" name=\"cash_amt\" placeholder=\"\" required=\"\"
                                   type=\"text\" value=\"". $cash_amt[0] ."\" readonly>
                        </div>
                        <div class=\"col-md-4\">
                            <input class=\"form-control\" name=\"cash_amt_2\" placeholder=\"\" required=\"\" type=\"text\"
                                   value=\"". $cash_amt[1] ."\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3 mb-2\">
                            <label for=\"awb_reference\">AWB Reference</label>
                        </div>
                        <div class=\"col-md-6 mb-6\">
                            <input class=\"form-control\" id=\"awb_reference\" name=\"awb_reference\" placeholder=\"\"
                                   value=\"". $row3["awb_reference"] ."\" type=\"text\" readonly>
                        </div>
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <div class=\"row\">
                        <div class=\"col-md-3 pb-10\">
                            <label for=\"goods_value\">Goods Value</label>
                        </div>
                        <div class=\"col-md-3 pb-10\">
                            <input class=\"form-control\" id=\"goods_value\" name=\"goods_value\" placeholder=\"\" value=\"". $goods_value[0] ."\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" name=\"goods_value_2\" placeholder=\"\" value=\"". $goods_value[1] ."\" type=\"text\"
                                   readonly>
                        </div>
                        <div class=\"col-md-2\">
                            <label for=\"pickup_by\">Pickup By</label>
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" id=\"pickup_by\" name=\"pickup_by\" placeholder=\"\" value=\"". $row4['pickup_by'] ."\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row pb-10\">
                        <div class=\"col-md-12\">
                            <input type=\"checkbox\" class=\"check-input\" id=\"exampleCheck1\">
                            <label class=\"check-label\" for=\"exampleCheck1\">\"SHIELD\": Extended Liability</label>
                        </div>
                    </div>
                    <div class=\"row pb-10\">
                        <div class=\"col-md-3\">
                            <label for=\"shield_value\">SHIELD value</label>
                        </div>

                        <div class=\"col-md-3\">
                            <input class=\"form-control\" id=\"shield_value\" name=\"shield_value\" placeholder=\"\" value=\"". $shield_value[0] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" name=\"shield_value_2\" placeholder=\"\" value=\"". $row4['pickup_by'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-2\">
                            <label for=\"weight\">Weight</label>
                        </div>
                        <div class=\"col-md-2 pb-10\">
                            <input class=\"form-control\" id=\"weight\" name=\"weight\" placeholder=\"\" value=\"". $row4['weight'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3\">
                            <label for=\"charg_wt\">Charg. Wt.</label>
                        </div>
                        <div class=\"col-md-2\">
                            <input class=\"form-control\" id=\"charg_wt\" name=\"charg_wt\" placeholder=\"\" value=\"". $charg_wt[0] ."\"
                                   required=\"\" type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"charg_wt_iden\" name=\"charg_wt_iden\"
                                    required=\"\" readonly>
                                    <option value=\"kg\">". $charg_wt[1] ."</option>
                                <option value=\"kg\">Kg</option>
                                <option>Funt</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->

                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-2 mb-3\">
                            <label for=\"pcs\">Pcs</label>
                        </div>
                        <div class=\"col-md-2 mb-2\">
                            <input class=\"form-control\" id=\"pcs\" name=\"pcs\" placeholder=\"\" value=\"". $row4['pcs'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3 mb-3\">
                            <label for=\"cube\">Cube</label>
                        </div>
                        <div class=\"col-md-2 mb-2\">
                            <input class=\"form-control\" id=\"cube\" name=\"cube\" placeholder=\"\" value=\"". $cube[0] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3 mb-2\">
                            <select class=\"form-control\" id=\"cube_iden\" name=\"cube_iden\"
                                    required=\"\" readonly>
                                <option value=\"m3\">". $cube[1] ."</option>
                                <option>Funt</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                                Please select a valid country.
                            </div>-->
                        </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"factor\">Factor</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <select class=\"form-control\" name=\"factor\" readonly>
                                <option value=\"5000\" selected>5000</option>
                                <option value=\"6000\">6000</option>
                            </select>
                        </div>
                        <div class=\"col-md-3\">
                            <button type=\"button\" class=\"btn btn-info\" disabled>Calc</button>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"due_dt\">Due Dt</label>
                        </div>
                        <div class=\"col-md-3\">
                            <input class=\"form-control\" id=\"due_dt\" name=\"due_dt\" placeholder=\"\" value=\"". $row4['due_dt'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-2\">
                            <button type=\"button\" class=\"btn btn-info\" disabled>...</button>
                        </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"product\">Product</label>
                        </div>
                           <div class=\"col-md-3 mb-10\">
                                <select class=\"form-control\" name=\"product_exp\" readonly>
                                <option value=\"exp\" selected>". $row4['product_1'] ."</option>
                                    <option value=\"exp\" selected>EXP</option>
                                    <option value=\"exp_1\">exp-1</option>
                                </select>
                            </div>
                            <div class=\"col-md-3 mb-10\">
                                <select class=\"form-control\" name=\"product_ppx\" readonly>
                                <option value=\"exp\" selected>". $row4['product_2'] ."</option>
                                    <option value=\"ppx\" selected>PPX</option>
                                    <option value=\"ppx_1\">ppx-1</option>
                                </select>
                            </div>
                    </div>
                    <br>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"pickup_dt\">Pickup Dt</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <input class=\"form-control\" id=\"pickup_dt\" name=\"pickup_dt\" placeholder=\"\" value=\"". $row4['pickup_dt'] ."\"
                                   required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-2\">
                           <button id=\"pickup_dt_submit\" type=\"button\" class=\"btn btn-info\" disabled>...</button>
                        </div>
                        <div class=\"col-md-3\">
                            <input class=\"form-control\" id=\"pickup_dt_tume\" name=\"pickup_dt_time\" placeholder=\"\" value=\"Then\"
                                   required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"Loc\">Loc</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <input class=\"form-control\" id=\"Loc\" name=\"loc\" placeholder=\"\" value=\"". $row4['loc'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <select class=\"form-control\" id=\"remarks\" name=\"remarks\" required=\"\" readonly>
                                <option value=\"remarks\">Remarks</option>
                                <option>". $remarks[0] ."</option>
                                <option>remarks-1</option>
                                <option>remarks-2</option>
                            </select>
                            <!--<div class=\"invalid-feedback\">
                               Please select a valid country.
                           </div>-->
                        </div>
                        <div class=\"col-md-6 mb-10\">
                            <input class=\"form-control\" id=\"remarks_val\" name=\"remarks_val\" placeholder=\"\" value=\"". $remarks[1] ."\"
                                   required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"goods_org\">Goods Org</label>
                        </div>
                        <div class=\"col-md-3 mb-10\">
                            <input class=\"form-control\" id=\"goods_org\" name=\"goods_org\" placeholder=\"\" value=\"". $row4['goods_org'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <label for=\"goods_org\">Desc.</label>
                        </div>
                        <div class=\"col-md-4 mb-10\">
                            <input class=\"form-control\" id=\"goods_org\" name=\"\" placeholder=\"\" value=\"". $row4['description'] ."\" required=\"\"
                                   type=\"text\" readonly>
                        </div>
                        <div class=\"col-md-3\">
                            <button type=\"button\" class=\"btn btn-info\" disabled>Items</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr class=\"mb-3\">
            <form action='/hawb_search' method='post' target=\"_blank\">
                <input type='hidden' name='invoice_id' value=\"{$row['id']}\">
                <button type=\"submit\" class=\"btn btn-info\">Print Invoice</button>
            </form>
        ";

            return  $text;
        } catch (PDOException $e) {
            return "попробуйте другой номер накладной";
        }
    }

    public function printInvoice($id_hawb){
        $sql = "SELECT * FROM hawb WHERE id = $id_hawb";
        $result = $this->connect->db->prepare($sql);
        $result->execute();
        $data = $result->fetchAll();
        foreach ($data as $key => $row){
            try {
                $sql = "SELECT * FROM box WHERE id = {$row['box_id']}";
                unset($row['box_id']);
                $result = $this->connect->db->query($sql);
                $arr = $result->fetch();
                $row['box'] = $arr;
                $charg_wt = explode(" ", $arr['charg_wt']);

                $sql = "SELECT * FROM money WHERE id = {$row['money_id']}";
                unset($row['money_id']);
                $result = $this->connect->db->query($sql);
                $arr = $result->fetch();
                $row['money'] = $arr;
                $sql = "SELECT * FROM consignee WHERE id = {$row['consignee_id']}";
                $result1 = $this->connect->db->query($sql);
                $text = "";
                $arr = $result1->fetch();
                $row['consignee'] = $arr;

                if ($arr) {
                    $text .= '<h3> Страна назначения:</h3>' . $row['contry'] . '<br><h3> Имя получятеля:</h3>' . $row['name_c'] . '<br><h3> телефон получателя:</h3>' . $row['phone'];

                }
                $sql = "SELECT * FROM snipper WHERE id = {$row['snipper_id']}";
                unset($row['snipper_id']);
                $result1 = $this->connect->db->query($sql);
                $text = "";
                $arr = $result1->fetch();
                $row['snipper'] = $arr;
                $result_arr[] = $row;
            } catch (PDOException $e) {

            }
        }
        $data = $result_arr[0];
        //Класс для чтения
        try {
            require './application/core/Classes/PHPExcel.php';
            //Класс для работы с датами в Excel
            require './application/core/Classes/PHPExcel/Writer/Excel5.php';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $border = [
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'top' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ],
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => [
                        'rgb' => '808080'
                    ]
                ]
            ]
        ];
        //Массив с настройками для выравнивания текста в ячейках
        $alignmentCenter = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ];
        $alignmentCenterVertical = [
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ]
        ];

        $colorRed = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => 'FF0000'
                ]
            ]
        ];
        $colorGreen = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => '00CD0E'
                ]
            ]
        ];
        $colorGrey = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => '8E91EF'
                ]
            ]
        ];
        $colorCommon = [
            'fill' => [
                'type' => 'solid',
                'color' => [
                    'rgb' => 'E4E4E4'
                ]
            ]
        ];
        $fontTop = [
            'font' => [
                'name' => 'Arial',
                'size' => 10,
                'bold' => true
            ]
        ];
        // Создаем объект класса PHPExcel
        $xls = new PHPExcel();
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(0);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $title = "{$data['id']}";
        /////////////////////////
        $sheet->setTitle($title);
        // Устанавливаем название и оформление столбцов
        $sheet->setCellValue("A1", 'Origin');
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getStyle('A1')->applyFromArray($alignmentCenter);
        // Выравнивание текста
        $sheet->setCellValue("B1", 'FinalETA');
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getStyle('B1')->applyFromArray($alignmentCenter);
        //
        $sheet->setCellValue("C1", 'HAWBOrigin');
        $sheet->getColumnDimension('С')->setWidth(20);
        $sheet->getStyle('C1')->applyFromArray($alignmentCenter);
        //
        $sheet->setCellValue("D1", 'AWB');
        $sheet->getStyle('D1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('D')->setWidth(30);
        //
        $sheet->setCellValue("E1", 'Type');
        $sheet->getStyle('E1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('E')->setWidth(30);
        ///
        $sheet->setCellValue("F1", 'PickupDate');
        $sheet->getStyle('F1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('F')->setWidth(30);
        ///
        $sheet->setCellValue("G1", 'PCS');
        $sheet->getStyle('G1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('G')->setWidth(30);
        ///
        $sheet->setCellValue("H1", 'Weight');
        $sheet->getStyle('H1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('H')->setWidth(30);
        ///
        $sheet->setCellValue("I1", 'PaymentType');
        $sheet->getStyle('I1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('I')->setWidth(30);
        ///
        $sheet->setCellValue("J1", 'Description');
        $sheet->getStyle('J1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('J')->setWidth(30);
        ///
        $sheet->setCellValue("K1", 'Customs');
        $sheet->getStyle('K1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('K')->setWidth(30);
        ///
        $sheet->setCellValue("L1", 'CustomsCurrency');
        $sheet->getStyle('L1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('L')->setWidth(30);
        ////
        $sheet->setCellValue("M1", 'ShipperName');
        $sheet->getStyle('M1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('M')->setWidth(30);
        ///
        $sheet->setCellValue("N1", 'OriginCountry');
        $sheet->getStyle('N1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('N')->setWidth(30);
        ///
        $sheet->setCellValue("O1", 'DestCountry');
        $sheet->getStyle('O1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('O')->setWidth(30);
        ///
        $sheet->setCellValue("P1", 'ConsigneeNumber');
        $sheet->getStyle('P1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('P')->setWidth(30);
        ///
        $sheet->setCellValue("Q1", 'ConsigneeName');
        $sheet->getStyle('Q1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('Q')->setWidth(30);
        ///
        $sheet->setCellValue("R1", 'ConsigneeAddress');
        $sheet->getStyle('R1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('R')->setWidth(30);
        ///
        $sheet->setCellValue("S1", 'DestCity');
        $sheet->getStyle('S1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('S')->setWidth(30);
        ///
        $sheet->setCellValue("T1", 'ConsigneeTel');
        $sheet->getStyle('T1')->applyFromArray($alignmentCenter);
        $sheet->getColumnDimension('T')->setWidth(30);;
        ///
        for ($i = 0; $i < count($data); $i++) {
            $j = $i + 2;
            //
            $sheet->setCellValue("A" . $j, $data['origin']);
            $sheet->getStyle("A" . $j)->applyFromArray($alignmentCenter);
            // Выравнивание текста
            $sheet->setCellValue("B" . $j, $data['final_eta']);
            $sheet->getStyle('B' . $j)->applyFromArray($alignmentCenter);
            //////
            $sheet->setCellValue("C" . $j, $data['origin']);
            $sheet->getStyle('C' . $j)->applyFromArray($alignmentCenter);
            ////////
            $sheet->setCellValue("D" . $j, $data['id']);
            $sheet->getStyle('D' . $j)->applyFromArray($alignmentCenter);
            ////
            //$sheet->setCellValue("E" . $j, "Type");// Найти
            //$sheet->getStyle('E' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("F" . $j, $data['box']['pickup_dt']);
            $sheet->getStyle('F' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("G" . $j, $data['box']['pcs']);
            $sheet->getStyle('G' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("H" . $j, $data['box']['weight']);
            $sheet->getStyle('H' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("I" . $j, $data['money']['payment']);
            $sheet->getStyle('I' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("J" . $j, $data['box']['description']);
            $sheet->getStyle('J' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("K" . $j,'Customs');/////Найти
            $sheet->getStyle('K' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("L" . $j,'CustomsCurrency');/////Найти
            $sheet->getStyle('L' . $j)->applyFromArray($alignmentCenter);
            ////
            $sheet->setCellValue("M" . $j,$data['snipper']['name_s']);
            $sheet->getStyle('M' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("N" . $j,$data['snipper']['contry']);
            $sheet->getStyle('N' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("O" . $j,$data['consignee']['contry']);
            $sheet->getStyle('O' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("P" . $j,$data['consignee']['account']);
            $sheet->getStyle('P' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("Q" . $j,$data['consignee']['name_c']);
            $sheet->getStyle('Q' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("R" . $j,$data['consignee']['address']);
            $sheet->getStyle('R' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("S" . $j,$data['consignee']['city']);
            $sheet->getStyle('S' . $j)->applyFromArray($alignmentCenter);
            ///
            $sheet->setCellValue("T" . $j,$data['consignee']['phone']);
            $sheet->getStyle('T' . $j)->applyFromArray($alignmentCenter);
        }
        //Рамки
        for ($i = 0; $i < 20; $i++) {
            for ($j = 0; $j < count($data) + 2; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray($border);
            }
        }
        // Устанавливаем индекс активного листа
        $xls->setActiveSheetIndex(1);
        // Получаем активный лист
        $sheet = $xls->getActiveSheet();
        // Подписываем лист
        $sheet->setTitle("Invoice");
        // Ship From
        $sheet->setCellValue("A9", 'Ship From');
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getStyle('A9')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("B9", $data['snipper']['name_s']);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getStyle('B9')->applyFromArray($alignmentCenter);
        /////////////////////////////////////////
        //
        // Invoice to:
        $sheet->setCellValue("A15", 'Invoice to:');
        $sheet->getStyle('A15')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("B15", $data['consignee']['name_c']);
        $sheet->getStyle('B15')->applyFromArray($alignmentCenter);
        /////Address
        $sheet->setCellValue("B16", $data['consignee']['address']);
        $sheet->getStyle('B16')->applyFromArray($alignmentCenter);
        //////City
        $sheet->setCellValue("B17", $data['consignee']['city']);
        $sheet->getStyle('B17')->applyFromArray($alignmentCenter);
        ///////////////////////////////////////////////////////////////////////
        ///
        // Contact
        $sheet->setCellValue("A20", 'Contact');//
        $sheet->getStyle('A20')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("B20", $data['consignee']['name_c']);
        $sheet->getStyle('B20')->applyFromArray($alignmentCenter);
        ///////////////////////////////////////////////////////////////////////
        ///
        // Tel
        $sheet->setCellValue("A21", 'Tel');
        $sheet->getStyle('A21')->applyFromArray($alignmentCenter);
        $sheet->setCellValue("B21", $data['consignee']['phone']);
        $sheet->getStyle('B21')->applyFromArray($alignmentCenter);
        ///////////////////////////////////////////////////////////////////////
        ///
        ///
        ///
        ///
        date_default_timezone_set('Europe/Kiev');
        $date_time = date("Y-m-d H:i:s");
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        header('Content-type: application/vnd.ms-excel;charset=utf-8');
        header('Content-Disposition: attachment; filename="report-' . $title . '.xls"');
        $objWriter->save('php://output');
        exit;
    }

    public function withdraw(){
        $bool = TRUE;
        $str = '';
        $connect = new Database(HOST, DB, USER, PASS);
        $sql = 'SELECT * FROM hawb WHERE excel='. $bool;
        $query = $connect->db->prepare($sql);
        $query->execute();
        $resultQuery = $query->fetchAll();
        foreach ($resultQuery as $key => $value){
            $str .= '<h4>' . $value['id'] . '</h4><br>';
        }
        if($str == ''){
            return FALSE;
        }else{
            return $str;
        }
    }



}
