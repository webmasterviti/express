<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:17 AM
 */

class model_ukrposhta extends Model
{

    private $connect;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
    }

    protected function makeAddress($array)
    {
        $sender = [
            "postcode" => $array['postcode'], //mandatory value
            "country" => $array['country'],
            "region" => $array['region'],
            "city" => $array['city'],
            "district" => $array['district'],
            "street" => $array['street'],
            "houseNumber" => $array['houseNumber'],
            "apartmentNumber" => $array['apartmentNumber'],
        ];
        $curl = curl_init('https://www.ukrposhta.ua/ecom/0.0.1/addresses');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($sender));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    protected function makeClient($array, $idAddressId)
    {
        $sender = [
            "addressId" => $idAddressId,
            "phoneNumber" => $array['phoneNumber'], // mandatory value
            "uniqueRegistrationNumber" => $array['unRegNumber'],
            "bankCode" => $array['bankCode'],
            "bankAccount" => $array['bankAccount'],
            "email" => $array['email'],
            "resident" => $array['resident']
        ];
        if ($array['individual']) {

            $sender["firstName"] = $array['firstName'];
            $sender["lastName"] = $array['lastName'];
            $sender["middleName"] = $array['middleName'];
            $sender["individual"] = $array['individual'];

        } else {

            $sender["name"] = $array['name'];
            $sender["individual"] = $array['individual'];
            $sender["edrpou"] = $array['edrpou'];
            $sender["tin"] = $array['tin'];

        }
        $curl = curl_init('https://www.ukrposhta.ua/ecom/0.0.1/clients?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($sender));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }


    private function makeShipment($array, $uuidSender, $uuidRecipient)
    {
        $shipment = [
            "sender" => [
                "uuid" => $uuidSender
            ],
            "recipient" => [
                "uuid" => $uuidRecipient
            ],
            'type' => $array['type'],
            'description' => $array['description'],
            'externalId' => $array['externalId'],
            "deliveryType" => $array['deliveryType'],
            "packageType" => $array['packageType'],
            // Тип доставки (4 основних типи: W2D склад-двері, W2W склад-склад , D2W двері-склад, D2D двері-двері)
            "paidByRecipient" => $array['paidByRecipient'],
            //Оплата за відправлення при отриманні.True – оплата одержувачем false -оплата відправником. По замовченню false
            "nonCashPayment" => $array['nonCashPayment'],
            //Оплата безготівковим розрахунком. True-безготівковий, false-готівковий. По замовченню tru
            "parcels" => [
                0 => [
                    "declaredPrice" => $array['declaredPrice'],
                    "weight" => $array['weight'],
                    "length" => $array['length']
                ],
                //1 => ["weight" => $array['weight']],
                //2=> ["length" => $array['length']],
            ],
        ];
        $curl = curl_init('https://www.ukrposhta.ua/ecom/0.0.1//shipments?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($shipment));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public function addrLabel($uuid)
    {
        $str = "https://www.ukrposhta.ua/ecom/0.0.1/shipments/" . $uuid . "/sticker?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970";
        $curl = curl_init($str);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b \n"
        ]);
        $file = curl_exec($curl);
        file_put_contents('addrLabel.pdf', $file);
        header('Content-type: application/pdf');
        readfile('addrLabel.pdf');
        curl_close($curl);
    }

    public function postalNote($uuid)
    {
        $str = "https://www.ukrposhta.ua/ecom/0.0.1/shipments/" . $uuid . "/label?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970";
        $curl = curl_init($str);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b \n"
        ]);
        $file = curl_exec($curl);
        file_put_contents('postNote.pdf', $file);
        //exit;
        //$result = json_decode($result, true);
        curl_close($curl);

    }

    public function form103($groupUuid)
    {
        $str = "https://www.ukrposhta.ua/ecom/0.0.1/shipment-groups/" . $groupUuid . "/form103a?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970";
        $curl = curl_init($str);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b \n"
        ]);
        $file = curl_exec($curl);
        file_put_contents('form103.pdf', $file);
        curl_close($curl);
    }

    public function pdf()
    {
        header("Content-type: application/pdf; charset=utf-8");

        $file = fopen("addrLabel.pdf", "r");
        $content = "";
        while ($f = fgets($file, 4096)) {
            $content .= $f;
        }
        echo $content;
    }

    public function addrLabelGroup($groupUuid)
    {
        $str = "https://www.ukrposhta.ua/ecom/0.0.1/shipment-groups/" . $groupUuid . "/sticker?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970";
        $curl = curl_init($str);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b \n"
        ]);
        $file = curl_exec($curl);
        file_put_contents('sticker.pdf', $file);
        curl_close($curl);
    }

    protected function makeNameGroup($name = 'Group 1')
    {
        $group = [
            "name" => $name,
        ];
        $curl = curl_init('https://www.ukrposhta.ua/ecom/0.0.1/shipment-groups?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer AIzaSyAA7ILfJ8MGSVX2906yFRGLP-shZY6LRvU'
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($group));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    protected function makeGroupShipment($array, $shipmentGroupUuid)
    {
        $shipments = [
            $array[0]['uuid'],
            $array[1]['uuid'],
            $array[2]['uuid'],
            $array[3]['uuid'],
            $array[4]['uuid']
        ];
        print_r($shipments);
        echo $url = 'https://www.ukrposhta.ua/ecom/0.0.1/shipment-groups/' . $shipmentGroupUuid . '/shipments?token=da8db59d-e3ca-4cb8-b3ec-b9ca8ad25970';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer cd3a9931-360c-3988-a7b5-48fdc051e26b'
        ]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($shipments));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }
    private function addIndex($index){
        if(strlen($index) <= 4){
            return '0' . $index;
        }else{
            return $index;
        }
    }
    public function getData($number = null)
    {
        if (!empty($number)) {
            $connect = new Database(HOST, DB, USER, PASS);
            //Создаем екземпляр класса Database
            $connect = new Database(HOST, DB, USER, PASS);
            //Екранируем запрос
            $query = $connect->db->prepare('SELECT * FROM hawb WHERE id=' . $number);
            $query->execute();
            //Получаем все данные с таблицы HAWB
            $resultQuery = $query->fetchAll();
            $resultQuery = $resultQuery[0];
            //Пробигаемся по всем внутренним массивам переменной $resultQuery( по каждому HAWB)
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ snipper заполняем информацией с таблицы snipper
            $query = $connect->db->prepare('SELECT name_s,phone,address,city,zip FROM snipper WHERE id =' . $resultQuery['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            $resultQuery['snipper_id'] = $result_snipper[0];
            $resultQuery['snipper_id']['zip'] = $this->addIndex($resultQuery['snipper_id']['zip']);
            $arr = explode(' ', $resultQuery['snipper_id']['name_s']);
            $resultQuery['snipper_id']['firstName'] = $arr[0];
            $resultQuery['snipper_id']['lastName'] = $arr[1];
            ///////////
            ///
            ///
            ///
            ///
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ consignee заполняем информацией с таблицы consignee
            $query = $connect->db->prepare('SELECT name_c,phone,address,city,zip FROM consignee WHERE id =' . $resultQuery['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery['consignee_id'] = $result_consignee[0];
            $resultQuery['consignee_id']['zip'] = $this->addIndex($resultQuery['consignee_id']['zip']);
            $arr = explode(' ', $resultQuery['consignee_id']['name_c']);
            $resultQuery['consignee_id']['firstName'] = $arr[0];
            $resultQuery['consignee_id']['lastName'] = $arr[1];
            /////
            ///
            ///
            ///
            $query = $connect->db->prepare('SELECT collect_amt FROM money WHERE id =' . $resultQuery['money_id']);
            $query->execute();
            $result_money = $query->fetchAll();
            $resultQuery['money_id'] = $result_money[0];
            ////////
            /////
            ///
            ///
            ///
            $query = $connect->db->prepare('SELECT weight,width,`length` FROM box WHERE id =' . $resultQuery['box_id']);
            $query->execute();
            $result_box = $query->fetchAll();
            $resultQuery['box_id'] = $result_box[0];
            ////////
            return $resultQuery;
        }
    }

    protected function inBarcode($barcode,$id){
        $connect = new Database(HOST, DB, USER, PASS);
        $data = [
            'id' => $id,
            'barcode' => $barcode,
        ];
        $sql = "UPDATE hawb SET barcode = :barcode
                    WHERE id = :id";
        $statement = $connect->db->prepare($sql);
        $statement->execute($data);
    }

    public function addForwarding($db)
    {
        //Make exeption country,barcode in addres
        //and individual in client
        $dataAddrSender = [
            "postcode" => $db['snipper_id']['zip'],
            "country" => "UA",
            //"region" => "Київская",
            "city" => $db['snipper_id']['city'],
            //"district" => "Київський",
            //"street"=>"	Магістральна",
            //"houseNumber"=>"38",
            //"apartmentNumber"=>"5"
        ];
        $dataAddrRecip = [
            "postcode" => $db['consignee_id']['zip'],
            "country" => "UA",
            //"region" => "Львівська",
            "city" => $db['consignee_id']['city'],
            //"district" => "Львівський",
            //"street"=>"Турянського",
            //"houseNumber"=>"10",
            //"apartmentNumber"=>"33"
        ];
        $dataClientSender = [
            "firstName" => $db['snipper_id']['firstName'],
            "lastName" => $db['snipper_id']['lastName'],
            //"middleName" => "Володимирович",
            "phoneNumber" => $db['snipper_id']['phone'],
            //"email" => "zaech28@gmail.com",
            // "name" => "Inter Expres",
            "individual" => true,
            //"edrpou" => "12345678"
        ];
        $dataClientRecip = [
            "firstName" => $db['consignee_id']['firstName'],
            "lastName" => $db['consignee_id']['lastName'],
            "phoneNumber" => $db['consignee_id']['phone'],
            //"email" => "maks.chev@gmail.com",
            "individual" => true,
        ];
        $dataShipment = [
            "deliveryType" => "W2W",
            // Тип доставки (4 основних типи: W2D склад-двері, W2W склад-склад , D2W двері-склад, D2D двері-двері)
            "paidByRecipient" => false,
            "description" => $db['id'],
            "externalId" => $db['id'],
            //"externalId" => ,//Зовнішній ідентифікатор контрагента
            //Оплата за відправлення при отриманні.True – оплата одержувачем false -оплата відправником. По замовченню false
            //Оплата безготівковим розрахунком. True-безготівковий, false-готівковий. По замовченню tru
            "declaredPrice" => $db['money_id']['collect_amt'],
            "weight" => $db['box_id']['weight'],
            "length" => $db['box_id']['length'],
            "width" => $db['box_id']['width']
        ];
        /*debug($dataAddrSender);
        debug($dataAddrRecip);//
        debug($dataClientSender);
        debug($dataClientRecip);
        debug($dataShipment);
        exit;*/
        $addressSender = $this->makeAddress($dataAddrSender);
        $addressRecip = $this->makeAddress($dataAddrRecip);
        $clientSender = $this->makeClient($dataClientSender, $addressSender['id']); // uuid
        $clientRecip = $this->makeClient($dataClientRecip, $addressRecip['id']); // uuid
        $shipment = $this->makeShipment($dataShipment, $clientSender['uuid'], $clientRecip['uuid']);
        $this->inBarcode($shipment['barcode'],$db['id']);
        //debug($shipment);
        //exit;
        return $shipment;
    }

}