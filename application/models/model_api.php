<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 7/26/2018
 * Time: 1:21 PM
 */

class model_api extends Model
{

    public $arr;

    protected function changeBarcode($data,$id){
        foreach ($data as $key => $value){
            $value['barcode'] = $id;
            $data[$key] = $value;
        }
        return $data;
    }

    protected function getStatusesMy($barcode,$id){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM exspress_hawb WHERE hawb_id = "' . $barcode . '"');
        $query->execute();
        $info = $query->fetchAll();
        foreach ($info as $key => $var){
            $var['barcode']=$var['hawb_id'];
            $var['date']= $var['date_time'];
            unset($var['hawb_id']);
            unset($var['date_time']);
            unset($var['id']);
            $info[$key] = $var;
        }
        return $info;
    }

    protected function getStatusesUkrPoshta($barcode,$id){
        $str = "https://www.ukrposhta.ua/status-tracking/0.0.1/statuses?barcode=" . $barcode;
        $curl = curl_init($str);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            "Authorization: Bearer 9db61aa2-1201-3990-8b48-65cd87f58dd7 \n"
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        $data = $this->changeBarcode($result,$id);
        curl_close($curl);
        return $data;
    }

    protected function getInfoDbCode($id){
        $connect = new Database(HOST, DB, USER, PASS);
        //Екранируем запрос
        $query = $connect->db->prepare('SELECT * FROM hawb WHERE id=' . $id);
        $query->execute();
        $barcode = $query->fetchAll()[0];
        if($barcode){
            return $barcode;
        }else{
            // пустой массив
            return false;
        }
    }

    protected function getInfoDbToken($token){
        if($token){
            $connect = new Database(HOST, DB, USER, PASS);
            $query = $connect->db->prepare('SELECT id,token FROM user WHERE token = "' . $token . '"');
            $query->execute();
            $token = $query->fetchAll();
            if($token){
                return $token[0];
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    protected function comparisonCodeToken($barcode,$resToken){
        if($barcode['user_id'] != $resToken['id']){
            return false;
        }
    }


    public function getStatuses($token = null,$id,$opt = null){
        $barcode = $this->getInfoDbCode($id);
        if($barcode == false){
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
            return $data = [
                "Error" => 444,
                "Message" => "shipping is not processed",
            ];
        }
        $resToken = $this->getInfoDbToken($token);
        if($resToken > 0 ) {
            if ($barcode > 0) {
                $resComparison = $this->comparisonCodeToken($barcode,$resToken);
                if($resComparison !== false) {
                    $statuses = $this->getStatusesMy($barcode['id'], $id);
                    if($opt){
                        if($opt = 'last'){
                            $count = count($statuses);
                            return $statuses[$count-1];
                        }
                    }else {
                        return $statuses;
                    }
                } else{
                    //если запрашиваемый бар-код добавлен пользователем не с эти токеном
                    header("HTTP/1.0 404 Not Found");
                    header("Status: 404 Not Found");
                    return $data = [
                        "Error" => 111,
                        "Message" => "No access to the specified bar code",
                    ];
                }
            } else {
                //если пустой массив то вернуть код ошибки
                header("HTTP/1.0 404 Not Found");
                header("Status: 404 Not Found");
                return $data = [
                    "Error" => 222,
                    "Message" => "No matches on the bar code",
                ];
            }
        }else{
            // если токен пустой либо нет совпадения в базе данных
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
            return $data = [
                "Error" => 333,
                "Message" => "The token is not valid",
            ];
        }
    }

    /*public function getStatuses($token = null, $id, $opt = null)
    {
        $barcode = $this->getInfoDbCode($id);
        if ($barcode['barcode'] === null) {
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
            return $data = [
                "Error" => 444,
                "Message" => "shipping is not processed",
            ];
        }
        $resToken = $this->getInfoDbToken($token);
        if ($resToken > 0) {
            if ($barcode > 0) {
                $resComparison = $this->comparisonCodeToken($barcode, $resToken);
                if ($resComparison !== false) {
                    $statuses = $this->getStatusesUkrPoshta($barcode['barcode'], $id);
                    if ($opt) {
                        if ($opt = 'last') {
                            $count = count($statuses);
                            return $statuses[$count - 1];
                        }
                    } else {
                        return $statuses;
                    }
                } else {
                    //если запрашиваемый бар-код добавлен пользователем не с эти токеном
                    header("HTTP/1.0 404 Not Found");
                    header("Status: 404 Not Found");
                    return $data = [
                        "Error" => 111,
                        "Message" => "No access to the specified bar code",
                    ];
                }
            } else {
                //если пустой массив то вернуть код ошибки
                header("HTTP/1.0 404 Not Found");
                header("Status: 404 Not Found");
                return $data = [
                    "Error" => 222,
                    "Message" => "No matches on the bar code",
                ];
            }
        } else {
            // если токен пустой либо нет совпадения в базе данных
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
            return $data = [
                "Error" => 333,
                "Message" => "The token is not valid",
            ];
        }
    }*/


}