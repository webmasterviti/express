<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/24/2018
 * Time: 12:15 PM
 */

class model_hawb_excel extends Model
{
    private $excel = [
        'AWB' => [],
        'Origin' => [],
        'PCS' => [],
        'Weight' => [],
        'CommodityDescription' => [],
        'Customs' => [],
        'CustomsCurrency' => [],
        'ShipperName' => [],
        'OriginCountry' => [],
        'ConsigneeName' => [],
        'Reference1' => [],
        'FinalETA' => [],
        'HAWBOrigin' => [],
        'Type' => [],
        'PickupDate' => [],
        'ConsigneeReference' => [],
        'ConsigneeNumber' => [],
        'AttnOf' => [],
        'ConsigneeAddress' => [],
        'ConsigneeTel' => [],
        'ConsigneeEmail' => [],
        'DestCity' => [],
        'DestCountry' => [],
        'CollectAmount' => [],
        'PaymentType' => [],
        'SQL' => [],
    ];
    private $xls;
    private $sheet;
    private $connect;
    private $array = [];
    public $countAddDelivery = 0;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
    }

    private function readHead()
    {
        array_push($this->array, [
            'origin' => $this->sheet->getCellByColumnAndRow(2, 2)->getValue(),
            'destination' => $this->sheet->getCellByColumnAndRow(2, 3)->getValue(),
            'hawb' => $this->sheet->getCellByColumnAndRow(4, 2)->getValue(),
            'manifest' => $this->sheet->getCellByColumnAndRow(4, 3)->getValue()
        ]);
    }

    private function readSnipper()
    {
        array_push($this->array, [
            'account' => $this->sheet->getCellByColumnAndRow(2, 6)->getValue(),
            'contry' => $this->sheet->getCellByColumnAndRow(2, 7)->getValue(),
            'phone' => $this->sheet->getCellByColumnAndRow(2, 8)->getValue(),
            'phone2' => $this->sheet->getCellByColumnAndRow(2, 9)->getValue(),
            'mobile' => $this->sheet->getCellByColumnAndRow(2, 10)->getValue(),
            'name_s' => $this->sheet->getCellByColumnAndRow(2, 11)->getValue(),
            'sent_by' => $this->sheet->getCellByColumnAndRow(2, 12)->getValue(),
            'ref' => $this->sheet->getCellByColumnAndRow(2, 13)->getValue(),
            'ref2' => $this->sheet->getCellByColumnAndRow(2, 14)->getValue(),
            'address' => $this->sheet->getCellByColumnAndRow(2, 15)->getValue(),
            'city' => $this->sheet->getCellByColumnAndRow(2, 16)->getValue(),
            'state' => $this->sheet->getCellByColumnAndRow(2, 17)->getValue(),
            'zip' => $this->sheet->getCellByColumnAndRow(2, 18)->getValue()
        ]);
    }

    private function readConsignee()
    {
        array_push($this->array, [
            'account' => $this->sheet->getCellByColumnAndRow(4, 6)->getValue(),
            'contry' => $this->sheet->getCellByColumnAndRow(4, 7)->getValue(),
            'phone' => $this->sheet->getCellByColumnAndRow(4, 8)->getValue(),
            'phone2' => $this->sheet->getCellByColumnAndRow(4, 9)->getValue(),
            'mobile' => $this->sheet->getCellByColumnAndRow(4, 10)->getValue(),
            'name_c' => $this->sheet->getCellByColumnAndRow(4, 11)->getValue(),
            'attn' => $this->sheet->getCellByColumnAndRow(4, 12)->getValue(),
            'ref' => $this->sheet->getCellByColumnAndRow(4, 13)->getValue(),
            'ref2' => $this->sheet->getCellByColumnAndRow(4, 14)->getValue(),
            'address' => $this->sheet->getCellByColumnAndRow(4, 15)->getValue(),
            'city' => $this->sheet->getCellByColumnAndRow(4, 16)->getValue(),
            'state' => $this->sheet->getCellByColumnAndRow(4, 17)->getValue(),
            'zip' => $this->sheet->getCellByColumnAndRow(4, 18)->getValue()
        ]);
    }

    private function readPayment()
    {
        array_push($this->array, [
            'payment' => $this->sheet->getCellByColumnAndRow(1, 21)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(2, 21)->getValue(),
            'srn_no' => $this->sheet->getCellByColumnAndRow(2, 22)->getValue(),
            'collection_ref' => $this->sheet->getCellByColumnAndRow(2, 23)->getValue(),
            'collect_amt' => $this->sheet->getCellByColumnAndRow(2, 24)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 24)->getValue(),
            'cod_amt' => $this->sheet->getCellByColumnAndRow(2, 25)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 25)->getValue(),
            'cash_amt' => $this->sheet->getCellByColumnAndRow(2, 26)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 26)->getValue(),
            'awb_referense' => $this->sheet->getCellByColumnAndRow(2, 27)->getValue(),
        ]);
    }

    private function readParsel()
    {
        array_push($this->array, [
            'good_value' => $this->sheet->getCellByColumnAndRow(2, 30)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 30)->getValue(),
            'pickup_by' => $this->sheet->getCellByColumnAndRow(2, 31)->getValue(),
            'shield_value' => $this->sheet->getCellByColumnAndRow(4, 32)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 32)->getValue(),
            'weight' => $this->sheet->getCellByColumnAndRow(2, 33)->getValue(),
            'pcs' => $this->sheet->getCellByColumnAndRow(2, 34)->getValue(),
            'cube' => $this->sheet->getCellByColumnAndRow(2, 35)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 35)->getValue(),
            'factor' => $this->sheet->getCellByColumnAndRow(2, 36)->getValue(),
            'product_1' => $this->sheet->getCellByColumnAndRow(2, 37)->getValue(),
            'product_2' => $this->sheet->getCellByColumnAndRow(3, 37)->getValue(),
            'due_dt' => $this->sheet->getCellByColumnAndRow(2, 38)->getValue(),
            'pickup_dt' => $this->sheet->getCellByColumnAndRow(2, 39)->getValue() . ' ' . $this->sheet->getCellByColumnAndRow(3, 39)->getValue(),
            'loc' => $this->sheet->getCellByColumnAndRow(2, 40)->getValue(),
            'remarks' => $this->sheet->getCellByColumnAndRow(2, 41)->getValue(),
            'good_org' => $this->sheet->getCellByColumnAndRow(2, 42)->getValue(),
            'desc' => $this->sheet->getCellByColumnAndRow(2, 43)->getValue(),
        ]);
    }

    private function writeDbFromExcel()
    {
        //====================================================Insert Snipper
        $snipper = $this->array[1];
        $statement = $this->connect->db->prepare('INSERT INTO snipper(id, account, contry, phone, phone2, name_s, sent_by, ref,ref2,address,city, state,zip) VALUES (:id, :account, :contry, :phone, :phone2, :name_s, :sent_by, :ref,:ref2,:address,:city, :state,:zip)');
        $id = 0;
        $account = $snipper['account'];
        $contry = $snipper['contry'];
        $phone = $snipper['phone'];
        $phone2 = $snipper['phone2'];
        $name_s = $snipper['name_s'];
        $sent_by = $snipper['sent_by'];
        $ref = $snipper['ref'];
        $ref2 = $snipper['ref2'];
        $address = $snipper['address'];
        $city = $snipper['city'];
        $state = $snipper['state'];
        $zip = $snipper['zip'];

        $statement->bindParam(':id', $id);
        $statement->bindParam(':account', $account);
        $statement->bindParam(':contry', $contry);
        $statement->bindParam(':phone', $phone);
        $statement->bindParam(':phone2', $phone2);
        $statement->bindParam(':name_s', $name_s);
        $statement->bindParam(':sent_by', $sent_by);
        $statement->bindParam(':ref', $ref);
        $statement->bindParam(':ref2', $ref2);
        $statement->bindParam(':address', $address);
        $statement->bindParam(':city', $city);
        $statement->bindParam(':state', $state);
        $statement->bindParam(':zip', $zip);

        $statement->execute();
        $id_snipper = $this->connect->db->lastInsertId();
        unset($statement);
        ////===================================================================


        //=============================================Insert consignee
        $consignee = $this->array[2];

        $statement = $this->connect->db->prepare('INSERT INTO consignee(id, account, contry, phone, phone2, name_c, attn, ref,ref2,address,city, state,zip) VALUES (:id, :account, :contry, :phone, :phone2, :name_c, :attn, :ref,:ref2,:address,:city, :state,:zip)');

        $account = $consignee['account'];
        $contry = $consignee['contry'];
        $phone = $consignee['phone'];
        $phone2 = $consignee['phone2'];
        $name_c = $consignee['name_c'];
        $attn = $consignee['attn'];
        $ref = $consignee['ref'];
        $ref2 = $consignee['ref2'];
        $address = $consignee['address'];
        $city = $consignee['city'];
        $state = $consignee['state'];
        $zip = $consignee['zip'];

        $statement->bindParam(':id', $id);
        $statement->bindParam(':account', $account);
        $statement->bindParam(':contry', $contry);
        $statement->bindParam(':phone', $phone);
        $statement->bindParam(':phone2', $phone2);
        $statement->bindParam(':name_c', $name_c);
        $statement->bindParam(':attn', $attn);
        $statement->bindParam(':ref', $ref);
        $statement->bindParam(':ref2', $ref2);
        $statement->bindParam(':address', $address);
        $statement->bindParam(':city', $city);
        $statement->bindParam(':state', $state);
        $statement->bindParam(':zip', $zip);

        $statement->execute();
        $id_consignee = $this->connect->db->lastInsertId();
        unset($statement);
        //=======================================================================


        //=============================================Insert money

        $money = $this->array[3];

        $statement = $this->connect->db->prepare('INSERT INTO money(id, payment, srn_no, collection_ref, collect_amt, cod_amt , cash_amt, awb_reference) VALUES (:id, :payment_id, :srn_no, :collection_ref, :collect_amt_id, :cod_amt_id , :cash_amt_id, :awb_reference)');

        $payment = $money['payment'];
        $srn_no = $money['srn_no'];
        $collection_ref = $money['collection_ref'];
        $collect_amt = $money['collect_amt'];
        $cod_amt = $money['cod_amt'];
        $cash_amt = $money['cash_amt'];
        $awb_reference = $money['awb_reference'];

        $statement->bindParam(':id', $id_p);
        $statement->bindParam(':payment_id', $payment);
        $statement->bindParam(':srn_no', $srn_no);
        $statement->bindParam(':collection_ref', $collection_ref);
        $statement->bindParam(':collect_amt_id', $collect_amt);
        $statement->bindParam(':cod_amt_id', $cod_amt);
        $statement->bindParam(':cash_amt_id', $cash_amt);
        $statement->bindParam(':awb_reference', $awb_reference);

        $statement->execute();
        $id_payment = $this->connect->db->lastInsertId();
        unset($statement);

        //=======================================================================


        //=============================================Insert box

        $box = $this->array[4];

        $statement = $this->connect->db->prepare('INSERT INTO box(id, goods_value, pickup_by, shield_value, weight, charg_wt , pcs, cube,factor,product_1,due_dt,pickup_dt,loc,remarks,goods_org,description,product_2) VALUES (:id, :goods_value, :pickup_by, :shield_value, :weight, :charg_wt , :pcs, :cube,:factor,:product_1,:due_dt,:pickup_dt,:loc,:remarks,:goods_org,:description, :product_2)');

        $goods_value = $box['goods_value'];
        $pickup_by = $box['pickup_by'];
        $shield_value = $box['shield_value'];
        $weight = $box['weight'];
        $charg_wt = $box['charg_wt'];
        $pcs = $box['pcs'];
        $cube = $box['cube'];
        $factor = $box['factor'];
        $product_1 = $box['product_1'];
        $product_2 = $box['product_2'];
        $due_dt = $box['due_dt'];
        $pickup_dt = $box['pickup_dt'];
        $loc = $box['loc'];
        $remarks = $box['remarks'];
        $goods_org = $box['goods_org'];
        $description = $box['description'];

        $statement->bindParam(':id', $id);
        $statement->bindParam(':goods_value', $goods_value);
        $statement->bindParam(':pickup_by', $pickup_by);
        $statement->bindParam(':shield_value', $shield_value);
        $statement->bindParam(':weight', $weight);
        $statement->bindParam(':charg_wt', $charg_wt);
        $statement->bindParam(':pcs', $pcs);
        $statement->bindParam(':cube', $cube);
        $statement->bindParam(':factor', $factor);
        $statement->bindParam(':product_1', $product_1);
        $statement->bindParam(':product_2', $product_2);
        $statement->bindParam(':due_dt', $due_dt);
        $statement->bindParam(':pickup_dt', $pickup_dt);
        $statement->bindParam(':loc', $loc);
        $statement->bindParam(':remarks', $remarks);
        $statement->bindParam(':goods_org', $goods_org);
        $statement->bindParam(':description', $description);

        $statement->execute();
        $id_box = $this->connect->db->lastInsertId();
        unset($statement);
        //========================================================


        //=============================================Insert hawb
        $hawb = $this->array['0'];
        $statement = $this->connect->db->prepare('INSERT INTO hawb(id,origin,destination,manifest,snipper_id,consignee_id,box_id,money_id) VALUES (:id,:origin,:destination,:manifest,:snipper_id,:consignee_id,:box_id,:money_id)');

        $id_hawb = $hawb['hawb'];
        $origin = $hawb['origin'];
        $destination = $hawb['destination'];
        $manifest = $hawb['manifest'];
        $snipper_id = $id_snipper;
        $consignee_id = $id_consignee;
        $box_id = $id_box;
        $money_id = $id_payment;

        $statement->bindParam(':id', $id_hawb);
        $statement->bindParam(':origin', $origin);
        $statement->bindParam(':destination', $destination);
        $statement->bindParam(':manifest', $manifest);
        $statement->bindParam(':snipper_id', $snipper_id);
        $statement->bindParam(':consignee_id', $consignee_id);
        $statement->bindParam(':box_id', $box_id);
        $statement->bindParam(':money_id', $money_id);
        $statement->execute();
        //========================================================
    }

    public function addFromNewExcel()
    {
        if (isset($_FILES['upload']['tmp_name'])) {

            $uploadInfo = $_FILES['upload'];
            if ($uploadInfo['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' or $uploadInfo['type'] == 'application/vnd.ms-excel') {
                try {
                    require_once('../core/Classes/PHPExcel/IOFactory.php');
                    // Открываем файл
                    $this->xls = PHPExcel_IOFactory::load($uploadInfo['tmp_name']);
                    $countSheet = $sheetCount = $this->xls->getSheetCount();
                    $i = 0;
                    while ($i < $countSheet) {
                        // Устанавливаем индекс активного листа
                        $this->xls->setActiveSheetIndex($i);
                        // Получаем активный лист
                        $this->sheet = $this->xls->getActiveSheet();
                        $this->readHead();
                        $this->readSnipper();
                        $this->readConsignee();
                        $this->readPayment();
                        $this->readParsel();
                        /*echo '<pre>';
                        print_r($this->array);
                        echo '</pre>';*/
                        $this->writeDbFromExcel();
                        $i++;
                        unset($this->array);
                        $this->array = [];
                    }
                    return TRUE;
                } catch (Exception $e) {
                    return FALSE;
                }

            }
        }
    }

    private function addStatus($id){
        $connect = new Database(HOST, DB, USER, PASS);
        $statement = $connect->db->prepare("SELECT * FROM exspress_hawb WHERE hawb_id = {$id}");
        $statement->execute();
        $data = $statement->fetchAll();
        if(empty($data)) {
            date_default_timezone_set('Europe/Kiev');
            $date = date("F j, Y, g:i a");
            $location = "Китай, Пекин";
            $activity = 'Record created.';
            //////Insert Express
            ///
            $sqlExpress = "INSERT INTO exspress_hawb(date_time,location,activity,hawb_id) VALUES(:date_time,:location,:activity,:hawb_id)";
            $statement = $connect->db->prepare($sqlExpress);
            $statement->bindParam(':date_time', $date);
            $statement->bindParam(':location', $location);
            $statement->bindParam(':activity', $activity);
            $statement->bindParam(':hawb_id', $id);
            try {
                $statement->execute();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
                return FALSE;
            }
            $id_express = $connect->db->lastInsertId();
            unset($statement);
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function addFromOldExcel($user_id, $batch_name)
    {
        //Проверяем наличие файла во временной деректории
        if (isset($_FILES['upload']['tmp_name'])) {
            $uploadInfo = $_FILES['upload'];
            //Проверяем форма передаваемого файла
            if ($uploadInfo['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' or $uploadInfo['type'] == 'application/vnd.ms-excel' or $uploadInfo['type'] == 'application/octet-stream') {
                //Создаем подключенение к базе данных используя класс Database
                $connect = new Database(HOST, DB, USER, PASS);
                //Класс для чтения
                try {
                    require './application/core/Classes/PHPExcel/IOFactory.php';
                    //Класс для работы с датами в Excel
                    require './application/core/Classes/PHPExcel/Shared/Date.php';
                }catch(Exception $e){
                    echo $e->getMessage();
                }
                $xls = PHPExcel_IOFactory::load($uploadInfo['tmp_name']);
                // Устанавливаем индекс активного листа
                $xls->setActiveSheetIndex(0);

                // Получаем активный лист
                $sheet = $xls->getActiveSheet();
                //count row on sheet
                $countRow = $sheet->getHighestRow();
                //count column on sheet
                $countColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
                ////Достаем название каждой колонке
                for ($i = 0; $i < $countColumn; $i++) {
                    //значение колонки
                    $valueColumn = $sheet->getCellByColumnAndRow($i, 1)->getValue();
                    //сравниваем полученое значение колонки со значениями в свойстве excel
                    foreach ($this->excel as $key => $value) {
                        //Проверям совпадения
                        if ($valueColumn == $key) {
                            //Если совпадение то начинаем проходится по рядку
                            for ($j = 2; $j < $countRow + 1; $j++) {
                                //Получаем значение ячейки рядка в колонке $i
                                $data = $sheet->getCellByColumnAndRow($i, $j)->getValue();
                                //Если значение этого елемента равно условию выполняем дополнительную обработку
                                //Используем класс Date для получения с ячейки даты
                                if ($valueColumn == 'FinalETA' or $valueColumn == 'PickupDate') {
                                    //Преобразовуем в нужный нам формат
                                    $data = date("Y-m-d H:i:s", PHPExcel_Shared_Date::ExcelToPHP($data));
                                }
                                //Если значение этого елемента равно условию выполняем дополнительную обработку
                                if ($valueColumn == 'Customs' or $valueColumn == 'Weight') {
                                    //количество оставляемых символов
                                    $pos = strrpos($data, '.');
                                    if ($pos !== false) {
                                        $data = substr($data, 0, $pos + 1 + 2);
                                    }
                                }
                                //Добавляем в массив значение ячеки рядка
                                $value [] = $data;
                            }
                            //После прохождение всех рядков присваиваем заполнений масив $value в свойство класс с ключем $key
                            $this->excel[$key] = $value;
                        }
                    }
                }
                //Заносим в базу данных
                //Начинаем собирать запрос
                //Проходимся по первому елементу в каждом массиве
                /*echo '<pre>';
                print_r($this->excel);
                echo '</pre>';
                echo $countRow -1;
                exit;*/
                for ($i = 0; $i < $countRow -1; $i++) {
                    //Запрос для таблицы hawb
                    $sqlHawb = "INSERT INTO hawb(excel,user_id,batch_name,final_eta,";
                    //Запрос для таблицы snipper
                    $sqlSnipper = "INSERT INTO snipper(";
                    //Запрос для таблицы consignee
                    $sqlConsignee = "INSERT INTO consignee(";
                    //Запрос для таблицы money
                    $sqlMoney = "INSERT INTO money(";
                    //Запрос для таблицы box
                    $sqlBox = "INSERT INTO box(";

                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            //Добавляем к запросу колонки при наличи ключей в свойстве
                            switch ($key) {
                                case 'AWB': {
                                    $sqlHawb .= 'id,';
                                    break;
                                }
                                case 'HAWBOrigin': {
                                    $sqlHawb .= 'origin,';
                                    break;
                                }
                                case 'OriginCountry': {
                                    $sqlSnipper .= 'contry,';
                                    break;
                                }
                                case 'ShipperName': {
                                    $sqlSnipper .= 'name_s,';
                                    break;
                                }
                                case 'Reference1': {
                                    $sqlSnipper .= 'ref,';
                                    break;
                                }
                                case 'ConsigneeReference': {
                                    $sqlConsignee .= 'ref,';
                                    break;
                                }
                                case 'ConsigneeAddress': {
                                    $sqlConsignee .= 'address,';
                                    break;
                                }
                                case 'ConsigneeName': {
                                    $sqlConsignee .= 'name_c,';
                                    break;
                                }
                                case 'ConsigneeTel': {
                                    $sqlConsignee .= 'phone,';
                                    break;
                                }
                                /*case 'ConsigneeEmail':{
                                    $sqlConsignee .= 'phone,';
                                    break;
                                }*/
                                case 'DestCity': {
                                    $sqlConsignee .= 'city,';
                                    break;
                                }
                                case 'DestCountry': {
                                    $sqlConsignee .= 'contry,';
                                    break;
                                }
                                case 'AttnOf': {
                                    $sqlConsignee .= 'attn,';
                                    break;
                                }
                                case 'PCS': {
                                    $sqlBox .= 'pcs,';
                                    break;
                                }
                                case 'Weight': {
                                    $sqlBox .= 'weight,';
                                    break;
                                }
                                case 'CommodityDescription': {
                                    $sqlBox .= 'description,';
                                    break;
                                }
                                case 'Customs': {
                                    $sqlBox .= 'goods_value,';
                                    break;
                                }
                                case 'PickupDate': {
                                    $sqlBox .= 'pickup_dt,';
                                    break;
                                }
                                case 'CollectAmount': {
                                    $sqlMoney .= 'collect_amt,';
                                    break;
                                }
                                case 'PaymentType': {
                                    $sqlMoney .= 'payment,';
                                    break;
                                }
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlMoney); $str++) {

                        if ($str == (strlen($sqlMoney) - 1)) {
                            //Если последняя не кома значит значений для заполнения нету
                            if ($sqlMoney{$str} != ',') {
                                unset($sqlMoney);
                            }
                            if ($sqlMoney{$str} == ',') {
                                $sqlMoney = substr($sqlMoney, 0, $str);
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlBox); $str++) {
                        //Если последняя не кома значит значений для заполнения нету
                        if ($str == (strlen($sqlBox) - 1)) {
                            if ($sqlBox{$str} != ',') {
                                unset($sqlBox);
                            }
                            if ($sqlBox{$str} == ',') {
                                $sqlBox = substr($sqlBox, 0, $str);
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlSnipper); $str++) {
                        //Если последняя не кома значит значений для заполнения нету
                        if ($str == (strlen($sqlSnipper) - 1)) {
                            if ($sqlSnipper{$str} != ',') {
                                unset($sqlSnipper);
                            }
                            if ($sqlSnipper{$str} == ',') {
                                $sqlSnipper = substr($sqlSnipper, 0, $str);
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlConsignee); $str++) {
                        //Если последняя не кома значит значений для заполнения нету
                        if ($str == (strlen($sqlConsignee) - 1)) {
                            if ($sqlConsignee{$str} != ',') {
                                unset($sqlConsignee);
                            }
                            if ($sqlConsignee{$str} == ',') {
                                $sqlConsignee = substr($sqlConsignee, 0, $str);
                            }
                        }
                    }

                    if (isset($sqlMoney)) {
                        $sqlMoney .= ') VALUES(';
                        $sqlHawb .= ' money_id,';
                    }
                    if (isset($sqlConsignee)) {
                        $sqlConsignee .= ') VALUES(';
                        $sqlHawb .= ' consignee_id,';
                    }
                    if (isset($sqlSnipper)) {
                        $sqlSnipper .= ') VALUES(';
                        $sqlHawb .= ' snipper_id,';
                    }
                    if(isset($sqlExpress)){
                        $sqlHawb .=' express_id,';
                    }
                    if (isset($sqlBox)) {
                        $sqlBox .= ') VALUES(';
                        $sqlHawb .= ' box_id,';
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlHawb); $str++) {
                        //Если последняя не кома значит значений для заполнения нету
                        if ($str == (strlen($sqlHawb) - 1)) {
                            if ($sqlHawb{$str} == ',') {
                                $sqlHawb = substr($sqlHawb, 0, $str);
                            }
                        }
                    }
                    if (isset($sqlHawb)) {
                        $sqlHawb .= ') VALUES(:excel,:user_id,:batch_name,:final_eta,';
                    }
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            //Добавляем к запросу колонки при наличи ключей в свойстве
                            switch ($key) {
                                case 'AWB': {
                                    $sqlHawb .= ':id,';
                                    break;
                                }
                                case 'HAWBOrigin': {
                                    $sqlHawb .= ':origin,';
                                    break;
                                }
                                case 'OriginCountry': {
                                    $sqlSnipper .= ':contry,';
                                    break;
                                }
                                case 'ShipperName': {
                                    $sqlSnipper .= ':name_s,';
                                    break;
                                }
                                case 'Reference1': {
                                    $sqlSnipper .= ':ref,';
                                    break;
                                }
                                case 'ConsigneeReference': {
                                    $sqlConsignee .= ':ref,';
                                    break;
                                }
                                case 'ConsigneeAddress': {
                                    $sqlConsignee .= ':address,';
                                    break;
                                }
                                case 'ConsigneeName': {
                                    $sqlConsignee .= ':name_c,';
                                    break;
                                }
                                case 'ConsigneeTel': {
                                    $sqlConsignee .= ':phone,';
                                    break;
                                }
                                /*case 'ConsigneeEmail':{
                                    $sqlConsignee .= ':email,';
                                    break;
                                }*/
                                case 'DestCity': {
                                    $sqlConsignee .= ':city,';
                                    break;
                                }
                                case 'DestCountry': {
                                    $sqlConsignee .= ':contry,';
                                    break;
                                }
                                case 'AttnOf': {
                                    $sqlConsignee .= ':attn,';
                                    break;
                                }
                                case 'PCS': {
                                    $sqlBox .= ':pcs,';
                                    break;
                                }
                                case 'Weight': {
                                    $sqlBox .= ':weight,';
                                    break;
                                }
                                case 'CommodityDescription': {
                                    $sqlBox .= ':description,';
                                    break;
                                }
                                case 'Customs': {
                                    $sqlBox .= ':goods_value,';
                                    break;
                                }
                                case 'PickupDate': {
                                    $sqlBox .= ':pickup_dt,';
                                    break;
                                }
                                case 'CollectAmount': {
                                    $sqlMoney .= ':collect_amt,';
                                    break;
                                }
                                case 'PaymentType': {
                                    $sqlMoney .= ':payment,';
                                    break;
                                }
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlMoney); $str++) {

                        if ($str == (strlen($sqlMoney) - 1)) {
                            if ($sqlMoney{$str} == ',') {
                                $sqlMoney = substr($sqlMoney, 0, $str);
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlBox); $str++) {
                        if ($str == (strlen($sqlBox) - 1)) {
                            if ($sqlBox{$str} == ',') {
                                $sqlBox = substr($sqlBox, 0, $str);
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlSnipper); $str++) {
                        if ($str == (strlen($sqlSnipper) - 1)) {
                            if ($sqlSnipper{$str} == ',') {
                                $sqlSnipper = substr($sqlSnipper, 0, $str);
                            }
                        }
                    }
                    //убераем кому
                    for ($str = 0; $str < strlen($sqlConsignee); $str++) {
                        if ($str == (strlen($sqlConsignee) - 1)) {
                            if ($sqlConsignee{$str} == ',') {
                                $sqlConsignee = substr($sqlConsignee, 0, $str);
                            }
                        }
                    }
                    if (isset($sqlMoney)) {
                        $sqlMoney .= ')';
                        $sqlHawb .= ' :money_id,';
                    }
                    if (isset($sqlConsignee)) {
                        $sqlConsignee .= ')';
                        $sqlHawb .= ' :consignee_id,';
                    }
                    if (isset($sqlSnipper)) {
                        $sqlSnipper .= ')';
                        $sqlHawb .= ' :snipper_id,';
                    }
                    if (isset($sqlBox)) {
                        $sqlBox .= ') ';
                        $sqlHawb .= ' :box_id,';
                    }

                    //убераем кому
                    for ($str = 0; $str < strlen($sqlHawb); $str++) {
                        //Если последняя не кома значит значений для заполнения нету
                        if ($str == (strlen($sqlHawb) - 1)) {
                            if ($sqlHawb{$str} == ',') {
                                $sqlHawb = substr($sqlHawb, 0, $str);
                            }
                        }
                    }
                    if (isset($sqlHawb)) {
                        $sqlHawb .= ')';
                    }

                    //////
                    ///
                    ///
                    ///
                    ///
                    //////Insert Snipper
                    $statement = $connect->db->prepare($sqlSnipper);
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            switch ($key) {
                                case 'ShipperName': {
                                    $statement->bindParam(':name_s', $value[$i]);
                                    break;
                                }
                                case 'OriginCountry': {
                                    $statement->bindParam(':contry', $value[$i]);
                                    break;
                                }
                                case 'Reference1': {
                                    $statement->bindParam(':ref', $value[$i]);
                                    break;
                                }
                            }
                        }
                    }
                    try {
                        $statement->execute();
                    } catch (Exception $e){
                        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                        return FALSE;
                    }
                   $id_snipper = $connect->db->lastInsertId();
                   unset($statement);

                   ///////////
                    ///
                    ///
                    ////
                   /////////////Insert Consignee
                    $statement = $connect->db->prepare($sqlConsignee);
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                           switch ($key) {
                                case 'ConsigneeReference': {
                                    $statement->bindParam(':ref', $value[$i]);
                                    break;
                                }
                                case 'ConsigneeAddress': {
                                    $statement->bindParam(':address', $value[$i]);
                                    break;
                                }
                                case 'ConsigneeName': {
                                    $statement->bindParam(':name_c', $value[$i]);
                                    break;
                                }
                                case 'ConsigneeTel': {
                                    $statement->bindParam(':phone', $value[$i]);
                                    break;
                                }
                                case 'DestCity': {
                                    $statement->bindParam(':city', $value[$i]);
                                    break;
                                }
                                case 'DestCountry': {
                                    $statement->bindParam(':contry', $value[$i]);
                                    break;
                                }
                                case 'AttnOf': {
                                    $statement->bindParam(':attn', $value[$i]);
                                    break;
                                }
                            }
                        }
                    }
                    try {
                        $statement->execute();
                    } catch (Exception $e){
                        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                        return FALSE;
                    }
                    $id_consignee = $connect->db->lastInsertId();
                    unset($statement);
                    ///////////////
                    ///
                    ///
                    ///
                    ///
                    /////////////Insert Money
                    $statement = $connect->db->prepare($sqlMoney);
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            switch ($key) {
                                case 'CollectAmount': {
                                    $statement->bindParam(':collect_amt',$value[$i]);
                                    break;
                                }
                                case 'PaymentType': {
                                    $statement->bindParam(':payment',$value[$i]);
                                    break;
                                }
                            }
                        }
                    }
                    try {
                        $statement->execute();
                    } catch (Exception $e){
                        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                        return FALSE;
                    }
                    $id_money = $connect->db->lastInsertId();
                    unset($statement);
                    ///////////////
                    ///
                    ///
                    ///
                    ///
                    /// /////////////Insert Box
                    $statement = $connect->db->prepare($sqlBox);
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            switch ($key) {
                                case 'PCS': {
                                    $statement->bindParam(':pcs',$value[$i]);
                                    break;
                                }
                                case 'Weight': {
                                    $statement->bindParam(':weight',$value[$i]);
                                    break;
                                }
                                case 'CommodityDescription': {
                                    $statement->bindParam(':description',$value[$i]);
                                    break;
                                }
                                case 'Customs': {
                                    $statement->bindParam(':goods_value',$value[$i]);
                                    break;
                                }
                                case 'PickupDate': {
                                    $statement->bindParam(':pickup_dt',$value[$i]);
                                    break;
                                }
                            }
                        }
                    }
                    try {
                        $statement->execute();
                    } catch (Exception $e){
                        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                        return FALSE;
                    }
                    $id_box = $connect->db->lastInsertId();
                    unset($statement);
                    ///////////////
                    ///
                    ///
                    ///
                    ///
                    /// /////////////Insert HAWB
                    $statement = $connect->db->prepare($sqlHawb);
                    foreach ($this->excel as $key => $value) {
                        if (isset($value[$i])) {
                            switch ($key) {
                                case 'AWB': {
                                    $sqlHawb .= 'id,';
                                    $statement->bindParam(':id',$value[$i]);
                                    $this->addStatus($value[$i]);
                                    break;
                                }
                                case 'HAWBOrigin': {
                                    $statement->bindParam(':origin',$value[$i]);
                                    break;
                                }
                            }
                        }
                    }
                    $bool = TRUE;
                    try {
                        $statement->bindParam(':money_id',$id_money);
                        $statement->bindParam(':consignee_id',$id_consignee);
                        $statement->bindParam(':snipper_id',$id_snipper);
                        $statement->bindParam(':box_id',$id_box);
                        $statement->bindParam(':excel', $bool);
                        $statement->bindParam(':user_id', $user_id);
                        $statement->bindParam(':batch_name', $batch_name);
                        $statement->bindParam(':final_eta', $this->excel[$i]['FinalETA']);
                        $statement->execute();
                        $this->countDelivery++;

                    } catch (Exception $e){
                        //echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
                    }
                    /////////////////////////
                    unset($sqlConsignee);
                    unset($sqlSnipper);
                    unset($sqlBox);
                    unset($sqlMoney);
                    unset($sqlHawb);

                }
                /*echo '<pre>';
                print_r($this->excel);
                echo '</pre>';
                exit;*/

                //$query = $connect->db->prepare($sql);
               // $query->execute();
                return TRUE;
            }
            return 'Type not exist';
        }
        return NULL;
    }


}