<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 8/1/2018
 * Time: 3:09 PM
 */

class model_hawb_status extends Model
{


    public $countUpdateParcelForBatch = 0;

    public function statusTest($id)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM exspress_hawb WHERE hawb_id =' . $id);
        $query->execute();
        $result_status = $query->fetchAll();
        if (empty($result_status)) {
            return false;
        } else {
            return true;
        }
    }

    public function statusDelete($id)
    {
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $data = [
                'id' => $id,
            ];
            $sql = "DELETE FROM exspress_hawb WHERE id =  :id";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    protected function statusInfo($arr)
    {
        $table = '';
        for ($i = count($arr) - 1; $i > -1; $i--) {
            $table .= "<tr>
                            <td>{$arr[$i]['date_time']}
                            </td>
                            <td>{$arr[$i]['location']}</td>
                            <td><p>{$arr[$i]['activity']}</p></td>
                            <td><form action=\"\\hawb_status\" method=\"post\">
                            <button class=\"btn btn-primary mt-10\" type=\"submit\" name=\"delete_status\">Delete</button>
                                <input style=\"display: none;\" type=\"text\" hidden=\"hidden\" class=\"form-control\" id=\"hawb_id\" name=\"hawb_id\" value=\"{$arr[$i]['id']}\">
                                <input style=\"display: none;\" type=\"text\" hidden=\"hidden\" class=\"form - control\" id=\"hawb_number\" name=\"hawb_number\" value=\"{$arr[$i]['hawb_id']}\">
                </form></td>
                        </tr>";
        }
        return $table;
    }

    public function getSelect()
    {
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $sql = "SELECT batch_name FROM hawb WHERE batch_name IS NOT NULL and archive IS NULL";
            $result = $connect->db->prepare($sql);
            $result->execute();
            $query = $result->fetchAll();
        } catch (Exception $e) {
            return false;
        }
        $count = count($query);
        for ($i = 0; $i < $count; $i++) {
            if (isset($query[$i])) {
                for ($j = $i + 1; $j < $count; $j++) {
                    if ($query[$j] == $query[$i]) {
                        unset($query[$j]);
                    }
                }
            }
        }
        $text = "<select class=\"form-control\" style=\"width: 70%;margin-top: 0px;\" tabindex=\"-1\" aria-hidden=\"true\" name=\"select\">";
        $text .= "<option selected=\"selected\">Выберете отчёт</option>";
        foreach ($query as $key => $value) {
            $text .= "<option value=\"{$value['batch_name']}\">{$value['batch_name']}</option>";
        }
        $text .= "</select>";
        return $text;
    }

    public function getData($id)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM exspress_hawb WHERE hawb_id =' . $id . ' ORDER BY id ASC');
        $query->execute();
        $result_status = $query->fetchAll();
        if (empty($result_status)) {
            return false;
        }
        $result_status = $this->statusInfo($result_status);
        $resultQuery['id_hawb'] = $id;
        $resultQuery['express_id'] = $result_status;
        return $resultQuery;
    }

    public function setStatusForBatch($data)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $sqlExpress = "SELECT id FROM hawb WHERE batch_name = \"{$data['select']}\"";
        $statement = $connect->db->prepare($sqlExpress);
        $statement->execute();
        $parcelBatch = $statement->fetchAll();
        if ($_POST['activity'] == "Посилка прийнята") {
            try {
                $sqlExpress = "UPDATE hawb SET archive = 1 WHERE batch_name = \"{$data['select']}\"";
                $statement = $connect->db->prepare($sqlExpress);
                $statement->execute();
            } catch (Exception $e) {
                ////////
            }
        }
        foreach ($parcelBatch as $key => $value) {
            try {
                $sqlExpress = "SELECT * FROM exspress_hawb WHERE hawb_id = {$value['id']}";
                $statement = $connect->db->prepare($sqlExpress);
                $statement->execute();
                $infoExpress = $statement->fetchAll();
                $match = 0;
                foreach ($infoExpress as $keyEx => $val){
                    if (
                        $val['date_time'] == $data['date_time'] &&
                        $val['location'] == $data['location'] &&
                        $val['activity'] == $data['activity']
                    ){
                        $match = 1;
                    }
                }
                if (!$match){
                    $sqlExpress = "INSERT INTO exspress_hawb(date_time,location,activity,hawb_id) VALUES(:date_time,:location,:activity,:hawb_id)";
                    $statement = $connect->db->prepare($sqlExpress);
                    $statement->bindParam(':date_time', $data['date_time']);
                    $statement->bindParam(':location', $data['location']);
                    $statement->bindParam(':activity', $data['activity']);
                    $statement->bindParam(':hawb_id', $value['id']);
                    $statement->execute();
                    $this->countUpdateParcelForBatch++;
                }
            } catch (Exception $e) {
                ///////
            }
        }
        return TRUE;
    }

    public function setData($arr)
    {
        $connect = new Database(HOST, DB, USER, PASS);
        $sqlExpress = "INSERT INTO exspress_hawb(date_time,location,activity,hawb_id) VALUES(:date_time,:location,:activity,:hawb_id)";
        $statement = $connect->db->prepare($sqlExpress);
        $statement->bindParam(':date_time', $arr['date_time']);
        $statement->bindParam(':location', $arr['location']);
        $statement->bindParam(':activity', $arr['activity']);
        $statement->bindParam(':hawb_id', $arr['hawb_id']);
        try {
            $statement->execute();
            return true;
        } catch (Exception $e) {
            return FALSE;
        }
    }
}