<?php

/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:30 AM
 */
class model_mawb_add extends Model
{
    private $connect;

    public function __construct()
    {
        $this->connect = new Database(HOST, DB, USER, PASS);
    }

    function addInTable($e1, $e2, $e3, $e4, $e5, $e6, $m1, $m2, $m3, $m4, $m5, $m6, $m7, $m8, $m9, $m10, $m11, $m12, $m13, $m14, $m15, $m16, $m17, $m18, $m19, $sub_post)
    {

        $sql = 'INSERT INTO express_mawb(id, al_code, flt_no, etd, etd_time,eta,eta_time,via) VALUES (:id, :al_code, :flt_no, :etd, :etd_time,:eta,:eta_time,:via)';
        $statement = $this->connect->db->prepare($sql);
        $id = 0;
        $via = 0;
        $statement->bindParam(':id', $id);
        $statement->bindParam(':al_code', $e1);
        $statement->bindParam(':flt_no', $e2);
        $statement->bindParam(':etd', $e3);
        $statement->bindParam(':etd_time', $e4);
        $statement->bindParam(':eta', $e5);
        $statement->bindParam(':eta_time', $e6);
        $statement->bindParam(':via', $via);

        $statement->execute();
        $idexpress = $this->connect->db->lastInsertId();

        $sql = 'INSERT INTO mawb(id, org_entity, org_port, carrier, mawn_no, origin_branch, destination_port, destination_entity,branch,mode_of_transport,no_of_baby_bags, no_of_master_bags,ttc_bags_weight,actual_weight,hawbs_weight,hawbs_cube,remarks,linehaul_supplier,srr,custom_reference,exspress_id,user_id,sub) VALUES (:id_m, :org_entity, :org_port, :carrier, :mawn_no, :origin_branch, :destination_port, :destination_entity,:branch,:mode_of_transport,:no_of_baby_bags, :no_of_master_bags,:ttc_bags_weight,:actual_weight,:hawbs_weight,:hawbs_cube,:remarks,:linehaul_supplier,:ssr,:custom_reference,:express_id,:user_id,:sub)';
        $statement = $this->connect->db->prepare($sql);
        $id_m = 0;
        $express_id = $idexpress;
        $user_id = $_SESSION['id'];
        $statement->bindParam(':id_m', $id_m);
        $statement->bindParam(':org_entity', $m1);
        $statement->bindParam(':org_port', $m2);
        $statement->bindParam(':carrier', $m3);
        $statement->bindParam(':mawn_no', $m4);
        $statement->bindParam(':origin_branch', $m5);
        $statement->bindParam(':destination_port', $m6);
        $statement->bindParam(':destination_entity', $m7);
        $statement->bindParam(':branch', $m8);
        $statement->bindParam(':mode_of_transport', $m9);
        $statement->bindParam(':no_of_baby_bags', $m10);
        $statement->bindParam(':no_of_master_bags', $m11);
        $statement->bindParam(':ttc_bags_weight', $m12);
        $statement->bindParam(':actual_weight', $m13);
        $statement->bindParam(':hawbs_weight', $m14);
        $statement->bindParam(':hawbs_cube', $m15);
        $statement->bindParam(':remarks', $m16);
        $statement->bindParam(':linehaul_supplier', $m17);
        $statement->bindParam(':ssr', $m18);
        $statement->bindParam(':custom_reference', $m19);
        $statement->bindParam(':express_id', $express_id);
        $statement->bindParam(':user_id', $user_id);
        $statement->bindParam(':sub', $sub_post);

        $statement->execute();

        return $this->connect->db->lastInsertId();
        /*id, org_entity, org_port, carrier, mawn_no, origin_branch, destination_port, destination_entity,branch,mode_of_transport,no_of_baby_bags, no_of_master_bags,ttc_bags_weight,actual_weight,hawbs_weight,hawbs_cube,remarks,linehaul_supplier,ssr,custom_reference,express_id,user_id*/

    }

    public function addManifest($mawb_id = 1, $post)
    {
        while ($value = current($post)) {
            if ($value == 'on') {
                $id_hawb[] = key($post);
            }
            next($post);
        }
        ///
        if (isset($id_hawb)) {
            $connect = new Database(HOST, DB, USER, PASS);
            //exit;
            try {
                for ($i = 0; $i < count($id_hawb); $i++) {
                    $data = [
                        'mawb_id' => $mawb_id,
                        'id' => $id_hawb[$i],
                    ];
                    $sql = "UPDATE hawb SET mawb_id = :mawb_id
                    WHERE id = :id";
                    $statement = $connect->db->prepare($sql);
                    $statement->execute($data);
                }
                return TRUE;
            } catch (Exception $e) {
                return FALSE;
            }
        }
    }

    public function sendMail($email)
    {


        $mailSubject = 'Invoices'; // Title mail

        $msg = 'Invoices';// Text mail


        //This path for manifest and mawb
        $pathMawb = './documents/mawb.pdf';
        $pathManifest = './documents/manifest.pdf';
        //Open manifest and mawb for read
        $fpManifest = fopen($pathManifest, "r");
        $fpMawb = fopen($pathMawb, "r");
        //If we don`t open file,so we are out message about error
        if (!$fpManifest) {
            echo "Файл manifest не может быть прочитан";
        }
        if (!$fpMawb) {
            echo "Файл mawb не может быть прочитан";
        }

        $fileManifest = fread($fpManifest, filesize($pathManifest));
        $fileMawb = fread($fpMawb, filesize($pathMawb));

        //Close opened file
        fclose($fpManifest);
        fclose($fpMawb);


        $boundary = "--" . md5(uniqid(time())); // генерируем разделитель

        $headers = "MIME-Version: 1.0\n";

        $headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
        $headers .= "From: Отправитель <'.FROM.'>\r\n";
        $multipart = "--$boundary\n";

        $encoding = 'UTF-8'; // или $kod = 'windows-1251';

        $multipart .= "Content-Type: text/html; charset=$encoding\n";

        $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";

        $multipart .= "$msg\n\n";


        $message_part = "--$boundary\n";

        $message_part .= "Content-Type: application/octet-stream\n";

        $message_part .= "Content-Transfer-Encoding: base64\n";

        $message_part .= "Content-Disposition: attachment;filename = \"Manifest.pdf\"\n\n";

        $message_part .= chunk_split(base64_encode($fileManifest)) . "\n";

        $message_part .= "--$boundary\n";

        $message_part .= "Content-Type: application/octet-stream\n";

        $message_part .= "Content-Transfer-Encoding: base64\n";

        $message_part .= "Content-Disposition: attachment;filename = \"Mawb.pdf\"\n\n";

        $message_part .= chunk_split(base64_encode($fileMawb)) . "\n";

        $multipart .= $message_part . "--$boundary--\n";


        if (!mail($email, $mailSubject, $multipart, $headers)) {
            return FALSE;
        }
        return TRUE;
    }


    function createMawbpdf ($id_mawb){

            require_once ('./application/core/Classes/fpdf/createReport.php');

        /*if (isset($_POST['search'])){
   $search = $_POST['search'];*/
        $pdf=new PDF();
//    $pdo = new Database("127.0.0.1","aramex",'root', '');


        /*    $sql1 = "SELECT * FROM snipper WHERE id = $search";
            $result =  $pdo->db->query($sql1);
                $result_consignee = $pdo->query->fetchAll();*/




        $pdf->SetWidths(array(40,40,40,40));
        $pdf->SetFont('Arial','',14);
        $pdf->AddPage();
        $pdf->BasicTable(array('Org Port','MAWB','FLT','Total No. of Shipments' ),array('LHR','556 LHR 14141676','112','11' ));
        $pdf->currentPointerPosition =10;
        $pdf->i = 0;
        $header=array('Dest port','Remarks','VIA','Total No. Master Bags' );
        $param=array('KBP','UKR PS112','','1' );
        $pdf->BasicTable($header,$param);
        $pdf->currentPointerPosition =10;
        $pdf->i = 0;
        $header=array('    ','             ','ETD','Total No. Baby Bags' );
        $param=array('','','5/31/18 12:20','1' );
        $pdf->BasicTable($header,$param);
        $pdf->currentPointerPosition =10;
        $pdf->i = 0;
        $header=array('','                ','ETA','Total Shipments Weight' );
        $param=array('','','5/31/18 17:40','16.36 KG' );
        $pdf->BasicTable($header,$param);
        $pdf->currentPointerPosition =10;
        $pdf->i = 0;
        $header=array('','               ','                        ','Consol Weight' );
        $param=array('','','','16.00 KG' );
        $pdf->BasicTable($header,$param);
        $pdf->currentPointerPosition =10;
        $pdf->i = 0;
        $header=array('Dest Country','TotalWeight');
        $param=array('UA','16.36 KG');
        $pdf->BasicTable2($header,$param);
        $pdf->i = 0;
        $pdf->SetWidths(array(10,30,20,30,40,30,20));
        $pdf->SetTextColor(255, 0,0);


// header hawb
        $pdf->Row(array('Serial','HAWB','Org','Sender','Receiver','Shpt Info','Service Info'));
        $pdf->Ln(6);
        $pdf->SetTextColor(0, 0,139);
//hawb
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('1','31650958065','LON','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Ln(6);
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('2','31650958066','LON','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasfdmblkdxngmkoxndogbnzxdnbzdjnbzijnbijznbjnzjvnzdxjvnbzoxjnbjz;dnbxzdojnjbzxdo;jn','Wgt 3.60 KG','Type EPX'));
        $pdf->Ln(6);
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('3','31650958067','LON','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Row(array('','','','James preston','Abbas Zulfugarov asfa faasfasf','Wgt 3.60 KG','Type EPX'));
        $pdf->Output('F','documents/manifest.pdf');
        /*
        $pdfi=new PDF_MC_Table();
        $pdfi->AddPage();
        $pdfi->SetFont('Arial','',14);
        $pdfi->SetWidths(array(10,20,30,30,30,30,30,30));
        srand(microtime()*1000000);
        $pdfi->Row(array(GenerateSentence($header[0]),GenerateSentence($header[1]),GenerateSentence($header[1]),GenerateSentence($header[1]),GenerateSentence($header[1]),GenerateSentence($header[1])));

        $pdfi->Output();*/


//Теперь будем пользоваться унаследованным от FPDF классом PDF
        /*$pdf=new PDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Times','',12);

        $pdf->Output();*/

//$pdo = new Database("127.0.0.1","aramex",'root', '');
//$result = $pdo->get_mawb();
//echo '<pre>';
//print_r($result) ;
//echo '</pre>';
        /*} else {
            return "попробуйте еще раз";
        }*/


    }

}