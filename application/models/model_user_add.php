<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:28 AM
 */
class model_user_add extends Model{
    private $connect;

    public function __construct(){
        $this->connect =  new Database(HOST,DB,USER,PASS);
    }

    public function userAdd($userInfo){
        $statement = $this->connect->db->prepare('INSERT INTO user(id, login, password, firstname, lastname, law) VALUES (:id, :login, :password, :firstname, :lastname, :law)');

        $id = 0;
        $login = $userInfo['login'];
        $password = $userInfo['password'];
        $firstname = $userInfo['first_name'];
        $lastname = $userInfo['last_name'];
        $law = $userInfo['law'];

        $statement->bindParam(':id',$id);
        $statement ->bindParam(':login', $login);
        $statement ->bindParam(':password', $password );
        $statement ->bindParam(':firstname', $firstname);
        $statement ->bindParam(':lastname', $lastname);
        $statement ->bindParam(':law', $law);

        $statement->execute();

        return TRUE;
    }
}