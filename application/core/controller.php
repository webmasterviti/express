<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 03.01.2018
 * Time: 23:33
 */

class Controller {

    public $model;
    public $view;

    public function __construct(){
        $this->view = new View();
        $this->model = new Model();
    }

    public function action_index(){

    }
    public function auth(){
        $resultAuth = $this->model->auth($_POST['login'], $_POST['password']);
        if (!$resultAuth['Result']){
            header('Location:/login');
        }
    }
}

