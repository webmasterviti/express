<?php

include ('fpdf.php');


class PDF extends FPDF
{
    var $widths;
    var $aligns;
    public $currentPointerPosition = 10;
    public $i =0;

    function Header()
    {
        //Логотип
        $this->Image('./application/core/Classes/fpdf/aramex.png',5,5,200);
        //шрифт Arial, жирный, размер 15
        $this->SetFont('Arial','B',15);
        //Разрыв строки
        $this->Ln(10);
    }

//Простая таблица
    function BasicTable($headers,$params)
    {
        $this->SetFont('Times','',8);
        foreach(array_combine($headers, $params) as $row => $cow)

            $this->Cell(46,7,$this->cellMultiColor([
                    [
                        'text' => $row,
                        'color' => [25, 25, 112],
                    ],
                    [
                        'text' => $cow,
                        'color' => [139, 0, 0],
                    ]]) ,0);

        $this->Ln();

    }

    function BasicTable2($headers,$params)
    {
        //Заголовок
        //$this->SetTextColor(0, 0,255);
        $this->SetFont('Times','',8);
        foreach(array_combine($headers, $params) as $row => $cow)
            $this->Cell(12,7,$this->cellMultiColor([
                [
                    'text' => $row,
                    'color' => [25, 25, 112],
                ],
                [
                    'text' => $cow,
                    'color' => [139, 0, 0],
                ]]) ,0);
        $this->Ln(10);
        $this->Line(10, 70, 200, 70);
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {


        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

    function cellMultiColor($stringParts)
    {
        foreach ($stringParts as $part) {


            // Set the pointer to the end of the previous string part
            $this->SetX($this->currentPointerPosition);

        // Get the color from the string part
            $this->SetTextColor($part['color'][0], $part['color'][1], $part['color'][2]);

            $this->Cell( $this->GetStringWidth($part['text']), 8, $part['text']);

        // Update the pointer to the end of the current string part
            $this->currentPointerPosition =+ $this->currentPointerPosition+2;
            $this->currentPointerPosition =+ $this->currentPointerPosition+$this->GetStringWidth($part['text']);
    }

       if ($this->i == 0){
           $this->currentPointerPosition = 48;
       }
        if ($this->i == 1){
            $this->currentPointerPosition = 100;
        }
        if ($this->i == 2){
            $this->currentPointerPosition = 140;
        }
        $this->i++;
    }



}




