<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 03.01.2018
 * Time: 23:30
 */


class Model
{
    public function get_data(){

    }

    public function auth($login, $pass){
        $resSession = $this->authSession();

        if (!$resSession['Result']) {
            $pdo = new Database(HOST, DB, USER, PASS);
            $resultQuery = $pdo->getAllUser($login);
            if (!$resultQuery) {
                return [
                    'Result' => FALSE,
                    'Text' => 'Login not found'
                ];
            }
            if ($resultQuery['password'] != $pass) {
                return [
                    'Result' => FALSE,
                    'Text' => 'Password does not match'
                ];
            }
            $_SESSION['id'] = $resultQuery['id'];
            $_SESSION['login'] = $resultQuery['login'];
            $_SESSION['password'] = $resultQuery['password'];
        }
        return [
            'Result' => TRUE,
            'Text' => 'Coincidence'
        ];
    }
    public function authSession(){
        //Проверяем наличие сессии
        if(!isset($_SESSION)){
            return FALSE;
        }
        //Создаем екземпляр класа для роботы с базой данных
        $pdo =  new Database(HOST,DB,USER,PASS);
        //Передаем в функцию getAllUser обэкта pdo значине переменной $_SESSION['login']
        //для получения информации по пользователю(id, login, password)
        $resultQuery = $pdo->getAllUser($_SESSION['login']);
        //Если результат функции false то пользователь не найден
        if (!$resultQuery){
            return [
                'Result' => FALSE,
                'Text'=>'Login not found'
            ];
        }
        //Проверяем совпадение пароля записаного в сессии с паролем который достали с базы данных
        if ($_SESSION['password'] != $resultQuery['password']){
            return [
                'Result' => FALSE,
                'Text' =>'Password does not match'
            ];
        }
        return [
            'Result' => TRUE,
            'Text' => 'Session data == Db data'
        ];
    }
    //Функция для перевода строкового представления прав пользователя в цифровой
    private function userLaw($userLaw){
        switch ($userLaw){
            case 'level_1':
                return 1;
            case 'level_2':
                return 2;
            case 'level_3';
                return 3;
        }
        return 0;
    }
    //Функция для получения полной информации по пользователю
    public function allInfoUser(){
        //Создаем екземпляр класса Database
        $connect = new Database(HOST, DB, USER, PASS);
        //Екринируем запрос
        //Запрос для получение всей информации по пользователю с таблици user
        $query = $connect->db->prepare('SELECT id, login, password,firstname,lastname,law FROM user WHERE id = :id');
        //Для выполнение запроса передаем значение переменной $_SESSION['id']
        $query->execute([':id' => $_SESSION['id']]);
        //Предсавляем данные в виде асоциативного массива
        $resultQuery = $query->fetchAll();
        //Меняем название поля в асоциативного массива для улобства дальнейшего использования
        //Меняем строковое значение на цифровое использовав функцию userLaw
        $resultQuery[0]['userLaw'] = $this->userLaw($resultQuery[0]['law']);
        //Удаляем поле law в массиве
        unset($resultQuery[0]['law']);
        return $resultQuery[0];
    }
    //Функия для получения информации с таблици HAWB
    public function allInfoHawb(){
        //Создаем екземпляр класса Database
        $connect = new Database(HOST, DB, USER, PASS);
        //Екранируем запрос
        $query = $connect->db->prepare('SELECT * FROM hawb');
        $query->execute();
        //Получаем все данные с таблицы HAWB
        $resultQuery = $query->fetchAll();
        //Пробигаемся по всем внутренним массивам переменной $resultQuery( по каждому HAWB)
        for($i = 0;$i < count($resultQuery);$i++){
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ snipper заполняем информацией с таблицы snipper
            $query = $connect->db->prepare('SELECT name_s,phone FROM snipper WHERE id ='. $resultQuery[$i]['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            ///////
            $resultQuery[$i]['snipper_id'] = $result_snipper[0];
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ consignee заполняем информацией с таблицы consignee
            $query = $connect->db->prepare('SELECT name_c,phone FROM consignee WHERE id ='. $resultQuery[$i]['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery[$i]['consignee_id'] = $result_consignee[0];
        }

        return $resultQuery;
    }
    //Функция для получения строк с таблицы Hawb у которых не заполнена колонка mawb_id
    //Тоесть HAWB не связан с MAWB
    public function partInfoHawb(){
        //Создаем екземпляр класса Database
        $connect = new Database(HOST, DB, USER, PASS);
        //Екранируем запрос
        $query = $connect->db->prepare('SELECT * FROM hawb WHERE mawb_id IS NULL');
        $query->execute();
        //Получаем строки в которых колонка mawb_id null в таблице HAWB
        $resultQuery = $query->fetchAll();
        //Пробигаемся по всем внутренним массивам переменной $resultQuery( по каждому HAWB)
        for($i = 0;$i < count($resultQuery);$i++){
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ snipper заполняем информацией с таблицы snipper
            $query = $connect->db->prepare('SELECT name_s,phone FROM snipper WHERE id ='. $resultQuery[$i]['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            ///////
            $resultQuery[$i]['snipper_id'] = $result_snipper[0];
            //Производим запрос для дополнение информации про HAWB
            //Вместо id ключ consignee заполняем информацией с таблицы consignee
            $query = $connect->db->prepare('SELECT name_c,phone FROM consignee WHERE id ='. $resultQuery[$i]['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery[$i]['consignee_id'] = $result_consignee[0];
        }
        return $resultQuery;
    }
}