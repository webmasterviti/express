<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkushav
 * Date: 11.05.2018
 * Time: 17:11
 */
class Database {
    // Свойство - обект класса PDO
    public $db;

    public function __construct($host, $db, $user, $pass, $charset = 'utf8'){
        try {
            //Определяем необходимые опции
            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_PERSISTENT => true
            ];
            $this->db = new PDO("mysql:host=$host;dbname=$db", $user, $pass,$opt);
            $this->db->exec("set names $charset");
        }
        catch(PDOException $e) {
            throw $e;
        }
        return $this->db;
    }
    //Функция для получения информации по пользоывателю(id, login, password)
    public function getAllUser($login){
        //Екранируем запрос
        $query = $this->db->prepare('SELECT id, login, password FROM user WHERE login = :login');
        //Подставляем значение и производим запрос
        $resultQuery = $query->execute([':login' => $login]);
        //Если пользователь не найден вернем ложное значение
        if (!$resultQuery){
            return FALSE;
        }
        //Представим данные в виде асоциативного массива
        $resultQuery = $query->fetchAll();
        //Так как совподение не должно привышать более одного
        //Представим массив без внутреннего перечисления
        return $resultQuery = $resultQuery[0];
    }

}